/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V12.0  February 14, 2005
*
* Z88 should compile and run under any UNIX OS and Motif 2.0.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/**********************************************************************
* Function g5i188 prueft die 5.Eingabegruppe fuer Z88NI.TXT
* 4.10.2005 Rieg
**********************************************************************/

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88v.h>
#include <stdio.h>   /* FILE,printf */
#endif

/***********************************************************************
* Functions
***********************************************************************/
void erif88(FR_INT4 izeile);

/***********************************************************************
* Start G5I188
***********************************************************************/
int g5i188(void)
{
extern FILE *fdatei;

extern FR_INT4AY jtyp;
extern FR_INT4AY jzeile;

extern FR_INT4 ne,niflag,izeile,LANG;

FR_DOUBLE epsx,epsy,epsz;

FR_INT4 i,iele,ityp,jtypalt,ix,iy,iz;

int ier;

char *cresult;
char cline[256];
char cmodex,cmodey,cmodez;

/**********************************************************************
* Schleife ueber alle Elemente: alle Elemente gleich ?
**********************************************************************/
jtypalt= jtyp[1];
        
for(i = 2;i <= ne;i++)
  {
  if(jtyp[i] != jtypalt)
    {
    if(LANG == 1)
      {
      printf("### Superelementtyp muss fuer alle Superelemente gleich sein\n");
      printf("### 2.Wert fuer Superelement %ld in Zeile %ld ueberpruefen\n",
      i-1,jzeile[i-1]);
      printf("### 2.Wert fuer Superelement %ld in Zeile %ld ueberpruefen\n",
      i,jzeile[i]);
      }
    if(LANG == 2)
      {
      printf(
      "### superelement type must be identical for all superelements\n");
      printf("### check 2nd entry for superelement %ld in line %ld\n",
      i-1,jzeile[i-1]);
      printf("### check 2nd entry for superelement %ld in line %ld\n",
      i,jzeile[i]);
      }
    return(2);
    }
  }

/**********************************************************************
* Schleife ueber alle Elemente
**********************************************************************/
for(i = 1;i <= ne;i++)
  {                                              /* 20 */
  izeile++;

/*---------------------------------------------------------------------
* Schreibfehler 1. Zeile 5.Gruppe ?
*--------------------------------------------------------------------*/ 
  cresult= fgets(cline,256,fdatei);
  if(!cresult)
    {
    erif88(izeile);
    return(2);
    }
        
  ier= sscanf(cline,"%ld %ld",&iele,&ityp);
  if(ier != 2) 
    {
    printf("%s\n",cline);
    if(LANG == 1) printf(
      "### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",izeile);
    if(LANG == 2) printf(
      "### typing error or missing entries in line %ld detected\n",izeile);
    return(2);
    }

/*---------------------------------------------------------------------
* logische Pruefung 1. Zeile 5.Gruppe ?
*--------------------------------------------------------------------*/ 
  if(iele != i)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Superelementnummer falsch\n");
      printf("### 1.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### superelement number wrong\n");
      printf("### check 1st entry in line %ld\n",izeile);
      }
    return(2);
    }

  if(jtyp[i] == 10)
    {
    if(!(ityp == 1 || ityp == 10))
      {
      if(LANG == 1)
        {
        printf("### Aus Superelementtyp 10 kann nur\n");
        printf("### Elementtyp 1 oder 10 erzeugt werden\n");
        printf("### 2.Wert fuer Superelement %ld in Zeile %ld ueberpruefen\n",
        i,jzeile[i]);
        printf("### 2.Wert fuer Superelement %ld in Zeile %ld ueberpruefen\n",
        i,izeile);
        }
      if(LANG == 2)
        {
        printf("### superelement type 10 can only generate\n");
        printf("### element types 1 or 10\n");
        printf("### check 2nd entry for superelement %ld in line %ld\n",
        i,jzeile[i]);
        printf("### check 2nd entry for superelement %ld in line %ld\n",
        i,izeile);
        }
      return(2);
      }
    }
  else if(jtyp[i] == 7 || jtyp[i] == 11) 
    {
    if(ityp != 7)
      {
      if(LANG == 1)
        {
        printf("### Aus Superelementtyp 7 bzw. 11 kann nur\n");
        printf("### Elementtyp 7 erzeugt werden\n");
        printf("### 2.Wert fuer Superelement %ld in Zeile %ld ueberpruefen\n",
        i,jzeile[i]);
        printf("### 2.Wert fuer Superelement %ld in Zeile %ld ueberpruefen\n",
        i,izeile);
        }
      if(LANG == 2)
        {
        printf("### superelement types 7 or 11 can only generate\n");
        printf("### element type 7\n");
        printf("### check 2nd entry for superelement %ld in line %ld\n",
        i,jzeile[i]);
        printf("### check 2nd entry for superelement %ld in line %ld\n",
        i,izeile);
        }
      return(2);
      }
    }
  else if(jtyp[i] == 8 || jtyp[i] == 12) 
    {
    if(ityp != 8)
      {
      if(LANG == 1)
        {
        printf("### Aus Superelementtyp 8 bzw. 12 kann nur\n");
        printf("### Elementtyp 8 erzeugt werden\n");
        printf("### 2.Wert fuer Superelement %ld in Zeile %ld ueberpruefen\n",
        i,jzeile[i]);
        printf("### 2.Wert fuer Superelement %ld in Zeile %ld ueberpruefen\n",
        i,izeile);
        }
      if(LANG == 2)
        {
        printf("### superelement types 8 or 12 can only generate\n");
        printf("### element type 8\n");
        printf("### check 2nd entry for superelement %ld in line %ld\n",
        i,jzeile[i]);
        printf("### check 2nd entry for superelement %ld in line %ld\n",
        i,izeile);
        }
      return(2);
      }
    }
  else if(jtyp[i] == 20) 
    {
    if(!(ityp == 19 || ityp == 20))
      {
      if(LANG == 1)
        {
        printf("### Aus Superelementtyp 20 koennen nur\n");
        printf("### Elementtypen 19 und 20 erzeugt werden\n");
        printf("### 2.Wert fuer Superelement %ld in Zeile %ld ueberpruefen\n",
        i,jzeile[i]);
        printf("### 2.Wert fuer Superelement %ld in Zeile %ld ueberpruefen\n",
        i,izeile);
        }
      if(LANG == 2)
        {
        printf("### superelement type 20 can only generate\n");
        printf("### element types 19 and 20\n");
        printf("### check 2nd entry for superelement %ld in line %ld\n",
        i,jzeile[i]);
        printf("### check 2nd entry for superelement %ld in line %ld\n",
        i,izeile);
        }
      return(2);
      }
    }
  else
    {
    if(LANG == 1)
      {
      printf("### Nicht erlaubt fuer Netzgenerator\n");
      printf("### 2.Wert fuer Superelement %ld in Zeile %ld ueberpruefen\n",
      i,jzeile[i]);
      printf("### 2.Wert fuer Superelement %ld in Zeile %ld ueberpruefen\n",
      i,izeile);
      }
    if(LANG == 2)
      {
      printf("### not allowed for net generator\n");
      printf("### check 2nd entry for superelement %ld in line %ld\n",
      i,jzeile[i]);
      printf("### check 2nd entry for superelement %ld in line %ld\n",
      i,izeile);
      }
    return(2);
    }
    
/*---------------------------------------------------------------------
* Schreibfehler 2. Zeile 5.Gruppe ?
*--------------------------------------------------------------------*/ 
  izeile++;
  
  cresult= fgets(cline,256,fdatei);
  if(!cresult)
    {
    erif88(izeile);
    return(2);
    }

  if(ityp == 1 || ityp == 10)
    {
    ier= sscanf(cline,"%ld %c %ld %c %ld %c",
    &ix,&cmodex,&iy,&cmodey,&iz,&cmodez);
    if(ier != 6) 
      {
      printf("%s\n",cline);
      if(LANG == 1)
        printf(
        "### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",
        izeile);
      if(LANG == 2)
        printf(
        "### typing error or missing entries in line %ld detected\n",
        izeile);
      return(2);
      }
    }
  else
    {
    ier= sscanf(cline,"%ld %c %ld %c",&ix,&cmodex,&iy,&cmodey);
    if(ier != 4) 
      {
      printf("%s\n",cline);
      if(LANG == 1)
        printf(
        "### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",
        izeile);
      if(LANG == 2)
        printf(
        "### typing error or missing entries in line %ld detected\n",
        izeile);
      return(2);
      }
    }

/*---------------------------------------------------------------------
* logische Pruefung 2.Zeile 5.Gruppe ?
*--------------------------------------------------------------------*/ 
  if(ix < 1)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Unterteilung X unzulaessig fuer Superelement %ld\n",i);
      printf("### 1.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### subdivision X not allowed for superelement %ld\n",i);
      printf("### check 1st entry in line %ld\n",izeile);
      }
    return(2);
    }

  if(!(cmodex == 'E' || cmodex == 'e' || cmodex == 'L' || cmodex == 'l'))
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf(
      "### Art der Unterteilung X unzulaessig fuer Superelement %ld\n",i);
      printf("### 2.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf(
      "### type of subdivision X not allowed for superelement %ld\n",i);
      printf("### check 2nd entry in line %ld\n",izeile);
      }
    return(2);
    }

  if(iy < 1)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Unterteilung Y unzulaessig fuer Superelement %ld\n",i);
      printf("### 3.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### subdivision Y not allowed for superelement %ld\n",i);
      printf("### check 3rd entry in line %ld\n",izeile);
      }
    return(2);
    }

  if(!(cmodey == 'E' || cmodey == 'e' || cmodey == 'L' || cmodey == 'l'))
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf(
      "### Art der Unterteilung Y unzulaessig fuer Superelement %ld\n",i);
      printf("### 4.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### type of subdivision Y not allowed for superelement %ld\n",i);
      printf("### check 4th entry in line %ld\n",izeile);
      }
    return(2);
    }

  if(ityp == 1 || ityp == 10)
    {
    if(iz < 1)
      {
      printf("%s\n",cline);
      if(LANG == 1)
        {
        printf("### Unterteilung Z unzulaessig fuer Superelement %ld\n",i);
        printf("### 5.Wert in Zeile %ld ueberpruefen\n",izeile);
        }
      if(LANG == 2)
        {
        printf("### subdivision Z not allowed for superelement %ld\n",i);
        printf("### check 5th entry in line %ld\n",izeile);
        }
      return(2);
      }

    if(!(cmodez == 'E' || cmodez == 'e' || cmodez == 'L' || cmodez == 'l'))
      {
      printf("%s\n",cline);
      if(LANG == 1)
        {
        printf(
        "### Art der Unterteilung Z unzulaessig fuer Superelement %ld\n",i);
        printf("### 6.Wert in Zeile %ld ueberpruefen\n",izeile);
        }
      if(LANG == 2)
        {
        printf(
        "### type of subdivision Z not allowed for superelement %ld\n",i);
        printf("### check 6th entry in line %ld\n",izeile);
        }
      return(2);
      }
    }
          
/*---------------------------------------------------------------------
* Schleifenende
*--------------------------------------------------------------------*/ 
  }                                              /* e20 */

/**********************************************************************
* niflag gesetzt ?
**********************************************************************/
if(niflag == 1)
  {
        
/*---------------------------------------------------------------------
* Schreibfehler 6.Gruppe ?
*--------------------------------------------------------------------*/ 
  izeile++;    

  cresult= fgets(cline,256,fdatei);
  if(!cresult)
    {
    erif88(izeile);
    return(2);
    }

  if(ityp == 1 || ityp == 10)
    {
    ier= sscanf(cline,"%lf %lf %lf",&epsx,&epsy,&epsz);
    if(ier != 3) 
      {
      printf("%s\n",cline);
      if(LANG == 1)
        printf(
        "### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",
        izeile);
      if(LANG == 2)
        printf(
        "### typing error or missing entries in line %ld detected\n",
        izeile);
      return(2);
      }
    }
  else
    {
    ier= sscanf(cline,"%lf %lf",&epsx,&epsy);
    if(ier != 2) 
      {
      printf("%s\n",cline);
      if(LANG == 1)
        printf(
        "### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",
        izeile);
      if(LANG == 2)
        printf(
        "### typing error or missing entries in line %ld detected\n",
        izeile);
      return(2);
      }
    }

/*---------------------------------------------------------------------
* logische Pruefung 6.Gruppe ?
*--------------------------------------------------------------------*/ 
  if(FR_FABS(epsx) < 1e-13)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf(
      "### Fangradius EPSX fast oder gleich 0 in Zeile %ld entdeckt\n",
      izeile);
      printf("### 1.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf(
      "### trap radius EPSX about or equal 0 in line %ld detected\n",
      izeile);
      printf("### check 1st entry in line %ld\n",izeile);
      }
    return(2);
    }

  if(FR_FABS(epsy) < 1e-13)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf(
      "### Fangradius EPSY fast oder gleich 0 in Zeile %ld entdeckt\n",
      izeile);
      printf("### 2.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf(
      "### trap radius EPSY about or equal 0 in line %ld detected\n",
      izeile);
      printf("### check 2nd entry in line %ld\n",izeile);
      }
    return(2);
    }
  
  if(ityp == 1 || ityp == 10)
    { 
    if(FR_FABS(epsz) < 1e-13)
      {
      printf("%s\n",cline);
      if(LANG == 1)
        {
        printf(
        "### Fangradius EPSZ fast oder gleich 0 in Zeile %ld entdeckt\n",
        izeile);
        printf("### 3.Wert in Zeile %ld ueberpruefen\n",izeile);
        }
      if(LANG == 2)
        {
        printf(
        "### trap radius EPSZ about or equal 0 in line %ld detected\n",
        izeile);
        printf("### check 3rd entry in line %ld\n",izeile);
        }
      return(2);
      }
    }

  }
        
/**********************************************************************
* Normales Ende
**********************************************************************/
return(0);
}
