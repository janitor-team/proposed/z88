/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V12.0  February 14, 2005
*
* Z88 should compile and run under any UNIX OS and Motif 2.0.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/**********************************************************************
* Function g2i188 prueft die 2.Eingabegruppe fuer Z88I1.TXT & Z88NI.TXT
* 4.10.2005 Rieg
**********************************************************************/

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88v.h>
#include <stdio.h>   /* FILE,printf */
#endif

/***********************************************************************
* Functions
***********************************************************************/
void erif88(FR_INT4 izeile);
int FR_GETC(void);

/***********************************************************************
* Start G2I188
***********************************************************************/
int g2i188(void)
{
extern FILE *fdatei;

extern FR_INT4AY ifrei;

extern FR_INT4 ndim,nkp,nfg,ipflag,iwarn,izeile,LANG;

FR_DOUBLE x,y,z,xalt,yalt,zalt;

FR_INT4 ixnull=0,iynull=0,iznull=0,ixgleich=0,iygleich=0,izgleich=0;
FR_INT4 i,iknot,ifreisum=0,iwarnalt;

int ier,jyn;

char *cresult;
char cline[256];
                           
/**********************************************************************
* Werte vorbelegen
**********************************************************************/
izeile  = 1;
iwarnalt= iwarn;

xalt= 0.; /* nur wg. compiler warnings */
yalt= 0.;
zalt= 0.;
        
/**********************************************************************
* 2 & 3-dimensional: Schleife
**********************************************************************/
for(i = 1;i <= nkp;i++)                          /* 40 */
  {
  izeile++;    
  if(i > 1)
    {
    xalt= x;
    yalt= y;
    zalt= z;
    }

  cresult= fgets(cline,256,fdatei);
  if(!cresult)
    {
    erif88(izeile);
    return(2);
    }

/*---------------------------------------------------------------------
* Schreibfehler ?
*--------------------------------------------------------------------*/ 
  if(ndim == 2)
    {          
    ier= sscanf(cline,"%ld %ld %lg %lg",&iknot,&ifrei[i],&x,&y);
    if(ier != 4) 
      {
      printf("%s\n",cline);
      if(LANG == 1)
        printf(
        "### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",
        izeile);
      if(LANG == 2)
        printf(
        "### typing error or missing entries in line %ld detected\n",
        izeile);
      return(2);
      }
    }
  else
    {          
    ier= sscanf(cline,"%ld %ld %lg %lg %lg",
    &iknot,&ifrei[i],&x,&y,&z);
    if(ier != 5) 
      {
      printf("%s\n",cline);
      if(LANG == 1)
        printf(
        "### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",
        izeile);
      if(LANG == 2)
        printf(
        "### typing error or missing entries in line %ld detected\n",
        izeile);
      return(2);
      }
    }

  ifreisum+= ifrei[i];
          
/*---------------------------------------------------------------------
* sichern der alten Werte; Sonderfall i= 1 
*--------------------------------------------------------------------*/ 
  if(i == 1)
    {
    xalt= x;
    yalt= y;
    zalt= z;
    }

/*---------------------------------------------------------------------
* Knotennummer ?
*--------------------------------------------------------------------*/ 
  if(iknot != i)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Knotennummer falsch\n");
      printf("### 1.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### node number wrong\n");
      printf("### check 1st entry in line %ld\n",izeile);
      }
    return(2);
    }

/*---------------------------------------------------------------------
* Freiheitsgrade 2,3 oder 6 ?
*--------------------------------------------------------------------*/ 
  if(!(ifrei[i] == 2 || ifrei[i] == 3 || ifrei[i] == 6))
    {       
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Anzahl Freiheitsgrade nicht 2,3 oder 6\n");
      printf("### 2.Wert (IBFLAG)in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### number of DOF not 2,3 or 6\n");
      printf("### check 2nd entry (IBFLAG)in line %ld\n",izeile);
      }
    return(2);
    }

/*---------------------------------------------------------------------
* Freiheitsgrade nicht 3 bei gesetztem Plattenflag ?
*--------------------------------------------------------------------*/ 
  if(ipflag != 0 && ifrei[i] != 3 )
    {       
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Platte: Anzahl Freiheitsgrade nicht 3\n");
      printf("### 2.Wert in Zeile %ld ueberpruefen\n",izeile);
      printf("### 8.Wert(IPFLAG)in Zeile 1 ueberpruefen\n");
      }
    if(LANG == 2)
      {
      printf("### plate: number of DOF not 3\n");
      printf("### check 2nd entry in line %ld\n",izeile);
      printf("### check 8th entry (IPFLAG)in line 1\n");      }
    return(2);
    }
/*---------------------------------------------------------------------
* x ?
*--------------------------------------------------------------------*/ 
  if(FR_FABS(x) < 1e-20) ixnull++;
  if(xalt == x)        ixgleich++;

/*---------------------------------------------------------------------
* y ?
*--------------------------------------------------------------------*/ 
  if(FR_FABS(y) < 1e-20) iynull++;
  if(yalt == y)        iygleich++;

/*---------------------------------------------------------------------
* z ?
*--------------------------------------------------------------------*/ 
  if(ndim == 3)
    {
    if(FR_FABS(z) < 1e-20) iznull++;
    if(zalt == z)        izgleich++;
    }

  }                                              /* e 40 */

/**********************************************************************
* Summe Freiheitsgrade o.k. ?
**********************************************************************/
if(ifreisum != nfg)
  {
  if(LANG == 1)
    {
    printf("### Summe Freiheitsgrade nicht korrekt\n");
    printf("### 4.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### total number of DOF not correct\n");
    printf("### check 4th entry in line 1\n");
    }
  return(2);
  }

/**********************************************************************
* irgendetwas ueber die Koordinaten faul ?
**********************************************************************/
if(ixnull == nkp)
  {
  if(LANG == 1)
    {
    printf("### Alle X-Werte nahezu oder gleich 0\n");
    printf("### X-Werte der Knotenkoordinaten ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### all X- values about or equal 0\n");
    printf("### check X- values of node coordinates\n");
    }
  return(2);
  }

if(iynull == nkp)
  {
  if(LANG == 1)
    {
    printf("? Alle Y-Werte nahezu oder gleich 0\n");
    printf("? Y-Werte der Knotenkoordinaten ueberpruefen\n");
    printf("? Nur fuer Elemente Nr.5, 9 und 13 sinnvoll ..verdaechtig\n");
    }
  if(LANG == 2)
    {
    printf("? all Y- values about or equal 0\n");
    printf("? check Y- values of node coordinates\n");
    printf("? only useful for element types 5, 9 and 13 ..suspicious\n");
    }
  iwarn++;
  }

if(ndim == 3)
  {
  if(iznull == nkp)
    {
  if(LANG == 1)
    {
    printf("? Alle Z-Werte nahezu oder gleich 0\n");
    printf("? Z-Werte der Knotenkoordinaten ueberpruefen\n");
    printf("? Nur fuer Elemente Nr.5 sinnvoll ..verdaechtig\n");
    }
  if(LANG == 2)
    {
    printf("? all Z- values about or equal 0\n");
    printf("? check Z- values of node coordinates\n");
    printf("? only useful for elements type 5 ..suspicious\n");
    }
    iwarn++;
    }
  }      
          
if(ixgleich == nkp)
  {
  if(LANG == 1)
    {
    printf("### Alle X-Werte identisch\n");
    printf("### X-Werte der Knotenkoordinaten ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### all X- values identical\n");
    printf("### check X- values of node coordinates\n");
    }
  return(2);
  }

if(iygleich == nkp)
  {
  if(LANG == 1)
    {
    printf("? Alle Y-Werte identisch\n");
    printf("? Y-Werte der Knotenkoordinaten ueberpruefen\n");
    printf("? Nur fuer Elemente Nr.5, 9 und 13 sinnvoll ..verdaechtig\n");
    }
  if(LANG == 2)
    {
    printf("? all Y- values identical\n");
    printf("? check Y- values of node coordinates\n");
    printf("? only useful for element types 5, 9 and 13 ..suspicious\n");
    }
  iwarn++;
  }

if(ndim == 3)
  {
  if(izgleich == nkp)
    {
  if(LANG == 1)
    {
    printf("? Alle Z-Werte identisch\n");
    printf("? Z-Werte der Knotenkoordinaten ueberpruefen\n");
    printf("? Nur fuer Elemente Nr.5 sinnvoll ..verdaechtig\n");
    }
  if(LANG == 2)
    {
    printf("? all Z- values identical\n");
    printf("? check Z- values of node coordinates\n");
    printf("? only useful for elements type 5 ..suspicious\n");
    }
    iwarn++;
    }
  }      
          
/*----------------------------------------------------------------------
* ggf. warnen
*---------------------------------------------------------------------*/
if(iwarn > iwarnalt)
  {
  if(LANG == 1) printf("Weiterchecken ? 1 <ja> 2 <nein> ");
  if(LANG == 2) printf("continue checking ? 1 <yes> 2 <no> ");

  do
    {
    jyn= FR_GETC();

    if     (jyn == 2) return(0);
    else if(jyn == 1) ;
    else
      {
      if(LANG == 1) printf("Falsche Auswahl --> 1 <ja> oder 2 <nein> ");
      if(LANG == 2) printf("wrong choice --> 1 <yes> or 2 <no> ");
      }
    }
  while(!(jyn == 1 || jyn == 2)); 
  }
         
/**********************************************************************
* Normales Ende
**********************************************************************/
return(0);
}
