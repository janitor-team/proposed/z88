/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V13.0  February 14, 2008
*
* Z88 should compile and run under any UNIX OS and Motif 2.0.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/**********************************************************************
* Function g3i188 prueft die 3.Eingabegruppe fuer Z88I1.TXT & Z88NI.TXT
* 15.5.2008 Rieg
**********************************************************************/

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88v.h>
#include <stdio.h>   /* FILE,printf */
#include <string.h>  /* strcpy */
#endif

/***********************************************************************
* Functions
***********************************************************************/
void erif88(FR_INT4 izeile);
int FR_GETC(void);

/***********************************************************************
* Start G3I188
***********************************************************************/
int g3i188(void)
{
extern FILE *fdatei;

extern FR_INT4AY jtyp;
extern FR_INT4AY jzeile;

extern FR_INT4 LANG,NEEDKOI;
extern FR_INT4 ndim,nkp,ne,ibflag,ipflag,iwarn,izeile;
extern FR_INT4 ispann,ipflag,ifnii1; 

FR_INT4 koi[21];

FR_INT4 j,i,k,iele,ityp,jmax= 0,iwarnalt;

int ier,jyn;

char *cresult;
char cline[256];

iwarnalt= iwarn;
                                   
/**********************************************************************
* Schleife ueber alle Elemente
**********************************************************************/
NEEDKOI= 1;

for(i = 1;i <= ne;i++)                           /* 80 */
  {
  izeile++;
  jzeile[i]= izeile;
          
/*---------------------------------------------------------------------
* Schreibfehler 1. Zeile 3.Gruppe ?
*--------------------------------------------------------------------*/ 
  cresult= fgets(cline,256,fdatei);
  if(!cresult)
    {
    erif88(izeile);
    return(2);
    }

  ier= sscanf(cline,"%ld %ld",&iele,&ityp);
  if(ier != 2) 
    {
    printf("%s\n",cline);
    if(LANG == 1) printf("### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",izeile);
    if(LANG == 2) printf("### typing error or missing entries in line %ld detected\n",izeile);
    return(2);
    }

  jtyp[i]= ityp;

/*---------------------------------------------------------------------
* ist File z88i3.txt noetig ?
*--------------------------------------------------------------------*/ 
  if(ispann == 0)
    {
    if(ityp == 1 || ityp == 3  || ityp == 6  || ityp ==  7 ||
       ityp == 8 || ityp == 10 || ityp == 11 || ityp == 12 ||
       ityp ==14 || ityp == 15 || ityp == 16 || ityp == 17 ||
       ityp ==18 || ityp == 19 || ityp == 20) ispann= 1;
    }
          
/*---------------------------------------------------------------------
* logische Pruefung 1. Zeile 3.Gruppe ?
*--------------------------------------------------------------------*/ 
  if(iele != i)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Elementnummer falsch\n");
      printf("### 1.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### element number wrong\n");
      printf("### check 1st entry in line %ld\n",izeile);
      }
    return(2);
    }
  
  if(!(ityp >= 1 && ityp <= 20))
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Elementtyp unbekannt\n");
      printf("### 2.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### unknown element type\n");
      printf("### check 2nd entry in line %ld\n",izeile);
      }
    return(2);
    }

  if((ityp == 2 || ityp == 13) && ibflag != 1)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Balkenflag in 1.Zeile nicht auf 1 gesetzt\n");
      printf("### 2.Wert in Zeile %ld ist ein Balken\n",izeile);
      printf("### 7.Wert in 1.Zeile ueberpruefen oder\n");
      printf("### 2.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### beam flag in 1st line not set to 1\n");
      printf("### 2. entry in line %ld is a beam\n",izeile);
      printf("### check 7th entry in 1st line or\n");
      printf("### check 2nd entry in line %ld\n",izeile);
      }
    return(2);
    }
  
  if((ityp == 18 || ityp == 19 || ityp == 20) && ipflag == 0)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Plattenflag in 1.Zeile nicht auf 1 oder 2 gesetzt\n");
      printf("### 2.Wert in Zeile %ld ist eine Platte\n",izeile);
      printf("### 8.Wert in 1.Zeile ueberpruefen oder\n");
      printf("### 2.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### plate flag in 1st line not set to 1\n");
      printf("### 2. entry in line %ld is a plate\n",izeile);
      printf("### check 8th entry in 1st line or\n");
      printf("### check 2nd entry in line %ld\n",izeile);
      }
    return(2);
    }
  if(ndim == 3)
    {
    if(!(ityp == 1 || ityp ==  2 || ityp ==  4 ||
         ityp == 5 || ityp == 10 || ityp == 16 || ityp == 17))
      {   
      printf("%s\n",cline);
      if(LANG == 1)
      {
      printf(
      "? Elementtyp und Dimension der Struktur nicht in Uebereinstimmung\n");
      printf("? 1.Wert in Zeile 1 und\n");
      printf("? 2.Wert in Zeile %ld fuer Element %ld ueberpruefen\n",izeile,i);
      printf("? 2-D Elementtyp in 3-D Struktur eingemischt\n");
      printf("? Kann aber fuer gemischte Strukturen sinnvoll sein\n");
      }
      if(LANG == 2)
      {
      printf(
      "? element type and dimension of structure do not match\n");
      printf("? check 1st entry in line 1 and\n");
      printf("? 2. entry in line %ld for element %ld\n",izeile,i);
      printf("? 2-D element type mixed in 3-D structure\n");
      printf("? possibly useful for mixed structures\n");
      }
      iwarn++;
      }
    }

  if(ndim == 2)
    {
    if(!(ityp ==  3 || ityp == 6  || ityp == 7  || ityp == 8  ||
         ityp ==  9 || ityp == 11 || ityp == 12 || ityp == 13 ||
         ityp == 14 || ityp == 15 || ityp == 18 || ityp == 19 ||
         ityp == 20))
      {   
      printf("%s\n",cline);
      if(LANG == 1)
      {
      printf(
      "### Elementtyp und Dimension der Struktur nicht in Uebereinstimmung\n");
      printf("### 1.Wert in Zeile 1 und\n");
      printf(
      "### 2.Wert in Zeile %ld fuer Element %ld ueberpruefen\n",izeile,i);
      printf("### 3-D Elementtyp in 2-D Struktur eingemischt\n");
      }
      if(LANG == 2)
      {
      printf(
      "### element type and dimension of structure do not match \n");
      printf("### check 1st entry in line 1 and\n");
      printf(
      "### 2nd entry in line %ld for Element %ld\n",izeile,i);
      printf("### 3-D element type mixed in 2-D structure\n");
      }
      return(2);
      }
    }

/*---------------------------------------------------------------------
* Koinzidenz
*--------------------------------------------------------------------*/ 
  izeile++;
  cresult= fgets(cline,256,fdatei);
  if(!cresult)
    {
    erif88(izeile);
    return(2);
    }

  if(ityp == 1  || ityp == 7 || ityp == 8 || ityp == 20)
    {
    ier= sscanf(cline,"%ld %ld %ld %ld %ld %ld %ld %ld",
    &koi[1],&koi[2],&koi[3],&koi[4],&koi[5],&koi[6],&koi[7],&koi[8]);
    if(ier != 8) 
      {
      printf("%s\n",cline);
      if(LANG == 1) printf("### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",izeile);
      if(LANG == 2) printf("### typing error or missing entries in line %ld detected\n",izeile);
      return(2);
      }
    jmax= 8;
    if(i == ne) NEEDKOI+= 19;   /* Sicherheit, damit's zu Z88F und Z88I1 passt */
    else        NEEDKOI+=  8;
    }

  else if(ityp == 11 || ityp == 12)
    {
    ier= sscanf(cline,
    "%ld %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld",
    &koi[1],&koi[2],&koi[3],&koi[4],&koi[5],&koi[6],
    &koi[7],&koi[8],&koi[9],&koi[10],&koi[11],&koi[12]);
    if(ier != 12) 
      {
      printf("%s\n",cline);
      if(LANG == 1) printf("### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",izeile);
      if(LANG == 2) printf("### typing error or missing entries in line %ld detected\n",izeile);
      return(2);
      }
    jmax= 12;
    if(i == ne) NEEDKOI+= 19;   /* Sicherheit, damit's zu Z88F und Z88I1 passt */
    else        NEEDKOI+= 12;
    }

  else if(ityp == 10)
    {
    ier= sscanf(cline,
    "%ld %ld %ld %ld %ld %ld %ld %ld %ld %ld\
 %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld",
    &koi[ 1],&koi[ 2],&koi[ 3],&koi[ 4],&koi[ 5],
    &koi[ 6],&koi[ 7],&koi[ 8],&koi[ 9],&koi[10],
    &koi[11],&koi[12],&koi[13],&koi[14],&koi[15],
    &koi[16],&koi[17],&koi[18],&koi[19],&koi[20]);
    if(ier != 20) 
      {
      printf("%s\n",cline);
      if(LANG == 1) printf("### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",izeile);
      if(LANG == 2) printf("### typing error or missing entries in line %ld detected\n",izeile);
      return(2);
      }
    jmax= 20;
    if(i == ne) NEEDKOI+= 19;   /* Sicherheit, damit's zu Z88F und Z88I1 passt */
    else        NEEDKOI+= 20;
    }

  else if(ityp == 2 || ityp == 4 || ityp == 5 || ityp == 9 || ityp == 13)
    {
    ier= sscanf(cline,"%ld %ld",&koi[1],&koi[2]);
    if(ier != 2) 
      {
      printf("%s\n",cline);
      if(LANG == 1) printf("### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",izeile);
      if(LANG == 2) printf("### typing error or missing entries in line %ld detected\n", izeile);
      return(2);
      }
    jmax= 2;
    if(i == ne) NEEDKOI+= 19;   /* Sicherheit, damit's zu Z88F und Z88I1 passt */
    else        NEEDKOI+=  2;
    }

  else if(ityp == 3 || ityp == 14 || ityp == 15 || ityp == 18)
    {
    ier= sscanf(cline,"%ld %ld %ld %ld %ld %ld",
    &koi[1],&koi[2],&koi[3],&koi[4],&koi[5],&koi[6]);
    if(ier != 6) 
      {
      printf("%s\n",cline);
      if(LANG == 1) printf("### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",izeile);
      if(LANG == 2) printf("### typing error or missing entries in line %ld detected\n",izeile);
      return(2);
      }
    jmax= 6;
    if(i == ne) NEEDKOI+= 19;   /* Sicherheit, damit's zu Z88F und Z88I1 passt */
    else        NEEDKOI+=  6;
    }

  else if(ityp == 16)
    {
    ier= sscanf(cline,
    "%ld %ld %ld %ld %ld %ld %ld %ld %ld %ld",
    &koi[ 1],&koi[ 2],&koi[ 3],&koi[ 4],&koi[ 5],
    &koi[ 6],&koi[ 7],&koi[ 8],&koi[ 9],&koi[10]);
    if(ier != 10) 
      {
      printf("%s\n",cline);
      if(LANG == 1) printf("### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",izeile);
      if(LANG == 2) printf("### typing error or missing entries in line %ld detected\n",izeile);
      return(2);
      }
    jmax= 10;
    if(i == ne) NEEDKOI+= 19;   /* Sicherheit, damit's zu Z88F und Z88I1 passt */
    else        NEEDKOI+= 10;
    }

  else if(ityp == 19)
    {
    ier= sscanf(cline,
    "%ld %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld",
    &koi[ 1],&koi[ 2],&koi[ 3],&koi[ 4],&koi[ 5],
    &koi[ 6],&koi[ 7],&koi[ 8],&koi[ 9],&koi[10],
    &koi[11],&koi[12],&koi[13],&koi[14],&koi[15],
    &koi[16]);
    if(ier != 16) 
      {
      printf("%s\n",cline);
      if(LANG == 1) printf("### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",izeile);
      if(LANG == 2) printf("### typing error or missing entries in line %ld detected\n",izeile);
      return(2);
      }
    jmax= 16;
    if(i == ne) NEEDKOI+= 19;   /* Sicherheit, damit's zu Z88F und Z88I1 passt */
    else        NEEDKOI+= 16;
    }

  else if(ityp == 17)
    {
    ier= sscanf(cline,"%ld %ld %ld %ld",&koi[1],&koi[2],&koi[3],&koi[4]);
    if(ier != 4) 
      {
      printf("%s\n",cline);
      if(LANG == 1) printf("### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",izeile);
      if(LANG == 2) printf("### typing error or missing entries in line %ld detected\n",izeile);
      return(2);
      }
    jmax= 4;
    if(i == ne) NEEDKOI+= 19;   /* Sicherheit, damit's zu Z88F und Z88I1 passt */
    else        NEEDKOI+=  4;
    }

    else if(ityp == 6)
      {
    ier= sscanf(cline,"%ld %ld %ld",&koi[1],&koi[2],&koi[3]);
    if(ier != 3) 
      {
      printf("%s\n",cline);
      if(LANG == 1) printf("### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",izeile);
      if(LANG == 2) printf("### typing error or missing entries in line %ld detected\n",izeile);
      return(2);
      }
    jmax= 3;
    if(i == ne) NEEDKOI+= 19;   /* Sicherheit, damit's zu Z88F und Z88I1 passt */
    else        NEEDKOI+=  3;
    }
            
/*======================================================================
* koi(j) innerhalb logischer Grenzen ?
*=====================================================================*/
  for(j = 1;j <= jmax;j++)
    {
    if(!(koi[j] > 0 && koi[j] <= nkp))
      {
      printf("%s\n",cline);
      if(LANG == 1)
        {
        printf(
        "### Knoten nicht zwischen 1 und %ld fuer Element %ld entdeckt\n",
        nkp,i);
        printf(
        "### 2. Wert in Zeile 1 und %ld. Wert in Zeile %ld ueberpruefen\n",
        j,izeile);
        }
      if(LANG == 2)
        {
        printf(
        "### node number not in range (1 to %ld) for element %ld detected\n",
        nkp,i);
        printf(
        "### check 2nd entry in line 1 and %ld. entry in line %ld\n",
        j,izeile);
        }
      return(2);
      }
    }

  for(j = 1;j <= jmax-1;j++)
    {
    for(k= j+1;k <= jmax;k++)
      {
      if(koi[j] == koi[k])
        {
        printf("%s\n",cline);
        if(LANG == 1)
        {
          printf(
          "### Identische Knoten fuer Element %ld in Zeile %ld entdeckt\n",
          i,izeile);
          printf(
          "### %ld. Wert und %ld. Wert in Zeile %ld ueberpruefen\n",
          j,k,izeile);
          }
        if(LANG == 2)
        {
          printf(
          "### identical nodes for element %ld in line %ld detected\n",
          i,izeile);
          printf(
          "### check entry %ld and entry %ld in line %ld\n",
          j,k,izeile);
          }
        return(2);
        }
      }
    }
/*---------------------------------------------------------------------
* Schleifenende
*--------------------------------------------------------------------*/ 
  }                                              /* e80 */

/*----------------------------------------------------------------------
* ggf. warnen
*---------------------------------------------------------------------*/
if(iwarn > iwarnalt)
  {
  if(LANG == 1) printf("Weiterchecken ? 1 <ja> 2 <nein> ");
  if(LANG == 2) printf("continue checking ? 1 <yes> 2 <no> ");

  do
    {
    jyn= FR_GETC();

    if     (jyn == 2) return(0);
    else if(jyn == 1) ;
    else
      {
      if(LANG == 1) printf("Falsche Auswahl --> 1 <ja> oder 2 <nein> ");
      if(LANG == 2) printf("wrong choice --> 1 <yes> or 2 <no> ");
      }
    }
  while(!(jyn == 1 || jyn == 2)); 
  }

/**********************************************************************
* Normales Ende
**********************************************************************/
return(0);
}
