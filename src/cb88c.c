/****************************************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the Windows & UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V13.0  February 14, 2008
*
* Z88 should compile and run under any Windows and UNIX OS.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
****************************************************************************************/ 
/****************************************************************************************
* CB88C fuer X11 und gtk+
* 13.5.2008 Rieg
****************************************************************************************/

/****************************************************************************************
* Includes
****************************************************************************************/
/*---------------------------------------------------------------------------------------
* UNIX
*--------------------------------------------------------------------------------------*/
#include <z88com.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/****************************************************************************************
* Functions
****************************************************************************************/
int ale88c(int);
int rcol88c(void);

/****************************************************************************************
* Callbacks
****************************************************************************************/

/*---------------------------------------------------------------------------------------
* CB_INTR
*--------------------------------------------------------------------------------------*/
void CB_INTR(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

strcpy(command,CBROWSER);
strcat(command," ");
strcat(command,CPREFIX);
if(LANG == 1) strcat(command,"g88ge.htm");
if(LANG == 2) strcat(command,"e88ge.htm");
iret= system(command);
if(iret != 0) ale88c(AL_NOHELP);
}

/*---------------------------------------------------------------------------------------
* CB_WER
*--------------------------------------------------------------------------------------*/
void CB_WER(GtkWidget *button,gpointer data)
{
extern GtkWidget *HWND; 
extern GtkWidget *MB_WER;

extern FR_INT4 LANG;
extern int     *pp;

char cmess[512],cbytes[128];

if(LANG == 1) strcpy(cmess,
"Der Z88-Commander Version 13.0\nProf.Dr.-Ing. Frank Rieg\nUniversitaet Bayreuth 2008\n\
frank.rieg@uni-bayreuth.de\nwww.z88.de\n");
if(LANG == 2) strcpy(cmess,
"The Z88-Commander Version 13.0\nProf. Frank Rieg\nUniversity of Bayreuth,Germany,2008\n\
frank.rieg@uni-bayreuth.de\nwww.z88.de\n");

#ifdef FR_XQUAD
        sprintf(cbytes,"Floats: %d Bytes\n",(int)sizeof(long double)); 
        strcat(cmess,cbytes);    
#endif
#ifdef FR_XDOUB
        sprintf(cbytes,"Floats: %d Bytes\n",(int)sizeof(double));  
        strcat(cmess,cbytes);   
#endif
#ifdef FR_XINT
        sprintf(cbytes,"Integers: %d Bytes\n",(int)sizeof(int)); 
        strcat(cmess,cbytes);
#endif
#ifdef FR_XLONG
        sprintf(cbytes,"Integers: %d Bytes\n",(int)sizeof(long));  
        strcat(cmess,cbytes); 
#endif
#ifdef FR_XLOLO
        sprintf(cbytes,"Integers: %d Bytes\n",(int)sizeof(long long));
        strcat(cmess,cbytes);   
#endif
        sprintf(cbytes,"Pointers: %d Bytes\n",(int)sizeof(pp));     
        strcat(cmess,cbytes);

MB_WER= gtk_message_dialog_new(GTK_WINDOW(HWND),GTK_DIALOG_DESTROY_WITH_PARENT,
        GTK_MESSAGE_INFO, GTK_BUTTONS_OK,cmess);
gtk_dialog_run(GTK_DIALOG(MB_WER));
gtk_widget_destroy(MB_WER);
}

/*---------------------------------------------------------------------------------------
* CB_EXIT
*--------------------------------------------------------------------------------------*/
void CB_EXIT(GtkWidget *button,gpointer data)
{
gtk_main_quit();
}

/*---------------------------------------------------------------------------------------
* CB_HELP
*--------------------------------------------------------------------------------------*/
void CB_HELP(GtkWidget *button,gpointer data)
{
extern GtkWidget *PB_HELP;
extern FR_INT4 LANG;
extern int ifhelp;

if(ifhelp == 0) 
  {
  ifhelp= 1;
  if(LANG == 1) g_object_set(PB_HELP,"label","Hilfe an",NULL);
  if(LANG == 2) g_object_set(PB_HELP,"label","Help is on",NULL);
  
  }
else  
  {
  ifhelp= 0;
  if(LANG == 1) g_object_set(PB_HELP,"label","Hilfe aus",NULL);
  if(LANG == 2) g_object_set(PB_HELP,"label","Help is off",NULL);
  }
}

/*---------------------------------------------------------------------------------------
* CB_Z88V
*--------------------------------------------------------------------------------------*/
void CB_Z88V(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88v");
  if(iret != 0) ale88c(AL_NOZ88V);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88v.htm");
  if(LANG == 2) strcat(command,"e88v.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_Z88N
*--------------------------------------------------------------------------------------*/
void CB_Z88N(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88n");
  if(iret != 0) ale88c(AL_NOZ88N);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88n.htm");
  if(LANG == 2) strcat(command,"e88n.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_Z88D
*--------------------------------------------------------------------------------------*/
void CB_Z88D(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88d");
  if(iret != 0) ale88c(AL_NOZ88D);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88d.htm");
  if(LANG == 2) strcat(command,"e88d.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_Z88E
*--------------------------------------------------------------------------------------*/
void CB_Z88E(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88e");
  if(iret != 0) ale88c(AL_NOZ88E);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88e.htm");
  if(LANG == 2) strcat(command,"e88e.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_Z88G
*--------------------------------------------------------------------------------------*/
void CB_Z88G(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88g");
  if(iret != 0) ale88c(AL_NOZ88G);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88g.htm");
  if(LANG == 2) strcat(command,"e88g.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_Z88H
*--------------------------------------------------------------------------------------*/
void CB_Z88H(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88h");
  if(iret != 0) ale88c(AL_NOZ88H);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88h.htm");
  if(LANG == 2) strcat(command,"e88h.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_Z88O
*--------------------------------------------------------------------------------------*/
void CB_Z88O(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88o");
  if(iret != 0) ale88c(AL_NOZ88O);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88o.htm");
  if(LANG == 2) strcat(command,"e88o.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_DYN
*--------------------------------------------------------------------------------------*/
void CB_DYN(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[],CEDITOR[];

char command[320];

if(ifhelp == 0)
  {
  strcpy(command,CEDITOR);
  strcat(command," z88.dyn");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOEDDYN);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88dy.htm");
  if(LANG == 2) strcat(command,"e88dy.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_FCD
*--------------------------------------------------------------------------------------*/
void CB_FCD(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[],CEDITOR[];

char command[320];

if(ifhelp == 0)
  {
  strcpy(command,CEDITOR);
  strcat(command," z88.fcd");
  iret= system(command);
  if(iret == 0)
    {
    iret= rcol88c();
    if(iret != 0)
      {
      if(LANG == 1) fprintf(stderr,"Datei Z88.FCD is ungueltig oder falsch! STOP !\n"); 
      if(LANG == 2) fprintf(stderr,"File Z88.FCD is invalid or wrong! STOP !\n");
      exit(1);
      }
    }
  else ale88c(AL_NOEDFCD);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88mr.htm");
  if(LANG == 2) strcat(command,"e88mr.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_NI
*--------------------------------------------------------------------------------------*/
void CB_NI(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[],CEDITOR[];

char command[320];

if(ifhelp == 0)
  {
  strcpy(command,CEDITOR);
  strcat(command," z88ni.txt");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOEDNI);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88ni.htm");
  if(LANG == 2) strcat(command,"e88ni.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_I1
*--------------------------------------------------------------------------------------*/
void CB_I1(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[],CEDITOR[];

char command[320];

if(ifhelp == 0)
  {
  strcpy(command,CEDITOR);
  strcat(command," z88i1.txt");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOEDI1);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88i1.htm");
  if(LANG == 2) strcat(command,"e88i1.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_I2
*--------------------------------------------------------------------------------------*/
void CB_I2(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[],CEDITOR[];

char command[320];

if(ifhelp == 0)
  {
  strcpy(command,CEDITOR);
  strcat(command," z88i2.txt");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOEDI2);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88i2.htm");
  if(LANG == 2) strcat(command,"e88i2.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_I3
*--------------------------------------------------------------------------------------*/
void CB_I3(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[],CEDITOR[];

char command[320];

if(ifhelp == 0)
  {
  strcpy(command,CEDITOR);
  strcat(command," z88i3.txt");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOEDI3);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88i3.htm");
  if(LANG == 2) strcat(command,"e88i3.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_I4
*--------------------------------------------------------------------------------------*/
void CB_I4(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[],CEDITOR[];

char command[320];

if(ifhelp == 0)
  {
  strcpy(command,CEDITOR);
  strcat(command," z88i4.txt");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOEDI4);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88i4.htm");
  if(LANG == 2) strcat(command,"e88i4.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_I5
*--------------------------------------------------------------------------------------*/
void CB_I5(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[],CEDITOR[];

char command[320];

if(ifhelp == 0)
  {
  strcpy(command,CEDITOR);
  strcat(command," z88i5.txt");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOEDI5);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88i5.htm");
  if(LANG == 2) strcat(command,"e88i5.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_O0
*--------------------------------------------------------------------------------------*/
void CB_O0(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[],CEDITOR[];

char command[320];

if(ifhelp == 0)
  {
  strcpy(command,CEDITOR);
  strcat(command," z88o0.txt");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOEDO0);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88ge.htm");
  if(LANG == 2) strcat(command,"e88ge.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_O1
*--------------------------------------------------------------------------------------*/
void CB_O1(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[],CEDITOR[];

char command[320];

if(ifhelp == 0)
  {
  strcpy(command,CEDITOR);
  strcat(command," z88o1.txt");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOEDO1);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88ge.htm");
  if(LANG == 2) strcat(command,"e88ge.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_O2
*--------------------------------------------------------------------------------------*/
void CB_O2(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[],CEDITOR[];

char command[320];

if(ifhelp == 0)
  {
  strcpy(command,CEDITOR);
  strcat(command," z88o2.txt");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOEDO2);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88ge.htm");
  if(LANG == 2) strcat(command,"e88ge.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_O3
*--------------------------------------------------------------------------------------*/
void CB_O3(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[],CEDITOR[];

char command[320];

if(ifhelp == 0)
  {
  strcpy(command,CEDITOR);
  strcat(command," z88o3.txt");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOEDO3);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88ge.htm");
  if(LANG == 2) strcat(command,"e88ge.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_O4
*--------------------------------------------------------------------------------------*/
void CB_O4(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[],CEDITOR[];

char command[320];

if(ifhelp == 0)
  {
  strcpy(command,CEDITOR);
  strcat(command," z88o4.txt");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOEDO4);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88ge.htm");
  if(LANG == 2) strcat(command,"e88ge.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_I1D
*--------------------------------------------------------------------------------------*/
void CB_I1D(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88x -i1tx");
  if(iret != 0) ale88c(AL_NOZ88X);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88x.htm");
  if(LANG == 2) strcat(command,"e88x.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_ISD
*--------------------------------------------------------------------------------------*/
void CB_ISD(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88x -iatx");
  if(iret != 0) ale88c(AL_NOZ88X);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88x.htm");
  if(LANG == 2) strcat(command,"e88x.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_NID
*--------------------------------------------------------------------------------------*/
void CB_NID(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88x -nitx");
  if(iret != 0) ale88c(AL_NOZ88X);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88x.htm");
  if(LANG == 2) strcat(command,"e88x.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_DI1
*--------------------------------------------------------------------------------------*/
void CB_DI1(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88x -i1fx");
  if(iret != 0) ale88c(AL_NOZ88X);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88x.htm");
  if(LANG == 2) strcat(command,"e88x.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_DIS
*--------------------------------------------------------------------------------------*/
void CB_DIS(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88x -iafx");
  if(iret != 0) ale88c(AL_NOZ88X);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88x.htm");
  if(LANG == 2) strcat(command,"e88x.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_DNI
*--------------------------------------------------------------------------------------*/
void CB_DNI(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88x -nifx");
  if(iret != 0) ale88c(AL_NOZ88X);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88x.htm");
  if(LANG == 2) strcat(command,"e88x.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_FC
*--------------------------------------------------------------------------------------*/
void CB_FC(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88f -c");
  if(iret != 0) ale88c(AL_NOZ88F);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88f.htm");
  if(LANG == 2) strcat(command,"e88f.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_FT
*--------------------------------------------------------------------------------------*/
void CB_FT(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88f -t");
  if(iret != 0) ale88c(AL_NOZ88F);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88f.htm");
  if(LANG == 2) strcat(command,"e88f.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_SM1
*--------------------------------------------------------------------------------------*/
void CB_SM1(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88i1");
  if(iret != 0) ale88c(AL_NOZ88I1);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88ite.htm");
  if(LANG == 2) strcat(command,"e88ite.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_I2C
*--------------------------------------------------------------------------------------*/
void CB_I2C(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88i2 -c");
  if(iret != 0) ale88c(AL_NOZ88I2);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88ite.htm");
  if(LANG == 2) strcat(command,"e88ite.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_I2S
*--------------------------------------------------------------------------------------*/
void CB_I2S(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88i2 -s");
  if(iret != 0) ale88c(AL_NOZ88I2);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88ite.htm");
  if(LANG == 2) strcat(command,"e88ite.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}

/*---------------------------------------------------------------------------------------
* CB_PAR
*--------------------------------------------------------------------------------------*/
void CB_PAR(GtkWidget *button,gpointer data)
{
extern FR_INT4 LANG;
extern int     ifhelp,iret;
extern char    CBROWSER[],CPREFIX[];

char command[320];

if(ifhelp == 0) 
  {
  iret= system("z88par");
  if(iret != 0) ale88c(AL_NOZ88PAR);
  }
else
  {
  strcpy(command,CBROWSER);
  strcat(command," ");
  strcat(command,CPREFIX);
  if(LANG == 1) strcat(command,"g88ite.htm");
  if(LANG == 2) strcat(command,"e88ite.htm");
  iret= system(command);
  if(iret != 0) ale88c(AL_NOHELP);
  }
}





