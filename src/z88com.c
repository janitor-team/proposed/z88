/****************************************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the Windows & UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V13.0  February 14, 2008
*
* Z88 should compile and run under any Windows and UNIX OS.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
****************************************************************************************/ 
/****************************************************************************************
* Z88COM fuer X11 und gtk+
* 8.5.2008 Rieg
****************************************************************************************/

/****************************************************************************************
* Includes
****************************************************************************************/
/*---------------------------------------------------------------------------------------
* UNIX
*--------------------------------------------------------------------------------------*/
#include <z88com.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/****************************************************************************************
* globale Variable, speziell fuer X11
****************************************************************************************/
GtkWidget *HWND; 
GtkWidget *LB_UEBER,*LB_EIN,*LB_PPP,*LB_AUS,*LB_DXF,*LB_SOL;
GtkWidget *PB_INTR,*PB_WER,*PB_EXIT,*PB_HELP;
GtkWidget *PB_Z88V,*PB_Z88N,*PB_Z88D,*PB_Z88E,*PB_Z88G,*PB_Z88H,*PB_Z88O;
GtkWidget *BOX_FRAME,*BOX_UEBER,*BOX_OBEN,*BOX_EIN,*BOX_PPP,*BOX_AUS,*BOX_DXF,*BOX_SOL;
GtkWidget *PB_DYN,*PB_FCD,*PB_NI,*PB_I1,*PB_I2,*PB_I3,*PB_I4,*PB_I5;
GtkWidget *PB_O0,*PB_O1,*PB_O2,*PB_O3,*PB_O4;
GtkWidget *PB_I1D,*PB_ISD,*PB_NID,*PB_DI1,*PB_DIS,*PB_DNI;
GtkWidget *PB_FC,*PB_FT,*PB_SM1,*PB_I2C,*PB_I2S,*PB_PAR;
GtkWidget *HSEP1,*HSEP2,*HSEP3,*HSEP4,*HSEP5,*HSEP6;

GtkWidget *MB_WER;

GtkTooltips *TT_INTR,*TT_HELP,*TT_Z88V,*TT_Z88N,*TT_Z88D,*TT_Z88E,*TT_Z88G,*TT_Z88H,*TT_Z88O;
GtkTooltips *TT_DYN,*TT_FCD,*TT_NI,*TT_I1,*TT_I2,*TT_I3,*TT_I4,*TT_I5;
GtkTooltips *TT_O0,*TT_O1,*TT_O2,*TT_O3,*TT_O4;
GtkTooltips *TT_I1D,*TT_ISD,*TT_NID,*TT_DI1,*TT_DIS,*TT_DNI;
GtkTooltips *TT_FC,*TT_FT,*TT_SM1,*TT_I2C,*TT_I2S,*TT_PAR;

gint       ispace= 3;

FILE       *fdyn,*fwlo;

FR_INT4    LANG= 2;

char       CEDITOR[128],CBROWSER[128],CPREFIX[128];

char       cdyn[12] = "z88.dyn";  
char       clgd[12] = "z88com.log";

int        ifhelp= 0,iret;
int        *pp;

/****************************************************************************************
*  Function-Declarationen
****************************************************************************************/
void CB_INTR   (GtkWidget *,gpointer);
void CB_WER    (GtkWidget *,gpointer);
void CB_EXIT   (GtkWidget *,gpointer);
void CB_HELP   (GtkWidget *,gpointer);
void CB_Z88V   (GtkWidget *,gpointer);
void CB_Z88N   (GtkWidget *,gpointer);
void CB_Z88D   (GtkWidget *,gpointer);
void CB_Z88E   (GtkWidget *,gpointer);
void CB_Z88G   (GtkWidget *,gpointer);
void CB_Z88H   (GtkWidget *,gpointer);
void CB_Z88O   (GtkWidget *,gpointer);
void CB_Z88X   (GtkWidget *,gpointer);
void CB_Z88F   (GtkWidget *,gpointer);
void CB_DYN    (GtkWidget *,gpointer);
void CB_FCD    (GtkWidget *,gpointer);
void CB_NI     (GtkWidget *,gpointer);
void CB_I1     (GtkWidget *,gpointer);
void CB_I2     (GtkWidget *,gpointer);
void CB_I3     (GtkWidget *,gpointer);
void CB_I4     (GtkWidget *,gpointer);
void CB_I5     (GtkWidget *,gpointer);
void CB_O0     (GtkWidget *,gpointer);
void CB_O1     (GtkWidget *,gpointer);
void CB_O2     (GtkWidget *,gpointer);
void CB_O3     (GtkWidget *,gpointer);
void CB_O4     (GtkWidget *,gpointer);
void CB_I1D    (GtkWidget *,gpointer);
void CB_ISD    (GtkWidget *,gpointer);
void CB_NID    (GtkWidget *,gpointer);
void CB_DI1    (GtkWidget *,gpointer);
void CB_DIS    (GtkWidget *,gpointer);
void CB_DNI    (GtkWidget *,gpointer);
void CB_FC     (GtkWidget *,gpointer);
void CB_FT     (GtkWidget *,gpointer);
void CB_SM1    (GtkWidget *,gpointer);
void CB_I2C    (GtkWidget *,gpointer);
void CB_I2S    (GtkWidget *,gpointer);
void CB_PAR    (GtkWidget *,gpointer);

int  lan88c(void);
int  rcol88c(void);

/****************************************************************************************
*  Hauptprogramm
****************************************************************************************/
int main(int argc, char *argv[])
  {
  char cstring[128];

/*---------------------------------------------------------------------------------------
*  LAN88C starten
*--------------------------------------------------------------------------------------*/
iret= lan88c();

if(iret == AL_NOLOG)
  {
  fprintf(stderr,"Cannot open file Z88COM.LOG ! STOP !\n"); 
  fclose(fwlo);
  exit(1); 
  }
if(iret == AL_NODYN)
  {
  fprintf(stderr,"Cannot open file Z88.DYN ! STOP !\n"); 
  fclose(fwlo);
  exit(1); 
  }
if(iret == AL_WRONGDYN)
  {
  fprintf(stderr,"File Z88.DYN is invalid or wrong! STOP !\n"); 
  fclose(fwlo);
  exit(1); 
  }

/*---------------------------------------------------------------------------------------
*  Z88.FCD einlesen
*--------------------------------------------------------------------------------------*/
iret= rcol88c();

if(iret != 0)
  {
  if(LANG == 1) fprintf(stderr,"Datei Z88.FCD is ungueltig oder falsch! STOP !\n"); 
  if(LANG == 2) fprintf(stderr,"File Z88.FCD is invalid or wrong! STOP !\n");

  fclose(fwlo);
  exit(1);
  }

/*---------------------------------------------------------------------------------------
*  Hauptfenster
*--------------------------------------------------------------------------------------*/
  gtk_init(&argc,&argv);

  HWND=   gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title   (GTK_WINDOW(HWND),"Z88COM V13");
  gtk_window_set_position(GTK_WINDOW(HWND),GTK_WIN_POS_CENTER);
  g_signal_connect       (GTK_OBJECT(HWND),"destroy",GTK_SIGNAL_FUNC(CB_EXIT),NULL);

/*---------------------------------------------------------------------------------------
*  Ueberschrift
*--------------------------------------------------------------------------------------*/
  if(LANG == 1) strcpy(cstring,"Der Z88-Commander Version 13.0");
  if(LANG == 2) strcpy(cstring,"The Z88-Commander Version 13.0");
  LB_UEBER= gtk_label_new(cstring); 

  HSEP1= gtk_hseparator_new();

/*---------------------------------------------------------------------------------------
*  Buttons erzeugen + Callbacks obere Reihe
*--------------------------------------------------------------------------------------*/
  if(LANG == 1) strcpy(cstring,"Ueberblick");
  if(LANG == 2) strcpy(cstring,"Overview");
  PB_INTR= gtk_button_new_with_label(cstring);
  g_signal_connect(GTK_OBJECT(PB_INTR),"clicked",GTK_SIGNAL_FUNC(CB_INTR),"Intro");
  TT_INTR= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Aufrufen der Online-Hilfe fuer einen Ueberblick");
  if(LANG == 2) strcpy(cstring,"Call the Online Help for an Overview");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_INTR),PB_INTR,cstring,NULL);
 
  if(LANG == 1) strcpy(cstring,"Info");
  if(LANG == 2) strcpy(cstring,"About");
  PB_WER= gtk_button_new_with_label(cstring);
  g_signal_connect(GTK_OBJECT(PB_WER),"clicked",GTK_SIGNAL_FUNC(CB_WER),"Info");

  if(LANG == 1) strcpy(cstring,"Ende");
  if(LANG == 2) strcpy(cstring,"Exit");
  PB_EXIT= gtk_button_new_with_label(cstring);
  g_signal_connect(GTK_OBJECT(PB_EXIT),"clicked",GTK_SIGNAL_FUNC(CB_EXIT),"Exit");
 
  if(LANG == 1) strcpy(cstring,"Hilfe aus");
  if(LANG == 2) strcpy(cstring,"Help is off");
  PB_HELP= gtk_button_new_with_label(cstring);
  g_signal_connect(GTK_OBJECT(PB_HELP),"clicked",GTK_SIGNAL_FUNC(CB_HELP),"Hilfe");
  TT_HELP= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Umschalten Hilfemode - Befehlsmode");
  if(LANG == 2) strcpy(cstring,"Toggle Help Mode - Command Mode");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_HELP),PB_HELP,cstring,NULL);

  PB_DYN= gtk_button_new_with_label("Z88.DYN");
  g_signal_connect(GTK_OBJECT(PB_DYN),"clicked",GTK_SIGNAL_FUNC(CB_DYN),"Z88.DYN");
  TT_DYN= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Speicher-Steuerdatei Z88.DYN editieren");
  if(LANG == 2) strcpy(cstring,"Edit the Memory Definition File Z88.DYN");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_DYN),PB_DYN,cstring,NULL);

  PB_FCD= gtk_button_new_with_label("Z88.FCD");
  g_signal_connect(GTK_OBJECT(PB_FCD),"clicked",GTK_SIGNAL_FUNC(CB_FCD),"Z88.FCD");
  TT_FCD= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Datei der Einstellungen Z88 fuer UNIX Z88.FCD editieren");
  if(LANG == 2) strcpy(cstring,"Edit the Defaults for UNIX File Z88.FCD");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_FCD),PB_FCD,cstring,NULL);

  HSEP2= gtk_hseparator_new();

/*---------------------------------------------------------------------------------------
*  Buttons erzeugen + Callbacks: Eingabedateien
*--------------------------------------------------------------------------------------*/
  if(LANG == 1) strcpy(cstring,"Editieren der Eingabedateien");
  if(LANG == 2) strcpy(cstring,"Edit the Input Files");
  LB_EIN= gtk_label_new(cstring); 

  PB_NI= gtk_button_new_with_label("Z88NI.TXT");
  g_signal_connect(GTK_OBJECT(PB_NI),"clicked",GTK_SIGNAL_FUNC(CB_NI),"Z88NI.TXT");
  TT_NI= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Netzgenerator-Eingabedatei Z88NI.TXT editieren");
  if(LANG == 2) strcpy(cstring,"Edit the Mesh Genrator Input File Z88NI.TXT");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_NI),PB_NI,cstring,NULL);

  PB_I1= gtk_button_new_with_label("Z88I1.TXT");
  g_signal_connect(GTK_OBJECT(PB_I1),"clicked",GTK_SIGNAL_FUNC(CB_I1),"Z88I1.TXT");
  TT_I1= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"allgemeine Strukturdaten Z88I1.TXT editieren");
  if(LANG == 2) strcpy(cstring,"Edit the Structure File Z88I1.TXT");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_I1),PB_I1,cstring,NULL);

  PB_I2= gtk_button_new_with_label("Z88I2.TXT");
  g_signal_connect(GTK_OBJECT(PB_I2),"clicked",GTK_SIGNAL_FUNC(CB_I2),"Z88I2.TXT");
  TT_I2= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Randbedingungen Z88I2.TXT editieren");
  if(LANG == 2) strcpy(cstring,"Edit the Boundary Condition File Z88I2.TXT");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_I2),PB_I2,cstring,NULL);

  PB_I3= gtk_button_new_with_label("Z88I3.TXT");
  g_signal_connect(GTK_OBJECT(PB_I3),"clicked",GTK_SIGNAL_FUNC(CB_I3),"Z88I3.TXT");
  TT_I3= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Steuerdatei Z88I3.TXT fuer die Spannungsberechnung editieren");
  if(LANG == 2) strcpy(cstring,"Edit the Flag File for Stresses Z88I3.TXT");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_I3),PB_I3,cstring,NULL);

  PB_I4= gtk_button_new_with_label("Z88I4.TXT");
  g_signal_connect(GTK_OBJECT(PB_I4),"clicked",GTK_SIGNAL_FUNC(CB_I4),"Z88I4.TXT");
  TT_I4= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Steuerdatei Z88I4.TXT fuer die Solver editieren");
  if(LANG == 2) strcpy(cstring,"Edit the Flag File for the Solvers Z88I4.TXT");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_I4),PB_I4,cstring,NULL);

  PB_I5= gtk_button_new_with_label("Z88I5.TXT");
  g_signal_connect(GTK_OBJECT(PB_I5),"clicked",GTK_SIGNAL_FUNC(CB_I5),"Z88I5.TXT");
  TT_I5= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Datei der Oberflaechenlasten Z88I5.TXT editieren");
  if(LANG == 2) strcpy(cstring,"Edit the File for Surface Loads Z88I5.TXT");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_I5),PB_I5,cstring,NULL);

  HSEP3= gtk_hseparator_new();

/*---------------------------------------------------------------------------------------
*  Buttons erzeugen + Callbacks: Prae- und Postprozessoren
*--------------------------------------------------------------------------------------*/
  if(LANG == 1) strcpy(cstring,"Prae- und Postprozessoren");
  if(LANG == 2) strcpy(cstring,"Pre- and Postprocessors");
  LB_PPP= gtk_label_new(cstring); 

  PB_Z88V= gtk_button_new_with_label("Z88V");
  g_signal_connect(GTK_OBJECT(PB_Z88V),"clicked",GTK_SIGNAL_FUNC(CB_Z88V),"Z88V");
  TT_Z88V= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Der Filechecker");
  if(LANG == 2) strcpy(cstring,"The File Checker");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_Z88V),PB_Z88V,cstring,NULL);

  PB_Z88N= gtk_button_new_with_label("Z88N");
  g_signal_connect(GTK_OBJECT(PB_Z88N),"clicked",GTK_SIGNAL_FUNC(CB_Z88N),"Z88N");
  TT_Z88N= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Der Netzgenerator");
  if(LANG == 2) strcpy(cstring,"The Mesh Generator");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_Z88N),PB_Z88N,cstring,NULL);

  PB_Z88D= gtk_button_new_with_label("Z88D");
  g_signal_connect(GTK_OBJECT(PB_Z88D),"clicked",GTK_SIGNAL_FUNC(CB_Z88D),"Z88D");
  TT_Z88D= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Die Spannungsberechnung");
  if(LANG == 2) strcpy(cstring,"Compute Stresses");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_Z88D),PB_Z88D,cstring,NULL);

  PB_Z88E= gtk_button_new_with_label("Z88E");
  g_signal_connect(GTK_OBJECT(PB_Z88E),"clicked",GTK_SIGNAL_FUNC(CB_Z88E),"Z88E");
  TT_Z88E= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Die Knotenkraftberechnung");
  if(LANG == 2) strcpy(cstring,"Compute Nodal Forces");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_Z88E),PB_Z88E,cstring,NULL);

  PB_Z88O= gtk_button_new_with_label("Z88O");
  g_signal_connect(GTK_OBJECT(PB_Z88O),"clicked",GTK_SIGNAL_FUNC(CB_Z88O),"Z88O");
  TT_Z88O= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Das OpenGL-Plotprogramm");
  if(LANG == 2) strcpy(cstring,"The OpenGL Plot Program");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_Z88O),PB_Z88O,cstring,NULL);

  HSEP4= gtk_hseparator_new();

/*---------------------------------------------------------------------------------------
*  Buttons erzeugen + Callbacks vierte Reihe: Konverter
*--------------------------------------------------------------------------------------*/
  if(LANG == 1) strcpy(cstring,"CAD-Konverter");
  if(LANG == 2) strcpy(cstring,"CAD Converters");
  LB_DXF= gtk_label_new(cstring); 

  PB_Z88G= gtk_button_new_with_label("Z88G");
  g_signal_connect(GTK_OBJECT(PB_Z88G),"clicked",GTK_SIGNAL_FUNC(CB_Z88G),"Z88G");
  TT_Z88G= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Der NASTRAN/COSMOS-Konverter");
  if(LANG == 2) strcpy(cstring,"The NASTRAN/COSMOS Converter");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_Z88G),PB_Z88G,cstring,NULL);

  PB_Z88H= gtk_button_new_with_label("Z88H");
  g_signal_connect(GTK_OBJECT(PB_Z88H),"clicked",GTK_SIGNAL_FUNC(CB_Z88H),"Z88H");
  TT_Z88H= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Der Cuthill-MCKee-Umnummerierer");
  if(LANG == 2) strcpy(cstring,"The Cuthill-McKee Program");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_Z88H),PB_Z88H,cstring,NULL);

  PB_I1D= gtk_button_new_with_label("Z88I1>DXF");
  g_signal_connect(GTK_OBJECT(PB_I1D),"clicked",GTK_SIGNAL_FUNC(CB_I1D),"Z88I1>DXF");
  TT_I1D= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"von Z88I1.TXT nach Z88X.DXF");
  if(LANG == 2) strcpy(cstring,"from Z88I1.TXT to Z88X.DXF");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_I1D),PB_I1D,cstring,NULL);

  PB_ISD= gtk_button_new_with_label("Z88I*>DXF");
  g_signal_connect(GTK_OBJECT(PB_ISD),"clicked",GTK_SIGNAL_FUNC(CB_ISD),"Z88I*>DXF");
  TT_ISD= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"von Z88I*.TXT nach Z88X.DXF");
  if(LANG == 2) strcpy(cstring,"from Z88I*.TXT to Z88X.DXF");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_ISD),PB_ISD,cstring,NULL);

  PB_NID= gtk_button_new_with_label("Z88NI>DXF");
  g_signal_connect(GTK_OBJECT(PB_NID),"clicked",GTK_SIGNAL_FUNC(CB_NID),"Z88NI>DXF");
  TT_NID= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"von Z88NI.TXT nach Z88X.DXF");
  if(LANG == 2) strcpy(cstring,"from Z88NI.TXT to Z88X.DXF");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_NID),PB_NID,cstring,NULL);

  PB_DI1= gtk_button_new_with_label("DXF>Z88I1");
  g_signal_connect(GTK_OBJECT(PB_DI1),"clicked",GTK_SIGNAL_FUNC(CB_DI1),"DXF>Z88I1");
  TT_DI1= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"von Z88X.DXF nach Z88I1.TXT");
  if(LANG == 2) strcpy(cstring,"from Z88X.DXF to Z88I1.TXT");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_DI1),PB_DI1,cstring,NULL);

  PB_DIS= gtk_button_new_with_label("DXF>Z88I*");
  g_signal_connect(GTK_OBJECT(PB_DIS),"clicked",GTK_SIGNAL_FUNC(CB_DIS),"DXF>Z88I*");
  TT_DIS= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"von Z88X.DXF nach Z88I*.TXT");
  if(LANG == 2) strcpy(cstring,"from Z88X.DXF to Z88I*.TXT");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_DIS),PB_DIS,cstring,NULL);

  PB_DNI= gtk_button_new_with_label("DXF>Z88NI");
  g_signal_connect(GTK_OBJECT(PB_DNI),"clicked",GTK_SIGNAL_FUNC(CB_DNI),"DXF>Z88NI");
  TT_DNI= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"von Z88X.DXF nach Z88NI.TXT");
  if(LANG == 2) strcpy(cstring,"from Z88X.DXF to Z88NI.TXT");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_DNI),PB_DNI,cstring,NULL);

  HSEP5= gtk_hseparator_new();

/*---------------------------------------------------------------------------------------
*  Buttons erzeugen + Callbacks: Solver
*--------------------------------------------------------------------------------------*/
  if(LANG == 1) strcpy(cstring,"Die Solver");
  if(LANG == 2) strcpy(cstring,"The Solvers");
  LB_SOL= gtk_label_new(cstring); 

  PB_FC= gtk_button_new_with_label("Z88F -C");
  g_signal_connect(GTK_OBJECT(PB_FC),"clicked",GTK_SIGNAL_FUNC(CB_FC),"Z88F-C");
  TT_FC= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Der Cholesky-Solver fuer kleine Strukturen, Rechenmode");
  if(LANG == 2) strcpy(cstring,"The Cholesky Solver for small structures, Compute Mode");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_FC),PB_FC,cstring,NULL);

  PB_FT= gtk_button_new_with_label("Z88F -T");
  g_signal_connect(GTK_OBJECT(PB_FT),"clicked",GTK_SIGNAL_FUNC(CB_FT),"Z88F-T");
  TT_FT= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Der Cholesky-Solver fuer kleine Strukturen, Testmode");
  if(LANG == 2) strcpy(cstring,"The Cholesky Solver for small structures, Test Mode");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_FT),PB_FT,cstring,NULL);

  PB_SM1= gtk_button_new_with_label("Z88I1");
  g_signal_connect(GTK_OBJECT(PB_SM1),"clicked",GTK_SIGNAL_FUNC(CB_SM1),"Z88SM1");
  TT_SM1= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Der Sparsematrix-Solver Part 1 fuer Z88I2 und Z88PAR");
  if(LANG == 2) strcpy(cstring,"The Sparse Matrix Solver Part 1 for Z88I2 and Z88PAR");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_SM1),PB_SM1,cstring,NULL);

  PB_I2C= gtk_button_new_with_label("Z88I2 -C");
  g_signal_connect(GTK_OBJECT(PB_I2C),"clicked",GTK_SIGNAL_FUNC(CB_I2C),"Z88I2C");
  TT_I2C= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,
  "Der Sparsematrix-Solver Part 2 fuer grosse Strukturen: Z88I2 mit SIC Vorkonditionierung");
  if(LANG == 2) strcpy(cstring,
  "The Sparse Matrix Solver Part 2 for large structures: Z88I2 with SIC preconditioner");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_I2C),PB_I2C,cstring,NULL);

  PB_I2S= gtk_button_new_with_label("Z88I2 -S");
  g_signal_connect(GTK_OBJECT(PB_I2S),"clicked",GTK_SIGNAL_FUNC(CB_I2S),"Z88I2S");
  TT_I2S= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,
  "Der Sparsematrix-Solver Part 2 fuer grosse Strukturen: Z88I2 mit SOR Vorkonditionierung");
  if(LANG == 2) strcpy(cstring,
  "The Sparse Matrix Solver Part 2 for large structures: Z88I2 with SOR preconditioner");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_I2S),PB_I2S,cstring,NULL);

  PB_PAR= gtk_button_new_with_label("Z88PAR");
  g_signal_connect(GTK_OBJECT(PB_PAR),"clicked",GTK_SIGNAL_FUNC(CB_PAR),"Z88PAR");
  TT_PAR= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Der Sparsematrix-Solver Part 2 fuer mittlere Strukturen: Z88PAR");
  if(LANG == 2) strcpy(cstring,"The Sparse Matrix Solver Part 2 for medium sized structures: Z88PAR");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_PAR),PB_PAR,cstring,NULL);

  HSEP6= gtk_hseparator_new();

/*---------------------------------------------------------------------------------------
*  Buttons erzeugen + Callbacks: Ausgabedateien
*--------------------------------------------------------------------------------------*/
  if(LANG == 1) strcpy(cstring,"Editieren der Ausgabedateien");
  if(LANG == 2) strcpy(cstring,"Edit the Output Files");
  LB_AUS= gtk_label_new(cstring); 

  PB_O0= gtk_button_new_with_label("Z88O0.TXT");
  g_signal_connect(GTK_OBJECT(PB_O0),"clicked",GTK_SIGNAL_FUNC(CB_O0),"Z88O0.TXT");
  TT_O0= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Aufbereitete Strukturdaten Z88O0.TXT editieren");
  if(LANG == 2) strcpy(cstring,"Edit Processed Input Data Z88O0.TXT for documentation");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_O0),PB_O0,cstring,NULL);

  PB_O1= gtk_button_new_with_label("Z88O1.TXT");
  g_signal_connect(GTK_OBJECT(PB_O1),"clicked",GTK_SIGNAL_FUNC(CB_O1),"Z88O1.TXT");
  TT_O1= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Aufbereitete Randbedingungen Z88O1.TXT editieren");
  if(LANG == 2) strcpy(cstring,"Edit Processed Boundary Conditions Z88O1.TXT for documentation");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_O1),PB_O1,cstring,NULL);

  PB_O2= gtk_button_new_with_label("Z88O2.TXT");
  g_signal_connect(GTK_OBJECT(PB_O2),"clicked",GTK_SIGNAL_FUNC(CB_O2),"Z88O2.TXT");
  TT_O2= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Berechnete Verschiebungen Z88O2.TXT editieren");
  if(LANG == 2) strcpy(cstring,"Edit Computed Displacements Z88O2.TXT");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_O2),PB_O2,cstring,NULL);

  PB_O3= gtk_button_new_with_label("Z88O3.TXT");
  g_signal_connect(GTK_OBJECT(PB_O3),"clicked",GTK_SIGNAL_FUNC(CB_O3),"Z88O3.TXT");
  TT_O3= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Berechnete Spannungen Z88O3.TXT editieren");
  if(LANG == 2) strcpy(cstring,"Edit Computed Stresses Z88O3.TXT");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_O3),PB_O3,cstring,NULL);

  PB_O4= gtk_button_new_with_label("Z88O4.TXT");
  g_signal_connect(GTK_OBJECT(PB_O4),"clicked",GTK_SIGNAL_FUNC(CB_O4),"Z88O4.TXT");
  TT_O4= gtk_tooltips_new();
  if(LANG == 1) strcpy(cstring,"Berechnete Knotenkraefte Z88O4.TXT editieren");
  if(LANG == 2) strcpy(cstring,"Edit Computed Nodal Forces Z88O4.TXT");
  gtk_tooltips_set_tip(GTK_TOOLTIPS(TT_O4),PB_O4,cstring,NULL);

/*---------------------------------------------------------------------------------------
*  Buttons anordnen
*--------------------------------------------------------------------------------------*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*  Boxen erzeugen
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  BOX_FRAME = gtk_vbox_new(FALSE,ispace);
  BOX_UEBER = gtk_hbox_new(TRUE, ispace);
  BOX_OBEN  = gtk_hbox_new(TRUE, ispace);
  BOX_EIN   = gtk_hbox_new(TRUE, ispace);
  BOX_PPP   = gtk_hbox_new(TRUE, ispace);
  BOX_DXF   = gtk_hbox_new(TRUE, ispace);
  BOX_SOL   = gtk_hbox_new(TRUE, ispace);
  BOX_AUS   = gtk_hbox_new(TRUE, ispace);

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*  Ueberschrift
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  gtk_box_pack_start(GTK_BOX(BOX_UEBER),LB_UEBER,    TRUE, TRUE, ispace);
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),BOX_UEBER,   TRUE, FALSE,ispace);
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),HSEP1,       TRUE, FALSE,ispace);

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*  obere Buttonreihe
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  gtk_box_pack_start(GTK_BOX(BOX_OBEN), PB_INTR,    TRUE, TRUE, ispace);
  gtk_box_pack_start(GTK_BOX(BOX_OBEN), PB_WER,     TRUE, TRUE, ispace);
  gtk_box_pack_start(GTK_BOX(BOX_OBEN), PB_EXIT,    TRUE, TRUE, ispace);
  gtk_box_pack_start(GTK_BOX(BOX_OBEN), PB_HELP,    TRUE, TRUE, ispace);
  gtk_box_pack_start(GTK_BOX(BOX_OBEN), PB_DYN,     TRUE, TRUE, ispace);
  gtk_box_pack_start(GTK_BOX(BOX_OBEN), PB_FCD,     TRUE, TRUE, ispace);
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),BOX_OBEN,   TRUE, FALSE,ispace);
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),HSEP2,      TRUE, FALSE,ispace);

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*  Buttonreihe fuer Eingabe
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),LB_EIN,     TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_EIN),  PB_NI,      TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_EIN),  PB_I1 ,     TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_EIN),  PB_I2 ,     TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_EIN),  PB_I3 ,     TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_EIN),  PB_I4 ,     TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_EIN),  PB_I5 ,     TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),BOX_EIN,    TRUE, FALSE, ispace);
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),HSEP3 ,     TRUE, FALSE, ispace);

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*  Buttonreihe fuer Eingabe
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),LB_PPP,     TRUE, TRUE, ispace);
  gtk_box_pack_start(GTK_BOX(BOX_PPP),  PB_Z88V,    TRUE, TRUE, ispace);
  gtk_box_pack_start(GTK_BOX(BOX_PPP),  PB_Z88N,    TRUE, TRUE, ispace);
  gtk_box_pack_start(GTK_BOX(BOX_PPP),  PB_Z88D,    TRUE, TRUE, ispace);
  gtk_box_pack_start(GTK_BOX(BOX_PPP),  PB_Z88E,    TRUE, TRUE, ispace);
  gtk_box_pack_start(GTK_BOX(BOX_PPP),  PB_Z88O,    TRUE, TRUE, ispace);
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),BOX_PPP,    TRUE, FALSE,ispace);
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),HSEP4 ,     TRUE, FALSE,ispace);

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*  Buttonreihe fuer DXF <-> Z88
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),LB_DXF,     TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_DXF),  PB_Z88G,    TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_DXF),  PB_Z88H,    TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_DXF),  PB_I1D,     TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_DXF),  PB_ISD,     TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_DXF),  PB_NID,     TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_DXF),  PB_DI1,     TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_DXF),  PB_DIS,     TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_DXF),  PB_DNI,     TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),BOX_DXF,    TRUE, FALSE, ispace);
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),HSEP5 ,     TRUE, FALSE, ispace);

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*  Buttonreihe fuer Solver
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),LB_SOL,    TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_SOL),  PB_FC,     TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_SOL),  PB_FT,     TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_SOL),  PB_SM1,    TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_SOL),  PB_I2C,    TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_SOL),  PB_I2S,    TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_SOL),  PB_PAR,    TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),BOX_SOL,   TRUE, FALSE, ispace);
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),HSEP6 ,    TRUE, FALSE, ispace);

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*  Buttonreihe fuer Ausgabe
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),LB_AUS,     TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_AUS),  PB_O0,      TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_AUS),  PB_O1,      TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_AUS),  PB_O2,      TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_AUS),  PB_O3,      TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_AUS),  PB_O4,      TRUE, TRUE,  ispace);
  gtk_box_pack_start(GTK_BOX(BOX_FRAME),BOX_AUS,    TRUE, FALSE, ispace);

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*  BOX_FRAME in HWND
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  gtk_container_add(GTK_CONTAINER(HWND),BOX_FRAME);
  gtk_widget_show_all(HWND);

  gtk_main();

  return 0;
  }

