/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V13.0 February 14, 2008
*
* Z88 should compile and run under any UNIX OS and Motif 2.0.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/****************************************************************************
*  Programm z88i1.c - der 1.Teil des Z88-Sparse-Solvers
*  14.2.08 Rieg
****************************************************************************/

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88i.h>  
#include <stdio.h>    /* FILE */
#endif

/****************************************************************************
*  Function-Declarationen
****************************************************************************/
int dyn88i1(void);
int ale88i(int);
int who88i1(void);
int ri188i(void);
int z88ai(void);
int wria88i(void);
int w4y88i(void);
int lan88i1(void);
int wrim88i(FR_INT4,int);
void stop88i(void);

/****************************************************************************
*  globale Variable
****************************************************************************/
/*--------------------------------------------------------------------------
* Files
*-------------------------------------------------------------------------*/
FILE *fdyn,*fl1,*fi1,*f1y,*f4y,*fo0,*fo1;

/*  
**   fdyn= z88.dyn
**   fl1 = z88i1.log
**   fi1=  z88i1.txt
**   f1y=  z88o1.bny
**   f4y=  z88o4.bny
**   fo0=  z88o0.txt
**   fo1=  z88o1.txt
*/ 

char cdyn[8] = "z88.dyn";
char cl1[10] = "z88i1.log";
char ci1[10] = "z88i1.txt";
char c1y[10] = "z88o1.bny";
char c4y[10] = "z88o4.bny";
char co0[10] = "z88o0.txt";
char co1[10] = "z88o1.txt";
  
/*--------------------------------------------------------------------------
* Pointer
*-------------------------------------------------------------------------*/
FR_DOUBLEAY x;
FR_DOUBLEAY y;
FR_DOUBLEAY z;
FR_DOUBLEAY emod;
FR_DOUBLEAY rnue;
FR_DOUBLEAY qpara;
FR_DOUBLEAY riyy;
FR_DOUBLEAY eyy;
FR_DOUBLEAY rizz;
FR_DOUBLEAY ezz;
FR_DOUBLEAY rit;
FR_DOUBLEAY wt;

FR_INT4AY ip;
FR_INT4AY iez;
FR_INT4AY koi;
FR_INT4AY ifrei; 
FR_INT4AY ioffs;
FR_INT4AY koffs;
FR_INT4AY ityp;
FR_INT4AY ivon;
FR_INT4AY ibis;
FR_INT4AY intord;

/*--------------------------------------------------------------------------
* Arrays
*-------------------------------------------------------------------------*/
FR_INT4 mcomp[21];                     /* 21 ist MAXPA */

/*--------------------------------------------------------------------------
* Variable
*-------------------------------------------------------------------------*/
FR_DOUBLE emode,rnuee,qparae,riyye,eyye,rizze,ezze,rite,wte,eps,rp;

FR_INT4 kfoun,intore,nel,ktyp,maxit;
FR_INT4 LANG,IDYNMEM;
FR_INT4 ndim,nkp,ne,nfg,neg,nfgp1,nkoi,kflag,ibflag,ipflag,iqflag;

/*--------------------------------------------------------------------------
* vorbelegte Variable
*-------------------------------------------------------------------------*/
FR_INT4 MAXIEZ=0,MAXGS=0,MAXNFG=0,MAXK=0,MAXE=0,MAXKOI=0,MAXNEG=0;

/*****************************************************************************
* hauptprogramm
*****************************************************************************/
int main(void)
{
int iret;

/*---------------------------------------------------------------------------
*  Die Sprache feststellen
*--------------------------------------------------------------------------*/
LANG = 0;
iret= lan88i1();

if(iret != 0)
  {
  ale88i(iret);
  stop88i();
  }

/*---------------------------------------------------------------------------
*  Ueberschrift
*--------------------------------------------------------------------------*/
who88i1();

/*---------------------------------------------------------------------------
* dyn88i1 starten: anfordern dynamic memory
*--------------------------------------------------------------------------*/
iret= dyn88i1();
if(iret != 0)
  {
  ale88i(iret);
  stop88i();              
  }           

/*===========================================================================
* Einlesen Z88I1.TXT
*==========================================================================*/
iret= ri188i();
if(iret != 0)
  {
  ale88i(iret);
  stop88i();              
  }           
  
/*===========================================================================
* Rechnen
*==========================================================================*/
iret= z88ai();
if(iret != 0)
  {
  ale88i(iret);
  stop88i();              
  }           

iret= wria88i();                   /* Schreiben Z88O1.TXT und Z88O1.BNY */ 
if(iret != 0)
  {
  ale88i(iret);
  stop88i();              
  }           

iret= w4y88i();                    /* Schreiben Z88O4.BNY */ 
if(iret != 0)
  {
  ale88i(iret);
  stop88i();              
  }

wrim88i(0,TX_ENDI1);
return(0);
}           

