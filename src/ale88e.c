/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V10.0  December 12, 2001
*
* Z88 should compile and run under any UNIX OS and Motif 2.0.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/***********************************************************************
*  function ale88e gibt Fehlermeldungen aus
*  19.3.2002 Rieg
***********************************************************************/ 

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88e.h>
#include <stdio.h>    /* printf */
#endif

/***********************************************************************
*  hier beginnt Function ale88e
***********************************************************************/
int ale88e(int ialert)
{
extern FR_INT4 LANG;

switch(ialert)
  {
  case AL_NOLOG:
    if(LANG == 1) printf("### kann Z88F.LOG nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88F.LOG ..stop ###\n");
    break;
  case AL_NODYN:
    if(LANG == 1) printf("### kann Z88.DYN nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88.DYN ..stop ###\n");
    break;
  case AL_WRONGDYN:
    if(LANG == 1) printf("### File Z88.DYN ist nicht korrekt ..Stop ###\n");
    if(LANG == 2) printf("### file Z88.DYN is not correct ..stop ###\n");
    break;
  case AL_NOMEMY:
    if(LANG == 1)printf("### nicht genuegend dynamisches Memory ..Stop ###\n");
    if(LANG == 2)printf("### insufficient dynamic memory ..stop ###\n");
    break;
  case AL_NOO4:
    if(LANG == 1) printf("### kann Z88O4.TXT nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88O4.TXT ..stop ###\n");
    break;
  case AL_NO1Y:
    if(LANG == 1) printf("### kann Z88O1.BNY nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88O1.BNY ..stop ###\n");
    break;
  case AL_NO3Y:
    if(LANG == 1) printf("### kann Z88O3.BNY nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88O3.BNY ..stop ###\n");
    break;
  case AL_JACNEG:
    if(LANG == 1)
    {
    printf("### Jacobi-Determinante Null oder negativ..Stop          ###\n");
    printf("### liegt oft an fehlenden oder falschen Randbedingungen ###\n");
    printf("### Randbedingungen checken (statisch unterbestimmt ?)   ###\n");
    }
    if(LANG == 2)
    {
    printf("### Jacobi-determinant zero or negative..stop    ###\n");
    printf("### often caused by missing or wrong constraints ###\n");
    printf("### recover: check constraints (underdefined ?)  ###\n");
    }
    break;
  }
return(0);
}
