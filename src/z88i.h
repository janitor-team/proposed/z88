/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the Windows & UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V13.0 February 14, 2008
*
* Z88 should compile and run under any UNIX OS and Windows.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/***********************************************************************
* z88i.h fuer UNIX und Windows
* 21.9.2008 Rieg
***********************************************************************/

/***********************************************************************
* Datentypen fuer Windows und UNIX
***********************************************************************/
#ifdef FR_XINT
#define FR_INT4AY int *                /* Pointer auf int         */
#define FR_INT4 int                    /* int                     */
#define FR_CALLOC calloc               /* calloc                  */
#define FR_SIZERW size_t               /* Size fuer fread, fwrite */
#endif

#ifdef FR_XLONG
#define FR_INT4AY long *               /* Pointer auf long        */
#define FR_INT4 long                   /* long                    */
#define FR_CALLOC calloc               /* calloc                  */
#define FR_SIZERW size_t               /* Size fuer fread, fwrite */
#endif

#ifdef FR_XLOLO
#define FR_INT4AY long long *          /* Pointer auf long        */
#define FR_INT4 long long              /* long                    */
#define FR_CALLOC calloc               /* calloc                  */
#define FR_SIZERW size_t               /* Size fuer fread, fwrite */
#endif

#ifdef FR_XDOUB
#define FR_SQRT sqrt                   /* sqrt                    */
#define FR_POW pow                     /* pow                     */
#define FR_FABS fabs                   /* fabs                    */
#define FR_SIN sin                     /* sin                     */
#define FR_COS cos                     /* cos                     */
#define FR_DOUBLEAY double *           /* Pointer auf double      */
#define FR_DOUBLE double               /* double                  */
#endif

#ifdef FR_XQUAD
#define FR_SQRT sqrtl                  /* sqrtl                   */
#define FR_POW powl                    /* powl                    */
#define FR_FABS fabsl                  /* fabsl                   */
#define FR_SIN sinl                    /* sinl                    */
#define FR_COS cosl                    /* cosl                    */
#define FR_DOUBLEAY long double *      /* Pointer auf long double */
#define FR_DOUBLE long double          /* long double             */
#endif

#include <z88math.h>

/***********************************************************************
* Daten
***********************************************************************/
#define ICO_Z88I1                       10
#define ICO_Z88I2                       15
#define ICO_Z88PAR                      25

#define CUR_Z88I1                       30
#define CUR_Z88I2                       35
#define CUR_Z88PAR                      45

#define BMP_Z88I1                       50
#define BMP_Z88I2                       55
#define BMP_Z88PAR                      65

/***********************************************************************
* Menue-IDs
***********************************************************************/
#define IDM_WER                        100
#define IDM_XIT                        110
#define IDM_SIC                        120
#define IDM_SOR                        130
#define IDM_SCG                        140
#define IDM_GO                         150

/***********************************************************************
* Toolbar-IDs
***********************************************************************/
#define ITC_GO                         200
#define ITC_HELP                       210
#define ITC_SIC                        220
#define ITC_SOR                        230
#define ITC_SCG                        240

/***********************************************************************
* Alerts
***********************************************************************/
#define AL_NOLOG1 3000                 /* kein Z88I1.LOG */ 
#define AL_NOLOG2 3005                 /* kein Z88I2.LOG */ 
#define AL_NODYN 3010                  /* kein Z88.DYN */
#define AL_WRONGDYN 3020               /* Fehler in Z88.DYN */
#define AL_NOMEMY 3030                 /* nicht genug Memory */
#define AL_NOI1 3040                   /* Fehler Oeffnen z88i1.txt */
#define AL_NOI2 3050                   /* Fehler Oeffnen z88i2.txt */
#define AL_NOI4 3060                   /* Fehler Oeffnen z88i4.txt */
#define AL_NOI5 3065                   /* Fehler Oeffnen z88i5.txt */
#define AL_NO1Y 3070                   /* Fehler Oeffnen z88o1.bny */
#define AL_NO2Y 3080                   /* Fehler Oeffnen z88o2.bny */
#define AL_NO3Y 3090                   /* Fehler Oeffnen z88o3.bny */
#define AL_NO4Y 3095                   /* Fehler Oeffnen z88o4.bny */
#define AL_NOO0 3100                   /* Fehler Oeffnen z88o0.txt */
#define AL_NOO1 3110                   /* Fehler Oeffnen z88o1.txt */
#define AL_NOO2 3120                   /* Fehler Oeffnen z88o2.txt */
#define AL_WRONDIM 3130                /* NDIM falsch */
#define AL_EXMAXK 3140                 /* zuviele Knoten */
#define AL_EXMAXE 3150                 /* zuviele Elemente */
#define AL_EXMAXKOI 3155               /* Koinzidenzvektor zu klein */
#define AL_EXMAXNFG 3160               /* zuviele Freiheitsgrade */
#define AL_EXMAXNEG 3170               /* zuviele E-Gesetze */
#define AL_EXMAXPR 3175                /* NOI zu klein */
#define AL_WROKFLAG 3180               /* KFLAG falsch */
#define AL_WROETYP 3190                /* falscher Elementtyp */
#define AL_EXGS 3200                   /* MAXGS exhausted */
#define AL_EXKOI 3210                  /* MAXKOI exhausted */
#define AL_EXMAXIEZ 3215               /* MAXIEZ exhausted */
#define AL_JACNEG 3230                 /* Jacobi-Determinate null/negativ */
#define AL_JACLOA 3235                 /* Jacobi-Determinate null/negativ */
#define AL_DIAGNULL 3240               /* Diagonalele null/negativ */
#define AL_WROCFLAG 3250               /* Steuerflag Z88I2 falsch */
#define AL_NOCFLAG 3260                /* kein Steuerflag Z88I2 */
#define AL_PARA88M1 3261               /* PARDISO -1 */
#define AL_PARA88M2 3262               /* PARDISO -2 */
#define AL_PARA88M3 3263               /* PARDISO -3 */
#define AL_PARA88M4 3264               /* PARDISO -4 */
#define AL_PARA88M5 3265               /* PARDISO -5 */
#define AL_PARA88M6 3266               /* PARDISO -6 */
#define AL_PARA88M7 3267               /* PARDISO -7 */
#define AL_PARA88M8 3268               /* PARDISO -8 */
#define AL_PARA88M9 3269               /* PARDISO -9 */
#define AL_PARA88M10 3270              /* PARDISO -10 */
#define AL_PARA88M11 3271              /* PARDISO -11 */

/***********************************************************************
* Texte
***********************************************************************/
#define TX_REAI1 3500                  /* Z88I1.TXT einlesen */
#define TX_REAI2 3510                  /* Z88I2.TXT einlesen */
#define TX_REAI4 3512                  /* Z88I4.TXT einlesen */
#define TX_REAI2P2 3515                /* Z88I2.TXT zum 2.mal einlesen */
#define TX_REAI5 3517                  /* Z88I5.TXT einlesen */
#define TX_WRIO0 3520                  /* Z88O0.TXT beschreiben */
#define TX_WRIO1 3530                  /* Z88O1.TXT beschreiben */
#define TX_WRIO2 3540                  /* Z88O2.TXT beschreiben bei Z88I2 */
#define TX_WRIO2P 3545                 /* Z88O2.TXT beschreiben bei Z88PAR */
#define TX_WRI1Y 3550                  /* Z88O1.BNY beschreiben */
#define TX_WRI2Y 3560                  /* Z88O2.BNY beschreiben */
#define TX_WRI3Y 3570                  /* Z88O3.BNY beschreiben */
#define TX_WRI4Y 3575                  /* Z88O4.BNY beschreiben */
#define TX_REA4Y 3577                  /* Z88O4.BNY einlesen */
#define TX_ELE 3580                    /* Element I */
#define TX_KOOR 3590                   /* Koordinaten einlesen */
#define TX_POLAR 3600                  /* Polarkoordinaten umr. */
#define TX_KOIN 3610                   /* Koinzidenz einlesen */
#define TX_EGES 3620                   /* E-Gesetze einlesen */
#define TX_Z88A 3630                   /* Start Z88A */
#define TX_FORMA 3640                  /* Formatieren */
#define TX_GSERF 3650                  /* GS erfordert n */
#define TX_KOIERF 3660                 /* KOI erfordert n */
#define TX_Z88B 3670                   /* Start Z88B */
#define TX_SUMMEMY 3675                /* Summe angeforderter Speicher */
#define TX_COMPI 3680                  /* Compilieren */
#define TX_Z88CC 3690                  /* Start Z88CC */
#define TX_Z88CP 3695                  /* Start Z88CP */
#define TX_Z88CT 3697                  /* Start Z88CT */
#define TX_ERBPA 3700                  /* Einarbeiten RB Pass I */
#define TX_SCAL88 3710                 /* Start SCAL88 */
#define TX_SICCG88 3720                /* Start SICCG88 */
#define TX_SORCG88 3722                /* Start SORCG88 */
#define TX_PARA88 3725                 /* Start PARA88 */
#define TX_NFG 3730                    /* Anzahl nfg */
#define TX_CHOJ 3740                   /* J = */
#define TX_REA2Y 3770                  /* Z88O2.BNY lesen */
#define TX_NONZ 3780                   /* Nicht- Nullelemente */
#define TX_GSSO 3790                   /* GS sortiert */
#define TX_REAO1Y 3800                 /* Z88O1.BNY eingelesen */
#define TX_JACOOK 3810                 /* Epsilon erreicht */
#define TX_JACONOTOK 3820              /* Epsilon nicht erreicht */
#define TX_ITERA 3830                  /* n-te Iteration */
#define TX_PART88 3840                 /* n-te partielle Cholesky-Z. */
#define TX_CR 3850                     /* neue Zeile */
#define TX_ENDI1 3860                  /* Ende Z88I1 */
#define TX_UMSPS 3870                  /* Beginn Umspeichern */
#define TX_UMSPF 3875                  /* Ende Umspeichern */
#define TX_SPAR 3880                   /* StartPARDISO */
#define TX_REBA88 3890                 /* Start REBA88 */
#define TX_LLT  3910                   /* LLT Solver */ 
#define TX_MINRES 3920                 /* MINRES Solver */ 
#define TX_CG 3930                     /* CG Solver */ 
#define TX_PARA88OK 3960               /* PARDISO sauber gelaufen */ 
#define TX_PARA88NOTOK 3970            /* PARDISO nicht sauber gelaufen */ 
#define TX_DYNMEMY 3980                /* dyn.Zusatzspeicher fuer Z88I1 */
#define TX_ALPHA 3984                  /* Ausgabe Alpha */
#define TX_OMEGA 3986                  /* Ausgabe Omega */

/***********************************************************************
* LOGs
***********************************************************************/
#define LOG_BZ88I1 4000                /* Beginn Z88I1 */
#define LOG_BZ88I2 4003                /* Beginn Z88I2 */
#define LOG_BZ88PAR 4007               /* Beginn Z88PAR */
#define LOG_OPENZ88DYN 4010            /* Oeffnen Z88.DYN */
#define LOG_NODYN 4020                 /* kann Z88.DYN nicht oeffnen */
#define LOG_WRONGDYN 4030              /* Z88.DYN nicht o.k. */
#define LOG_MAXIEZ 4035                /* MAXIEZ */
#define LOG_MAXGS 4040                 /* MAXGS */
#define LOG_MAXKOI 4050                /* MAXKOI */
#define LOG_MAXK 4060                  /* MAXK */
#define LOG_MAXE 4070                  /* MAXE */
#define LOG_MAXNFG 4080                /* MAXNFG */
#define LOG_MAXNEG 4090                /* MAXNEG */
#define LOG_MAXESM 4100                /* MAXESM */
#define LOG_MAXPR 4105                 /* MAXPR fuer NOI */
#define LOG_OKDYN 4110                 /* Z88.DYN scheint o.k. zu sein */
#define LOG_ALLOCMEMY 4120             /* Memory anlegen */
#define LOG_ARRAYNOTOK 4130            /* Memory Kennung I nicht o.k. */
#define LOG_ARRAYOK 4140               /* Memory Kennung I angelegt */
#define LOG_SUMMEMY 4150               /* Memory angefordert */
#define LOG_EXITDYN88I1 4160           /* Verlassen DYN88I1 */
#define LOG_EXITDYN88I2 4165           /* Verlassen DYN88I2 */
#define LOG_BRI188 4170                /* Start RI188 */
#define LOG_NOI1 4180                  /* kein Z88I1.TXT */
#define LOG_WRONGDIM 4200              /* ndim falsch */
#define LOG_EXMAXK 4210                /* MAXK ueberschritten */
#define LOG_EXMAXE 4220                /* MAXE ueberschritten */
#define LOG_EXMAXKOI 4225              /* Koinzidenzvektor zu klein */
#define LOG_EXMAXNFG 4230              /* MAXNFG ueberschritten */
#define LOG_EXMAXNEG 4240              /* MAXNEG ueberschritten */
#define LOG_EXMAXPR 4245               /* MAXPR ueberschritten */
#define LOG_WROKFLAG 4250              /* KFLAG falsch */
#define LOG_KOOR 4260                  /* Einlesen Koordinaten */
#define LOG_KOIN 4270                  /* Einlesen Koinzidenz */
#define LOG_EGES 4280                  /* Einlesen E-Gesetze */
#define LOG_EXITRI188 4290             /* Verlassen RI188 */
#define LOG_EXITRI588 4295             /* Verlassen RI588 */
#define LOG_BWRIA88F 4300              /* Start WRIA88F */
#define LOG_NO1Y 4310                  /* kein Z88O1.BNY */
#define LOG_NOO0 4320                  /* kein Z88O0.TXT */
#define LOG_WRI1Y 4330                 /* Beschreiben Z88O1.BNY */
#define LOG_WRIO0 4340                 /* Beschreiben Z88O0.TXT */
#define LOG_GSERF 4350                 /* GS erforderlich */
#define LOG_KOIERF 4360                /* KOI erforderlich */
#define LOG_EXITWRIA88F 4370           /* Verlassen WRIA88F */
#define LOG_Z88A 4380                  /* Start Z88A */
#define LOG_FORMA 4390                 /* Formatieren */
#define LOG_WROETYP 4400               /* falscher Elementtyp */
#define LOG_EXITZ88A 4410              /* Verlassen Z88A */
#define LOG_Z88B 4420                  /* Start Z88B */
#define LOG_EXGS 4430                  /* MAXGS ueberschritten */
#define LOG_EXKOI 4440                 /* MAXKOI ueberschritten */
#define LOG_EXMAXIEZ 4445              /* MAXIEZ ueberschritten */
#define LOG_COMPI 4450                 /* Compilation */
#define LOG_EXITZ88B 4470              /* Verlassen Z88B */
#define LOG_Z88CC 4480                 /* Start Z88CC */
#define LOG_Z88CT 4482                 /* Start Z88CT */
#define LOG_Z88CP 4485                 /* Start Z88CP */
#define LOG_NOI2 4490                  /* kein Z88I2.TXT */
#define LOG_NOI4 4495                  /* kein Z88I4.TXT */
#define LOG_NOI5 4497                  /* kein Z88I5.TXT */
#define LOG_BRI588 4499                /* Start RI588I */
#define LOG_NO3Y 4500                  /* kein Z88O3.BNY */
#define LOG_NO4Y 4505                  /* kein Z88O4.BNY */
#define LOG_NOO1 4510                  /* kein Z88O1.TXT */
#define LOG_NOO2 4520                  /* kein Z88O2.TXT */
#define LOG_REAI2 4530                 /* Einlesen Z88I2.TXT */
#define LOG_REAI4 4535                 /* Einlesen Z88I4.TXT */
#define LOG_WRIO1 4540                 /* Beschreiben Z88O1.TXT */
#define LOG_ERBPA 4550                 /* Einarbeiten RB Pass I */
#define LOG_SCAL88 4560                /* Start SCAL88 */
#define LOG_PARA88 4562                /* Start PARA88 */
#define LOG_SICCG88 4570               /* Start SICCG88 */
#define LOG_SORCG88 4575               /* Start SORCG88 */
#define LOG_WRI3Y 4580                 /* Beschreiben Z88O3.BNY */
#define LOG_WRI4Y 4585                 /* Beschreiben Z88O4.BNY */
#define LOG_WRIO2 4590                 /* Beschreiben Z88O2.TXT */
#define LOG_EXITZ88CC 4600             /* Verlassen Z88CC */
#define LOG_VORW 4610                  /* Vorwaertseinsetzen */
#define LOG_RUECKW 4620                /* Rueckwaertseinsetzen */
#define LOG_DIAGNULL 4630              /* Diagonalelement <= 0 */
#define LOG_JACNEG 4640                /* Jacobi- Determinate <= 0 */
#define LOG_JACLOA 4645                /* Jacobi- Determinate <= 0 */
#define LOG_CFLAGC 4650                /* ICFLAG = 1: /C */
#define LOG_CFLAGN 4660                /* ICFLAG = 2: /N */
#define LOG_CFLAGA 4670                /* ICFLAG = 3: /A */
#define LOG_CFLAGT 4680                /* ICFLAG = 4: /T */
#define LOG_NO2Y 4690                  /* kein Z88O2.BNY */
#define LOG_WRI2Y 4700                 /* Beschreiben Z88O2.BNY */
#define LOG_WRI2YOK 4710               /* Z88O2.BNY beschrieben*/
#define LOG_WRI4YOK 4715               /* Z88O4.BNY beschrieben*/
#define LOG_REA2Y 4720                 /* Lesen Z88O2.BNY */
#define LOG_REA4Y 4725                 /* Lesen Z88O4.BNY */
#define LOG_REA2YOK 4730               /* Z88O2.BNY gelesen*/
#define LOG_REA4YOK 4735               /* Z88O4.BNY gelesen*/
#define LOG_REAO1Y 4740                /* Z88O1.BNY einlesen*/
#define LOG_R1Y88OK 4750               /* Z88O1.BNY gelesen*/
#define LOG_MAXIT 4760                 /* max. Anzahl von Iterationen */
#define LOG_CFLAGS 4770                /* gewaehlter Mode */
#define LOG_ITERA 4780                 /* Anzahl Iterationen */
#define LOG_EXITZ88CP 4790             /* Verlassen Z88CP */
#define LOG_EXITZ88CT 4795             /* Verlassen Z88CT */
#define LOG_UMSPS 4800                 /* Beginn Umspeichern */
#define LOG_UMSPF 4810                 /* Ende Umspeichern */
#define LOG_SPAR 4820                  /* StartPARDISO */
#define LOG_PARA88M  4830              /* PARDISO-Fehler -1 bis -11 */
#define LOG_PARA88NOTOK 4850           /* PARDISO nicht sauber gelaufen */
#define LOG_PARA88OK 4860              /* PARDISO sauber gelaufen */




