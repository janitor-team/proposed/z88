/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V12.0  February 14, 2005
*
* Z88 should compile and run under any UNIX OS and Motif 2.0.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/***********************************************************************
* Diese Compilerunit umfasst:
* - wrim88f gibt Texte aus (1 FR_INT4)
* - wproz88 gibt Prozentanteil aus (1 FR_INT4, 1 FR_DOUBLE)
* - wtyp88f gibt Elementtypen aus (1 FR_INT4, 1 FR_INT4)
* 18.1.2008 Rieg
***********************************************************************/ 

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88f.h>
#include <stdio.h>    /* printf */
#endif

/***********************************************************************
* Formate
***********************************************************************/
#ifdef FR_XINT
#define PD "%d"
#define P5D "%5d"
#endif

#ifdef FR_XLONG
#define PD "%ld"
#define P5D "%5ld"
#endif

#ifdef FR_XLOLO
#define PD "%lld"
#define P5D "%5lld"
#endif

#ifdef FR_XDOUB
#define P6F "%6.2lf"
#endif

#ifdef FR_XQUAD
#define P6F "%6.2Lf"
#endif

/***********************************************************************
*  hier beginnt Function wrim88f
***********************************************************************/
int wrim88f(FR_INT4 i,int iatx)
{
extern FR_INT4 LANG;

switch(iatx)
  {
  case TX_CHOJ:
    printf("\r" PD,i);
  break;

  case TX_ELE:
    if(LANG == 1) printf("\rElement " PD,i);
    if(LANG == 2) printf("\relement " PD,i);
  break;

  case TX_NFG:
    if(LANG == 1) printf(PD " x " PD " = Groesse Gleichungsystem \n",i,i);
    if(LANG == 2) printf(PD " x " PD " = size of system of equations\n",i,i);
  break;
 
  case TX_REAI1:
    if(LANG == 1) printf("Z88I1.TXT einlesen\n");
    if(LANG == 2) printf("reading Z88I1.TXT\n");
  break;

  case TX_REAI5:
    if(LANG == 1) printf("\n***** Lastvektoren berechnen *****");
    if(LANG == 2) printf("\n***** computing load vectors *****");
  break;

  case TX_KOOR:
    if(LANG == 1) printf("Koordinaten einlesen\n");
    if(LANG == 2) printf("reading coordinates\n");
  break;

  case TX_POLAR:
    if(LANG == 1) printf("Polar/Zylinder-Koordinaten umrechnen\n");
    if(LANG == 2) printf("converting polar/cylinder coordinates\n");
  break;

  case TX_KOIN:
    if(LANG == 1) printf("Koinzidenz einlesen\n");
    if(LANG == 2) printf("reading element informations\n");
  break;

  case TX_EGES:
    if(LANG == 1) printf("Elastizitaetsgesetze einlesen\n");
    if(LANG == 2) printf("reading material informations\n");
  break;

  case TX_Z88A:
    if(LANG == 1) printf(">>> Start Z88A: Pass 1 von Z88F <<<\n");
    if(LANG == 2) printf(">>> start Z88A: pass 1 of Z88F <<<\n");
  break;

  case TX_Z88B:
    if(LANG == 1) printf(">>> Start Z88B: Pass 2 von Z88F <<<\n");
    if(LANG == 2) printf(">>> start Z88B: pass 2 of Z88F <<<\n");
  break;

  case TX_FORMA:
    if(LANG == 1) printf("***** Formatieren *****\n");
    if(LANG == 2) printf("***** formatting *****\n");
  break;

  case TX_WRIO0:
    if(LANG == 1) printf("Z88O0.TXT beschreiben\n");
    if(LANG == 2) printf("writing Z88O0.TXT\n");
  break;

  case TX_WRIO1:
    if(LANG == 1) printf("Z88O1.TXT beschreiben\n");
    if(LANG == 2) printf("writing Z88O1.TXT\n");
  break;

  case TX_WRIO2:
    if(LANG == 1) printf("Z88O2.TXT beschreiben, Ende Z88F\n");
    if(LANG == 2) printf("writing Z88O2.TXT, Z88F done\n");
  break;

  case TX_WRI1Y:
    if(LANG == 1) printf("\nZ88O1.BNY beschreiben\n");
    if(LANG == 2) printf("\nwriting Z88O1.BNY\n");
  break;

  case TX_GSERF:
    if(LANG == 1) printf("Vektor GS erfordert " PD " Elemente\n",i);
    if(LANG == 2) printf("vector GS needs " PD " elements\n",i);
  break;

  case TX_KOIERF:
    if(LANG == 1) printf("Vektor KOI erfordert " PD " Elemente\n",i);
    if(LANG == 2) printf("vector KOI needs " PD " elements\n",i);
  break;

  case TX_WRI3Y:
    if(LANG == 1) printf("Z88O3.BNY beschreiben\n");
    if(LANG == 2) printf("writing Z88O3.BNY\n");
  break;

  case TX_COMPI:
    if(LANG == 1) printf("***** Compilation *****\n");
    if(LANG == 2) printf("***** compilation *****\n");
  break;

  case TX_Z88CC:
    if(LANG == 1) printf(">>> Start Z88CC: Pass 3 von Z88F <<<\n");
    if(LANG == 2) printf(">>> start Z88CC: pass 3 of Z88F <<<\n");
  break;

  case TX_REAI2:
    if(LANG == 1) printf("Einlesen von Randbedingungsfile Z88I2.TXT\n");
    if(LANG == 2) printf("reading constraint file Z88I2.TXT\n");
  break;

  case TX_ERBPA:
    if(LANG == 1) printf("Einarbeiten der Randbedingungen Pass " PD "\n",i);
    if(LANG == 2) printf("incorporating constraints pass " PD "\n",i);
  break;

  case TX_SCAL88:
    if(LANG == 1) printf("Start SCAL88\n");
    if(LANG == 2) printf("start SCAL88\n");
  break;

  case TX_CHOY88:
    if(LANG == 1)
    printf("***** Start des Cholesky- Verfahrens CHOY88 *****\n");
    if(LANG == 2)
    printf("***** start of Cholesky solver CHOY88 *****\n");
  break;

  case TX_VORW:
    if(LANG == 1) printf("\nVorwaertseinsetzen\n");
    if(LANG == 2) printf("\nforward-substitution\n");
  break;

  case TX_RUECKW:
    if(LANG == 1) printf("Rueckwaertseinsetzen\n");
    if(LANG == 2) printf("back-substitution\n");
  break;
  }

return(0);
}

/***********************************************************************
* function wproz88 gibt Prozentanteil aus
***********************************************************************/ 
int wproz88(FR_INT4 iltx,FR_DOUBLE proz)
{
extern FR_INT4 LANG;

if(LANG == 1)
  {
  printf("\nVektor GS enthaelt " PD " Elemente\n",iltx);
  printf("Anteil Nullelemente in GS ist " P6F " Prozent\n",proz);
  }
if(LANG == 2)
  {
  printf("\nvector GS has " PD " elements\n",iltx);
  printf("portion of zero-elements in GS is " P6F " p.c.\n",proz);
  }

return(0);
}

/***********************************************************************
*  function wtyp88f gibt Elementtypen in Z88F aus
***********************************************************************/ 
int wtyp88f(FR_INT4 k,FR_INT4 i)
{
extern FR_INT4 LANG;

if(LANG == 1) printf("\rNr. " P5D " Typ " P5D,k,i);
if(LANG == 2) printf("\rno. " P5D " type " P5D,k,i);

return(0);
}
