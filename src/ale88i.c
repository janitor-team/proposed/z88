/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V13.0  February 14, 2008
*
* Z88 should compile and run under any UNIX OS and Motif 2.0.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/***********************************************************************
*  function ale88i gibt Fehlermeldungen aus
*  14.2.2008 Rieg
***********************************************************************/ 

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88i.h>
#include <stdio.h>    /* printf */
#endif

/***********************************************************************
*  hier beginnt Function ale88i
***********************************************************************/
int ale88i(int ialert)
{
extern FR_INT4 LANG;

switch(ialert)
  {
  case AL_NOCFLAG:
    if(LANG == 1)
    {
    printf("### kein Steuerflag fuer Z88I2 angegeben ..Stop  ###\n");
    printf("### Richtiger Aufruf:  z88i2 -c (SICCG- Solver)  ###\n");
    printf("###                    z88i2 -s (SORCG- Solver)  ###\n");
    }
    if(LANG == 2)
    {
    printf("### no flag given for Z88I2 ..stop       ###\n");
    printf("### correct use: z88i2 -c (solver SICCG) ###\n");
    printf("###              z88i2 -s (solver SORCG) ###\n");
    }
    break;
  case AL_WROCFLAG:
    if(LANG == 1)
    {
    printf("### Steuerflag falsch fuer Z88I2 angegeben ..Stop  ###\n");
    printf("### Richtiger Aufruf:  z88i2 -c (SICCG- Solver)    ###\n");
    printf("###                    z88i2 -s (SORCG- Solver)    ###\n");
    }
    if(LANG == 2)
    {
    printf("### wrong flag given for Z88I2 ..stop    ###\n");
    printf("### correct use: z88i2 -c (solver SICCG) ###\n");
    printf("###              z88i2 -s (solver SORCG) ###\n");
    }
    break;
  case AL_NOLOG1:
    if(LANG == 1) printf("### kann Z88I1.LOG nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88I1.LOG ..stop ###\n");
    break;
  case AL_NOLOG2:
    if(LANG == 1) printf("### kann Z88I2.LOG nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88I2.LOG ..stop ###\n");
    break;
  case AL_NODYN:
    if(LANG == 1) printf("### kann Z88.DYN nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88.DYN ..stop ###\n");
    break;
  case AL_WRONGDYN:
    if(LANG == 1) printf("### File Z88.DYN ist nicht korrekt ..Stop ###\n");
    if(LANG == 2) printf("### file Z88.DYN is not correct ..stop ###\n");
    break;
  case AL_NOMEMY:
    if(LANG == 1)printf("### nicht genuegend dynamisches Memory ..Stop ###\n");
    if(LANG == 2)printf("### insufficient dynamic memory ..Stop ###\n");
    break;
  case AL_NOI1:
    if(LANG == 1) printf("### kann Z88I1.TXT nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88I1.TXT ..stop ###\n");
    break;
  case AL_NOI2:
    if(LANG == 1) printf("### kann Z88I2.TXT nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88I2.TXT ..stop ###\n");
    break;
  case AL_NOI4:
    if(LANG == 1) printf("### kann Z88I4.TXT nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88I4.TXT ..stop ###\n");
    break;
  case AL_NOI5:
    if(LANG == 1) printf("### kann Z88I5.TXT nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88I5.TXT ..stop ###\n");
    break;
  case AL_NO1Y:
    if(LANG == 1) printf("### kann Z88O1.BNY nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88O1.BNY ..stop ###\n");
    break;
  case AL_NO2Y:
    if(LANG == 1) printf("### kann Z88O2.BNY nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88O2.BNY ..stop ###\n");
    break;
  case AL_NO3Y:
    if(LANG == 1) printf("### kann Z88O3.BNY nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88O3.BNY ..stop ###\n");
    break;
  case AL_NO4Y:
    if(LANG == 1) printf("### kann Z88O4.BNY nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88O4.BNY ..stop ###\n");
    break;
  case AL_NOO0:
    if(LANG == 1) printf("### kann Z88O0.TXT nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88O0.TXT ..stop ###\n");
    break;
  case AL_NOO1:
    if(LANG == 1) printf("### kann Z88O1.TXT nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88O1.TXT ..stop ###\n");
    break;
  case AL_NOO2:
    if(LANG == 1) printf("### kann Z88O2.TXT nicht oeffnen ..Stop ###\n");
    if(LANG == 2) printf("### cannot open Z88O2.TXT ..stop ###\n");
    break;
  case AL_WRONDIM:
    if(LANG == 1) printf("### Dimension falsch ..Stop ###\n");
    if(LANG == 2) printf("### wrong dimension ..stop ###\n");
    break;
  case AL_EXMAXK:
    if(LANG == 1)
    {
    printf("### Zuviele Knoten ..zuwenig Speicher ..Stop ###\n");
    printf("### Abhilfe: MAXK in Z88.DYN erhoehen        ###\n");
    }
    if(LANG == 2)
    {
    printf("### too many nodes ..insufficient memory ..stop ###\n");
    printf("### recover: increase MAXK in Z88.DYN           ###\n");
    }
    break;
  case AL_EXMAXE:
    if(LANG == 1)
    {
    printf("### Zuviele Elemente ..zuwenig Speicher ..Stop ###\n");
    printf("### Abhilfe: MAXE in Z88.DYN erhoehen          ###\n");
    }
    if(LANG == 2)
    {
    printf("### too many elements ..insufficient memory ..stop ###\n");
    printf("### recover: increase MAXE in Z88.DYN              ###\n");
    }
    break;
  case AL_EXMAXKOI:
    if(LANG == 1)
    {
    printf("### Koinzidenzvektor KOI laeuft ueber ..Stop ###\n");
    printf("### Abhilfe: MAXKOI in Z88.DYN erhoehen      ###\n");
    }
    if(LANG == 2)
    {
    printf("### koincidence vector KOI exhausted ..stop ###\n");
    printf("### recover: increase MAXKOI in Z88.DYN     ###\n");
    }
    break;
  case AL_EXMAXNFG:
    if(LANG == 1)
    {
    printf("### Zuviele Freiheitsgrade ..zuwenig Speicher ..Stop ###\n");
    printf("### Abhilfe: MAXNFG in Z88.DYN erhoehen              ###\n");
    }
    if(LANG == 2)
    {
    printf("### too many DOF ..insufficient memory ..stop ###\n");
    printf("### recover: increase MAXNFG in Z88.DYN       ###\n");
    }
    break;
  case AL_EXMAXNEG:
    if(LANG == 1)
    {
    printf("### Zuviele Elastizitaetsgesetze ..zuwenig Speicher ..Stop ###\n");
    printf("### Abhilfe: MAXNEG in Z88.DYN erhoehen                    ###\n");
    }
    if(LANG == 2)
    {
    printf("### too many mat lines ..insufficient memory ..stop ###\n");
    printf("### recover: increase MAXNEG in Z88.DYN             ###\n");
    }
    break;
  case AL_EXMAXPR:
    if(LANG == 1)
    {
    printf("### Zuviele Lastvektoren ..zuwenig Speicher ..Stop ###\n");
    printf("### Abhilfe: MAXPR in Z88.DYN erhoehen             ###\n");
    }
    if(LANG == 2)
    {
    printf("### too many load vectors ..insufficient memory ..stop ###\n");
    printf("### recover: increase MAXPR in Z88.DYN                 ###\n");
    }
    break;
  case AL_WROKFLAG:
    if(LANG == 1) printf("### KFLAG falsch ..Stop ###\n");
    if(LANG == 2) printf("### KFLAG wrong ..stop ###\n");
    break;
  case AL_WROETYP:
    if(LANG == 1) printf("### Unbekannter Elementtyp in Z88I1.TXT ..Stop ###\n");
    if(LANG == 2) printf("### unknown element type in Z88I1.TXT ..stop ###\n");
    break;
  case AL_EXGS:
    if(LANG == 1)
    {
    printf("### Gleichungssystem zu gross ..zuwenig Speicher ..Stop ###\n");
    printf("### Abhilfe: MAXGS hoeher setzen in Z88.DYN             ###\n");
    }
    if(LANG == 2)
    {
    printf("### system of equations too large ..insufficient memory ..stop ###\n");
    printf("### recover: increase MAXGS in Z88.DYN                         ###\n");
    }
    break;
  case AL_EXKOI:
    if(LANG == 1)
    {
    printf("### Gleichungssystem zu gross ..zuwenig Speicher ..Stop ###\n");
    printf("### Abhilfe: MAXKOI hoeher setzen in Z88.DYN            ###\n");
    }
    if(LANG == 2)
    {
    printf("### system of equations too large ..insufficient memory ..stop ###\n");
    printf("### recover: increase MAXKOI in Z88.DYN                        ###\n");
    }
    break;
  case AL_EXMAXIEZ:
    if(LANG == 1)
    {
    printf("\n### Pointer IEZ hat zuwenig Speicher ..Stop   ###\n");
    printf("### Abhilfe: MAXIEZ hoeher setzen in Z88.DYN  ###\n");
    }
    if(LANG == 2)
    {
    printf("\n### Pointer IEZ has insufficient memory ..stop ###\n");
    printf("### recover: increase MAXIEZ in Z88.DYN        ###\n");
    }
    break;
  case AL_JACNEG:
    if(LANG == 1)
    {
    printf("### Jacobi-Determinante Null oder negativ..Stop  ###\n");
    printf("### Elemente zu verzerrt oder falsch numeriert   ###\n");
    printf("### prefen, Numerierung gegen Uhrzeigersinn?     ###\n");
    }
    if(LANG == 2)
    {
    printf("### Jacobi-determinant zero or negative..stop    ###\n");
    printf("### elements to distorted or wrong numbered      ###\n");
    printf("### recover: check numbering (anti-clockwise)    ###\n");
    }
    break;
  case AL_JACLOA:
    if(LANG == 1)
    {
    printf("### Jacobi-Determinante Null oder negativ..Stop  ###\n");
    printf("### Lastvektoren falsch in Z88I5.TXT             ###\n");
    printf("### Stimmt die Numerierung der Flaeche? Handbuch ###\n");
    }
    if(LANG == 2)
    {
    printf("### Jacobi-determinant zero or negative..stop  ###\n");
    printf("### load vectors wrong in file Z88I5.TXT       ###\n");
    printf("### numering correct? See manual               ###\n");
    }
    break;
  case AL_DIAGNULL:
    if(LANG == 1)
    {
    printf("### Diagonalelement im G-System Null oder negativ..Stop  ###\n");
    printf("### liegt oft an fehlenden oder falschen Randbedingungen ###\n");
    printf("### Randbedingungen checken (statisch unterbestimmt ?)   ###\n");
    }
    if(LANG == 2)
    {
    printf("### diagonal element in array zero or negative..stop ###\n");
    printf("### often caused by missing or wrong constraints     ###\n");
    printf("### recover: check constraints (underdefined ?)      ###\n");
    }
    break;
  case AL_PARA88M1:
    if(LANG == 1) printf("### PARDISO Fehler -1: Eingabefehler STOP ###\n");
    if(LANG == 2) printf("### PARDISO error -1: input inconsistent STOP ###\n");
  break;
  case AL_PARA88M2:
    if(LANG == 1) printf("### PARDISO Fehler -2: zuwenig Speicher STOP ###\n");
    if(LANG == 2) printf("### PARDISO error -2: not enough memory STOP ###\n");
  break;
  case AL_PARA88M3:
    if(LANG == 1) printf("### PARDISO Fehler -3: Umordnungsproblem STOP ###\n");
    if(LANG == 2) printf("### PARDISO error -3: reordering problem STOP ###\n");
  break;
  case AL_PARA88M4:
    if(LANG == 1) printf("### PARDISO Fehler -4: Pivot Null STOP ###\n");
    if(LANG == 2) printf("### PARDISO error -4: zero pivot STOP ###\n");
  break;
  case AL_PARA88M5:
    if(LANG == 1) printf("### PARDISO Fehler -5: Interner Fehler STOP ###\n");
    if(LANG == 2) printf("### PARDISO error -5: internal error STOP ###\n");
  break;
  case AL_PARA88M6:
    if(LANG == 1) printf("### PARDISO Fehler -6: Umordnen klappte nicht STOP ###\n");
    if(LANG == 2) printf("### PARDISO error -6: reordering failed STOP ###\n");
  break;
  case AL_PARA88M7:
    if(LANG == 1) printf("### PARDISO Fehler -7: Diagonalmatrixproblem STOP ###\n");
    if(LANG == 2) printf("### PARDISO error -7: diagonal matrix problem STOP ###\n");
  break;
  case AL_PARA88M8:
    if(LANG == 1) printf("### PARDISO Fehler -8: 32-Bit Ueberlauf STOP ###\n");
    if(LANG == 2) printf("### PARDISO error -8: 32-bit overflow STOP ###\n");
  break;
  case AL_PARA88M9:
    if(LANG == 1) printf("### PARDISO Fehler -9: zuwenig Speicher fuer OOC STOP ###\n");
    if(LANG == 2) printf("### PARDISO error -9: not enough memory for OOC STOP ###\n");
  break;
  case AL_PARA88M10:
    if(LANG == 1) printf("### PARDISO Fehler -10: Problem beim Oeffnen von OOC-Zwischendateien STOP ###\n");
    if(LANG == 2) printf("### PARDISO error -10: problem opening temporary files for OOC STOP ###\n");
  break;
  case AL_PARA88M11:
    if(LANG == 1) printf("### PARDISO Fehler -11: Schreib/Leseproblem bei OOC-Zwischendateien STOP ###\n");
    if(LANG == 2) printf("### PARDISO error -11: read/write problem with OOC STOP ###\n");
  break;

  }
return(0);
}
