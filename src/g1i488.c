/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the Windows & UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V13.0  February 14, 2008
*
* Z88 should compile and run under any Windows and UNIX OS.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/**********************************************************************
* Function g1i488 prueft Z88I4.TXT
* 15.5.2008 Rieg
**********************************************************************/

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88v.h>
#include <stdio.h>   /* FILE,printf */
#endif

/***********************************************************************
* Functions
***********************************************************************/
void erif88(FR_INT4 izeile);

/***********************************************************************
* Start G1I488
***********************************************************************/
int g1i488(void)
{
extern FILE *fdatei;

extern FR_INT4 izeile,LANG;

extern FR_DOUBLE eps,rpomega,rpalpha;
extern FR_INT4 maxit,icore;

int ier;

char *cresult;
char cline[256];

/**********************************************************************
* Checken der 1.Zeile
**********************************************************************/
izeile= 1; 

cresult= fgets(cline,256,fdatei);
if(!cresult)
  {
  erif88(izeile);
  return(2);
  }

ier= sscanf(cline,"%ld %lf %lf %lf %ld",&maxit,&eps,&rpalpha,&rpomega,&icore);
if(ier != 5) 
  {
  printf("%s\n",cline);
  if(LANG == 1)
    printf("### Schreibfehler oder fehlende Daten in Zeile 1 entdeckt\n");
  if(LANG == 2)
    printf("### typing error or missing entries in line 1 detected\n");
  return(2);
  }
        
/*----------------------------------------------------------------------
* logische Pruefungen
*---------------------------------------------------------------------*/
if(maxit <= 0)
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### Falsche Anzahl Iterationen fuer Z88I2:\n");
    printf("### MAXIT muss groesser 0 sein\n");
    printf("### Wie waer's mit 1000 oder 10000 ?\n");
    printf("### 1.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### wrong maximal number of iterations for Z88I2\n");
    printf("### MAXIT has to be larger than 0\n");
    printf("### How about 1000 or 10000 ?\n");
    printf("### check 1st entry in line 1\n");
    }
  return(2);
  }

if(eps <= 0)
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### EPSILON fuer Z88I2 unzulaessig\n");
    printf("### EPSILON muss klein, aber groesser 0 sein\n");
    printf("### wie waer's mit 0.0000001 oder 1e-10 ?\n");
    printf("### 2.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### EPSILON invalid\n");
    printf("### EPSILON should be small but > 0\n");
    printf("### How about 0.0000001 or 1e-10 ?\n");
    printf("### check 2nd entry in line 1\n");
    }
  return(2);
  }

if(rpomega < 0. || rpomega > 2.)
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### OMEGA nicht in Ordnung\n");
    printf("### RP hat folgende Bedeutung:\n");
    printf("### Z88I2 mit SOR: Omega ist der Relax-Parameter,0<Omega<2,z.B. 1.2\n");
    printf("### 3.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### OMEGA invalid\n");
    printf("### Omega has the following meaning:\n");
    printf("### Z88I2 with SOR: Omega is the Relax parameter,0<Omega<2,for example 1.2\n");
    printf("### check 3rd entry in line 1\n");
    }
  return(2);
  }

if(rpalpha < 0)
  {
  if(LANG == 1)
    {
    printf("### ALPHA nicht in Ordnung\n");
    printf("### Alpha hat folgende Bedeutung:\n");
    printf("### Z88I2 mit SIC: Alpha ist der Shift-Parameter,0<Alpha<1,z.B. 0.0001\n");
    printf("### 4.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### ALPHA invalid\n");
    printf("### Alpha has the following meaning:\n");
    printf("### Z88I2 with SIC : Alpha is the Shift paramater,0<Alpha<1,for example 0.0001\n");
    printf("### check 4th entry in line 1\n");
    }
  return(2);
  }

if(icore < 1 || icore > 19 || icore == 10)
  {
  if(LANG == 1)
    {
    printf("### ICORE nicht in Ordnung\n");
    printf("### ICORE hat folgende Bedeutung:\n");
    printf("### Z88I2 : ICORE = 1 ~ 9: 1~9 CPUs\n");
    printf("### Z88PAR: ICORE = 1 ~ 9: 1~9 CPUs, in-core Solver\n");
    printf("### Z88PAR: ICORE = 11~19: 1~9 CPUs, out-of-core Solver\n");
    printf("### 5.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### ICORE invalid\n");
    printf("### ICORE has the following meaning:\n");
    printf("### Z88I2 : ICORE = 1 ~ 9: 1~9 CPUs\n");
    printf("### Z88PAR: ICORE = 1 ~ 9: 1~9 CPUs, in-core Solver\n");
    printf("### Z88PAR: ICORE = 11~19: 1~9 CPUs, out-of-core Solver\n");
    printf("### check 5th entry in line 1\n");
    }
  return(2);
  }

return(0);
}
