/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V12.0  February 14, 2005
*
* Z88 should compile and run under any UNIX OS and Motif 2.0.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/***********************************************************************
* wrim88h gibt Texte aus (1 FR_INT4)
* 15.3.2005 Rieg
***********************************************************************/ 

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88h.h>
#include <stdio.h>    /* printf */
#endif

/***********************************************************************
*  hier beginnt Function wrim88h
***********************************************************************/
int wrim88h(FR_INT4 i,int iatx)
{
extern FR_INT4 LANG;

switch(iatx)
  {
  case TX_INOPEN:
    if(LANG == 1) printf("Zwischendatei Z88H.IN geoeffnet\n");
    if(LANG == 2) printf("buffer file Z88H.IN opened\n");
  break;

  case TX_I1OPEN:
    if(LANG == 1) printf("Z88I1.TXT geoeffnet\n");
    if(LANG == 2) printf("Z88I1.TXT opened\n");
  break;

  case TX_I2OPEN:
    if(LANG == 1) printf("Z88I2.TXT geoeffnet\n");
    if(LANG == 2) printf("Z88I2.TXT opened\n");
  break;

  case TX_I5OPEN:
    if(LANG == 1) printf("Z88I5.TXT geoeffnet\n");
    if(LANG == 2) printf("Z88I5.TXT opened\n");
  break;

  case TX_OUTOPEN:
    if(LANG == 1) printf("Zwischendatei Z88H.OUT geoeffnet\n");
    if(LANG == 2) printf("buffer file Z88H.OUT opened\n");
  break;

  case TX_MEMOK:
    if(LANG == 1) printf("Memory angelegt\n");
    if(LANG == 2) printf("memory allocated\n");
  break;

  case TX_1STOP:
    if(LANG == 1) printf("Grad = %ld, zu hoch\n",i);
    if(LANG == 2) printf("grade = %ld, to big\n",i);
  break;

  case TX_MAXGR:
    if(LANG == 1) printf("Max. Grad = %ld",i);
    if(LANG == 2) printf("max. grade = %ld",i);
  break;

  case TX_STARTNUM:
    if(LANG == 1) printf("  Startnummer = %ld\n",i);
    if(LANG == 2) printf("  start number = %ld\n",i);
  break;

  case TX_LEVEL:
    if(LANG == 1) printf("\rLevel= %ld",i);
    if(LANG == 2) printf("\rlevel= %ld",i);
  break;

  case TX_STOPERM:
    if(i == 1)
      {
      if(LANG == 1) printf("Abspeichern Permutationsvektor, umkehrter CM\n");
      if(LANG == 2) printf("storing permutation vector, reverse CM\n");
      }
    if(i == 2)
      {
      if(LANG == 1) printf("Abspeichern Permutationsvektor, normaler CM\n");
      if(LANG == 2) printf("storing permutation vector, normal CM\n");
      }
  break;

  case TX_UMSPEI:
    if(LANG == 1) printf("Umspeichern der Files\n");
    if(LANG == 2) printf("storing files\n");
  break;

  case TX_NPRCM:
    if(LANG == 1) printf("  Profil = %ld",i);
    if(LANG == 2) printf("  profile = %ld",i);
  break;

  case TX_NPRRCM:
    if(LANG == 1) printf("  Umgekehrtes Profil = %ld\n",i);
    if(LANG == 2) printf("  reverse profile = %ld\n",i);
  break;

  case TX_TXT2OLD:
    if(LANG == 1) printf("Bisherige Z88I*.TXT in Z88I*.OLD sichern\n");
    if(LANG == 2) printf("copying existing Z88I*.TXT into Z88I*.OLD\n");
  break;

  case TX_WRII1:
    if(LANG == 1) printf("Nun Z88I1.TXT erzeugen\n");
    if(LANG == 2) printf("now building Z88I1.TXT\n");
  break;

  case TX_WRII2:
    if(LANG == 1) printf("Nun Z88I2.TXT erzeugen\n");
    if(LANG == 2) printf("now building Z88I2.TXT\n");
  break;

  case TX_WRII5:
    if(LANG == 1) printf("Nun Z88I5.TXT erzeugen\n");
    if(LANG == 2) printf("now building Z88I5.TXT\n");
  break;

  case TX_Z88DONE:
    if(LANG == 1) printf("Alle Dateien geschlossen. Z88H fertig.\n");
    if(LANG == 2) printf("All files closed. Z88H done\n");
  break;
  }
return(0);
}

