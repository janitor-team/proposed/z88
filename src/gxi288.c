/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V10.0  December 12, 2001
*
* Z88 should compile and run under any UNIX OS and Motif 2.0.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/***********************************************************************
* Compilerunit gxi288 enthaelt:
* g1i288
* g2i288
* 22.3.2002 Rieg
***********************************************************************/

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88v.h>
#include <stdio.h>   /* FILE,printf */
#include <string.h>  /* strcpy */
#endif

/***********************************************************************
* Functions
***********************************************************************/
void erif88(FR_INT4 izeile);

/***********************************************************************
* Start G1I288
***********************************************************************/
int g1i288(void)
{
extern FILE *fdatei;

extern FR_INT4 ndim,nfg,nrb,izeile,LANG;

int ier;

char *cresult;
char cline[256];

/**********************************************************************
* Checken der 1.Zeile
**********************************************************************/
izeile= 1; 

cresult= fgets(cline,256,fdatei);
if(!cresult)
  {
  erif88(izeile);
  return(2);
  }

ier= sscanf(cline,"%ld",&nrb);
if(ier != 1) 
  {
  printf("%s\n",cline);
  if(LANG == 1)
    printf("### Schreibfehler oder fehlende Daten in Zeile 1 entdeckt\n");
  if(LANG == 2)
    printf("### typing error or missing entries in line 1 detected\n");
  return(2);
  }
        
/*----------------------------------------------------------------------
* logische Pruefung nrb 
*---------------------------------------------------------------------*/
if(nrb > nfg)
  {
  printf("%s\n",cline);
  if(LANG == 1)
    printf("### Zuviele Randbedingungen in Zeile 1 entdeckt\n");
  if(LANG == 2)
    printf("### too many constraints in line 1 detected\n");
  return(2);
  }

if(ndim == 2)
  {
  if(nrb < 4)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Zuwenig Randbedingungen (< 4) fuer 2-D Struktur\n");
      printf("### in Zeile 1 entdeckt\n");
      }
    if(LANG == 2)
      {
      printf("### too few constraints (< 4) for 2-D structure\n");
      printf("### in line 1 detected\n");
      }
    return(2);
    }
  }

if(ndim == 3)
  {
  if(nrb < 6)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Zuwenig Randbedingungen (< 6) fuer 3-D Struktur\n");
      printf("### in Zeile 1 entdeckt\n");
      }
    if(LANG == 2)
      {
      printf("### too few constraints (< 6) for 3-D structure\n");
      printf("### in line 1 detected\n");
      }
    return(2);
    }
  }
return(0);
}
          
/***********************************************************************
* Start G2I288
***********************************************************************/
int g2i288(void)
{
extern FILE *fdatei;

extern FR_INT4AY ifrei;

extern FR_INT4 nkp,nrb,izeile,LANG;

FR_DOUBLE wert;

FR_INT4 nkn,ifg,iflag1,nknalt,ifgalt,i;

int ier;

char *cresult;
char cline[256],clina[256];

/**********************************************************************
* Checken der 2.Gruppe
**********************************************************************/
nknalt = 0;  /* nur wg. compiler warnings */
ifgalt = 0;

for(i = 1;i <= nrb;i++)
  {
  izeile++;

  if(i > 1)
    {
    nknalt= nkn;
    strcpy(clina,cline);
    ifgalt= ifg;
    }
            
  cresult= fgets(cline,256,fdatei);
  if(!cresult)
    {
    erif88(izeile);
    return(2);
    }

  ier= sscanf(cline,"%ld %ld %ld %lg",&nkn,&ifg,&iflag1,&wert);
  if(ier != 4) 
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf(
      "### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",
      izeile);
      printf("### Stimmt Anzahl Randbedingungen aus Zeile 1 mit der\n");
      printf("### dann folgenden Anzahl Steuerzeilen ueberein ?\n");
      }
    if(LANG == 2)
      {
      printf(
      "### typing error or missing entries in line %ld detected\n",
      izeile);
      printf("### is number of constraints line 1 identical with\n");
      printf("### number of following constraint lines ?\n");
      }
    return(2);
    }

/*----------------------------------------------------------------------
* logische Pruefung
*---------------------------------------------------------------------*/
  if(nkn < 1 || nkn > nkp)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Knotennummer ungueltig\n");
      printf("### 1.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### node number invalid\n");
      printf("### check 1st entry in line %ld\n",izeile);
      }
    return(2);
    }

  if(i > 1)
    { 
    if(nkn < nknalt)
      {
      printf("%s\n",clina);
      printf("%s\n",cline);
      if(LANG == 1)
        {
        printf("### Knotennummern nicht aufsteigend geordnet\n");
        printf("### 1.Wert in Zeile %ld ueberpruefen\n",izeile-1);
        printf("### 1.Wert in Zeile %ld ueberpruefen\n",izeile);
        }
      if(LANG == 2)
        {
        printf("### node numbers not ascending arranged\n");
        printf("### check 1st entry in line %ld\n",izeile-1);
        printf("### check 1st entry in line %ld\n",izeile);
        }
      return(2);
      }
    }
          
  if(ifg < 1)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Nummer Freiheitsgrad unzulaessig\n");
      printf("### 2.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### number DOF invalid\n");
      printf("### check 2nd entry in line %ld\n",izeile);
      }
    return(2);
    }

  if(ifg > ifrei[nkn])
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Nummer Freiheitsgrad groesser als in Z88I1.TXT\n");
      printf("### 2.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### number DOF greater than defined in Z88I1.TXT\n");
      printf("### check 2nd entry in line %ld\n",izeile);
      }
    return(2);
    }

  if(nkn == nknalt)
    { 
    if(ifg <= ifgalt)
      {
      printf("%s\n",clina);
      printf("%s\n",cline);
      if(LANG == 1)
        {
        printf("### Freiheitsgrade nicht aufsteigend geordnet\n");
        printf("### 2.Wert in Zeile %ld ueberpruefen\n",izeile-1);
        printf("### 2.Wert in Zeile %ld ueberpruefen\n",izeile);
        }
      if(LANG == 2)
        {
        printf("### DOF not ascending arranged\n");
        printf("### check 2nd entry in line %ld\n",izeile-1);
        printf("### check 2nd entry in line %ld\n",izeile);
        }
      return(2);
      }
    }

  if(!(iflag1 == 1 || iflag1 == 2))
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Steuerflag unzulaessig (nicht 1 oder 2)\n");
      printf("### 3.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### load/displacement flag invalid (neither 1 nor 2)\n");
      printf("### check 3rd entry in line %ld\n",izeile);
      }
    return(2);
    }

  }
return(0);
}
