/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V13.0  February 14, 2008
*
* Z88 should compile and run under any UNIX OS and Motif 2.0.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/***********************************************************************
* Diese Compilerunit umfasst:
* wrim88i gibt Texte aus (1 FR_INT4)
* wtyp88i gibt Elementtypen aus (1 FR_INT4, 1 FR_INT4)
* wfor88i gibt kfoun beim Sortieren aus (1 FR_INT4, 1 FR_INT4)
* wran88i gibt Randbedingungen aus (1 FR_INT4, 1 FR_INT4)
*
* diese Datei verwenden Z88I1, Z88I2 und Z88PAR
* 21.9.2008 Rieg
***********************************************************************/ 

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88i.h>
#include <stdio.h>    /* printf */
#endif

/***********************************************************************
* Formate
***********************************************************************/
#ifdef FR_XINT
#define PD "%d"
#define PD9 "%9d"
#endif

#ifdef FR_XLONG
#define PD "%ld"
#define PD9 "%9ld"
#endif

#ifdef FR_XLOLO
#define PD "%lld"
#define PD9 "%9lld"
#endif

/***********************************************************************
*  hier beginnt Function wrim88i
***********************************************************************/
int wrim88i(FR_INT4 i,int iatx)
{
extern FR_INT4 LANG;

switch(iatx)
  {
  case TX_CR:
    printf("\n");
  break;

  case TX_ITERA:
    printf("\r" PD ". Iteration",i);
    fflush(stdout);
  break;  
 
  case TX_CHOJ:
    printf("\r" PD,i);
  break;

  case TX_ELE:
    if(LANG == 1) printf("\rElement " PD,i);
    if(LANG == 2) printf("\relement " PD,i);
  break;

  case TX_NFG:
    if(LANG == 1) printf(PD " x " PD " = Groesse Gleichungsystem \n",i,i);
    if(LANG == 2) printf(PD " x " PD " = size of system of equations\n",i,i);
  break;

  case TX_SUMMEMY:
    if(LANG == 1) printf("Programm hat " PD " MB fix angefordert\n", i);
    if(LANG == 2) printf("program allocated " PD " MB memory\n",i);
  break;

  case TX_DYNMEMY:
    if(LANG == 1) printf("\nProgramm hat " PD " MB dynamischen Speicher angefordert\n", i);
    if(LANG == 2) printf("\nprogram allocated " PD " MB dynamic memory\n",i);
  break;

  case TX_REAI1:
    if(LANG == 1) printf("Z88I1.TXT einlesen\n");
    if(LANG == 2) printf("reading Z88I1.TXT\n");
  break;

  case TX_REAI5:
    if(LANG == 1) printf ("\n***** Lastvektoren berechnen *****");
    if(LANG == 2) printf ("\n***** computing load vectors *****");
  break;

  case TX_KOOR:
    if(LANG == 1) printf("Koordinaten einlesen\n");
    if(LANG == 2) printf("reading coordinates\n");
  break;

  case TX_POLAR:
    if(LANG == 1) printf("Polar/Zylinder-Koordinaten umrechnen\n");
    if(LANG == 2) printf("converting polar/cylinder coordinates\n");
  break;

  case TX_KOIN:
    if(LANG == 1) printf("Koinzidenz einlesen\n");
    if(LANG == 2) printf("reading element informations\n");
  break;

  case TX_EGES:
    if(LANG == 1) printf("Elastizitaetsgesetze einlesen\n");
    if(LANG == 2) printf("reading material informations\n");
  break;

  case TX_Z88A:
    if(LANG == 1) printf(">>> Start Z88AI <<<\n");
    if(LANG == 2) printf(">>> start Z88AI <<<\n");
  break;

  case TX_NONZ:
    if(LANG == 1) printf("\n" PD " Elemente in IJ entdeckt\n", i);
    if(LANG == 2) printf("\n" PD " elements in IJ detected\n",i);
  break;

  case TX_GSSO:
    if(LANG == 1) printf(">>> alle Pointer assembliert <<<\n");
    if(LANG == 2) printf(">>> pointers assembled <<<\n");
  break;

  case TX_Z88B:
    if(LANG == 1) printf(">>> Start Z88BI: Pass 1 von Z88I2 und Z88PAR <<<\n");
    if(LANG == 2) printf(">>> start Z88BI: pass 1 of Z88I2 and Z88PAR <<<\n");
  break;

  case TX_FORMA:
    if(LANG == 1) printf("***** Formatieren *****\n");
    if(LANG == 2) printf("***** formatting *****\n");
  break;

  case TX_WRIO0:
    if(LANG == 1) printf("Z88O0.TXT beschreiben\n");
    if(LANG == 2) printf("writing Z88O0.TXT\n");
  break;

  case TX_WRIO1:
    if(LANG == 1) printf("Z88O1.TXT beschreiben\n");
    if(LANG == 2) printf("writing Z88O1.TXT\n");
  break;

  case TX_WRI1Y:
    if(LANG == 1) printf("Z88O1.BNY beschreiben\n");
    if(LANG == 2) printf("writing Z88O1.BNY\n");
  break;

  case TX_WRI4Y:
    if(LANG == 1) printf("Z88O4.BNY beschreiben\n");
    if(LANG == 2) printf("writing Z88O4.BNY\n");
  break;

  case TX_ENDI1:
    if(LANG == 1) printf("Ende Z88I1\n\
Sie koennten Z88.DYN einstellen (MAXGS & MAXKOI),\n\
dann starten Sie den Solver Part 2:\nZ88I2 oder Z88PAR\n");
    if(LANG == 2) printf("Z88I1 done\n\
you may now adjust Z88.DYN (MAXGS & MAXKOI)\n\
then lauch the solver part 2:\nZ88I2 or Z88PAR\n");
  break;

  case TX_GSERF:
    if(LANG == 1) printf("Vektor GS erfordert " PD " Elemente\n",i);
    if(LANG == 2) printf("vector GS needs " PD " elements\n",i);
  break;

  case TX_KOIERF:
    if(LANG == 1) printf("Vektor KOI erfordert " PD " Elemente\n",i);
    if(LANG == 2) printf("vector KOI needs " PD " elements\n",i);
  break;

  case TX_WRI2Y:
    if(LANG == 1) printf("Z88O2.BNY beschreiben\n");
    if(LANG == 2) printf("writing Z88O2.BNY\n");
  break;

  case TX_WRI3Y:
    if(LANG == 1) printf("Z88O3.BNY beschreiben\n");
    if(LANG == 2) printf("writing Z88O3.BNY\n");
  break;

  case TX_COMPI:
    if(LANG == 1) printf("***** Compilation *****\n");
    if(LANG == 2) printf("***** compilation *****\n");
  break;

  case TX_REAO1Y:
    if(LANG == 1) printf("Fortsetzung: Z88O1.BNY einlesen\n");
    if(LANG == 2) printf("execution continuing: reading Z88O1.BNY\n");
  break;

  case TX_REA2Y:
    if(LANG == 1) printf("Z88O2.BNY einlesen\n");
    if(LANG == 2) printf("reading Z88O2.BNY\n");
  break;

  case TX_REA4Y:
    if(LANG == 1) printf("Z88O4.BNY einlesen\n");
    if(LANG == 2) printf("reading Z88O4.BNY\n");
  break;

  case TX_Z88CC:
    if(LANG == 1) printf("\n>>> Start Z88CI: Pass 2 von Z88I2 <<<\n");
    if(LANG == 2) printf("\n>>> start Z88CI: pass 2 of Z88I2 <<<\n");
  break;

  case TX_Z88CP:
    if(LANG == 1) printf("\n>>> Start Z88CP: Pass 2 von Z88PAR <<<\n");
    if(LANG == 2) printf("\n>>> start Z88CP: pass 2 of Z88PAR <<<\n");
  break;

  case TX_REAI4:
    if(LANG == 1) printf("Einlesen von Steuerfile Z88I4.TXT\n");
    if(LANG == 2) printf("reading parameter file Z88I4.TXT\n");
  break;

  case TX_REAI2:
    if(LANG == 1) printf("Einlesen von Randbedingungsfile Z88I2.TXT\n");
    if(LANG == 2) printf("reading constraint file Z88I2.TXT\n");
  break;

  case TX_REAI2P2:
    if(LANG == 1) printf("\nEinlesen von Randbedingungsfile Z88I2.TXT\n");
    if(LANG == 2) printf("\nreading constraint file Z88I2.TXT\n");
  break;

  case TX_ERBPA:
    if(LANG == 1) printf("Einarbeiten der Randbedingungen Pass " PD "\n",i);
    if(LANG == 2) printf("incorporating constraints pass " PD "\n",i);
  break;

  case TX_SCAL88:
    if(LANG == 1) printf("Start SCAL88\n");
    if(LANG == 2) printf("start SCAL88\n");
  break;

  case TX_PART88:
    if(LANG == 1) printf("\rpartielle Cholesky- Zerlegung Nr." PD,i);
    if(LANG == 2) printf("\rincomplete Cholesky decomposition no." PD,i);
    fflush(stdout);
  break;

  case TX_SICCG88:
    if(LANG == 1) printf("***** Start des Schwarz-Rieg Solvers SICCG88 *****\n");
    if(LANG == 2) printf("***** start of Schwarz-Rieg solver SICCG88 *****\n");
  break;

  case TX_SORCG88:
    if(LANG == 1) printf("***** Start des Schwarz-Rieg Solvers SORCG88 *****\n");
    if(LANG == 2) printf("***** start of Schwarz-Rieg solver SORCG88 *****\n");
  break;

  case TX_JACOOK:
    if(LANG == 1) printf("\nResiduenvektor < Eps, alles okay!\n");
    if(LANG == 2) printf("\nlimit Eps reached, sounds good!\n");
  break;

  case TX_JACONOTOK:
    if(LANG == 1) printf("\nEps nicht erreicht, maxit erreicht -leider!\n");
    if(LANG == 2) printf("\neps not reached, maxit reached, sorry!\n");
  break;

  case TX_PARA88:
    if(i >= 1 && i <= 9)
      {
      if(LANG == 1) printf("***** Start des Solvers PARA88 in-core, CPUs: " PD " *****\n",i);
      if(LANG == 2) printf("***** start of solver PARA88 in-core, CPUs: " PD " *****\n",i);
      }
    if(i >= 11 && i <= 19)
      {
      if(LANG == 1) printf("***** Start des Solvers PARA88 out-of-core, CPUs: " PD " *****\n",i-10);
      if(LANG == 2) printf("***** start of solver PARA88 out-of-core, CPUs: " PD " *****\n",i-10);
      }
  break;

  case TX_UMSPS:
    if(LANG == 1) printf("GS umgespeichen... ");
    if(LANG == 2) printf("reformatting GS ... ");
  break;

  case TX_UMSPF:
    if(LANG == 1) printf("GS in PARDISO-Format umgespeichert\n");
    if(LANG == 2) printf("GS stored in PARDISO format\n");
  break;

  case TX_SPAR:
    if(LANG == 1) printf("****** Start PARDISO ******\n");
    if(LANG == 2) printf("****** running PARDISO ******\n");
  break;

  case TX_PARA88OK:
    if(LANG == 1) printf("PARDISO sauber gelaufen\n");
    if(LANG == 2) printf("PARDISO cleanly finished\n");
  break;

  case TX_PARA88NOTOK:
    if(LANG == 1) printf("# PARDISO abgebrochen #\n");
    if(LANG == 2) printf("# PARDISO stopped with errors #\n");
  break;

  case TX_LLT:
      printf("LLT Solver\n");
   break;

  case TX_CG:
      printf("CG Solver\n");
   break;

  case TX_MINRES:
      printf("MINRES Solver\n");
   break;

  case TX_REBA88:
    if(LANG == 1) printf("Umbau Sparse-Matrix auf CCS-Format\n");
    if(LANG == 2) printf("Building CCS of Sparse Matrix\n");
  break;

  case TX_WRIO2:
    if(LANG == 1) printf("Z88O2.TXT beschreiben, Ende Z88I2\n");
    if(LANG == 2) printf("writing Z88O2.TXT, Z88I2 done\n");
  break;

  case TX_WRIO2P:
    if(LANG == 1) printf("Z88O2.TXT beschreiben, Ende Z88PAR\n");
    if(LANG == 2) printf("writing Z88O2.TXT, Z88PAR done\n");
  break;

  }
return(0);
}

/***********************************************************************
*  function wtyp88i gibt Elementtypen in Z88I aus
***********************************************************************/ 
int wtyp88i(FR_INT4 k,FR_INT4 i)
{
extern FR_INT4 LANG;

if(LANG == 1) printf("\rNr. " PD " Typ " PD "                         ",k,i);
if(LANG == 2) printf("\rno. " PD " type " PD "                        ",k,i);

return(0);
}

/***********************************************************************
*  function wfor88i gibt kfoun beim Sortieren in Z88I1 aus
***********************************************************************/ 
int wfor88i(FR_INT4 k,FR_INT4 i)
{
extern FR_INT4 LANG;

if(LANG == 1) printf("\rnoch " PD9 " Schritte, Pointer IEZ = " PD9,k,i);
if(LANG == 2) printf("\rstill " PD9 " steps, pointer IEZ = " PD9,k,i);

return(0);
}

/***********************************************************************
*  function wran88i gibt Randbedingungen aus
***********************************************************************/ 
int wran88i(FR_INT4 k,FR_INT4 i)
{
extern FR_INT4 LANG;

if(LANG == 1) printf("\rRandbedingung Nr. " PD " Typ " PD,k,i);
if(LANG == 2) printf("\rconstraint no. " PD " type " PD,k,i);

fflush(stdout);
return(0);
}
