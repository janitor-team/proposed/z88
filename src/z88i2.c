/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V12.0 February 14, 2005
*
* Z88 should compile and run under any UNIX OS and Motif 2.0.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/****************************************************************************
*  Programm z88i2.c
*  der FEA- Iterations- Solver - Part 2
*  8.5.2008 Rieg
****************************************************************************/

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88i.h>  
#include <stdio.h>    /* FILE */
#include <ctype.h>    /* toupper */
#include <string.h>   /* strcpy,strcmp */
#endif

/****************************************************************************
*  Function-Declarationen
****************************************************************************/
int dyn88i2(void);
int ale88i(int);
int who88i2(void);
int z88bi(void);
int z88ci(void);
int lan88i2(void);
void stop88i(void);
int r1y88i(void);
int r4y88i(void);

/****************************************************************************
*  globale Variable
****************************************************************************/
/*--------------------------------------------------------------------------
* Files
*-------------------------------------------------------------------------*/
FILE *fdyn,*fl2,*fi2,*fi4,*fi5,*f1y,*f3y,*f4y,*fo1,*fo2;

/*  
**   fdyn= z88.dyn
**   fl2 = z88i2.log
**   fi2=  z88i2.txt
**   fi4=  z88i4.txt
**   fi5=  z88i5.txt
**   f1y=  z88o1.bny
**   f3y=  z88o3.bny
**   f4y=  z88o4.bny
**   fo1=  z88o1.txt
**   fo2=  z88o2.txt
*/ 

char cdyn[8] = "z88.dyn";
char cl2[10] = "z88i2.log";
char ci2[10] = "z88i2.txt";
char ci4[10] = "z88i4.txt";
char ci5[10] = "z88i5.txt";
char c1y[10] = "z88o1.bny";
char c3y[10] = "z88o3.bny";
char c4y[10] = "z88o4.bny";
char co1[10] = "z88o1.txt";
char co2[10] = "z88o2.txt";
  
/*--------------------------------------------------------------------------
* Pointer
*-------------------------------------------------------------------------*/
FR_DOUBLEAY GS;
FR_DOUBLEAY CI;
FR_DOUBLEAY se;
FR_DOUBLEAY rs;
FR_DOUBLEAY xi;
FR_DOUBLEAY xa;
FR_DOUBLEAY v;
FR_DOUBLEAY pk;
FR_DOUBLEAY zz;
FR_DOUBLEAY fak;
FR_DOUBLEAY x;
FR_DOUBLEAY y;
FR_DOUBLEAY z;
FR_DOUBLEAY emod;
FR_DOUBLEAY rnue;
FR_DOUBLEAY qpara;
FR_DOUBLEAY riyy;
FR_DOUBLEAY eyy;
FR_DOUBLEAY rizz;
FR_DOUBLEAY ezz;
FR_DOUBLEAY rit;
FR_DOUBLEAY wt;
FR_DOUBLEAY pres;
FR_DOUBLEAY tr1;
FR_DOUBLEAY tr2;

FR_INT4AY ip;
FR_INT4AY iez;
FR_INT4AY koi;
FR_INT4AY ifrei; 
FR_INT4AY ioffs;
FR_INT4AY koffs;
FR_INT4AY ityp;
FR_INT4AY ivon;
FR_INT4AY ibis;
FR_INT4AY intord;
FR_INT4AY nep;
FR_INT4AY noi;
FR_INT4AY noffs;

/*--------------------------------------------------------------------------
* Arrays
*-------------------------------------------------------------------------*/
FR_INT4 mcomp[21];                     /* 21 ist MAXPA */

/* Diese Arrays werden in HEXA88,LQUA88,QSHE88 und CSHE88 verwendet */

FR_DOUBLE xk[21], yk[21], zk[21];      /* 21 ist MAXPA , HEXA88 */
FR_DOUBLE h[21];                       /* 21 ist MAXPA , HEXA88 */
FR_DOUBLE b[361];                      /* ist 6 x 60 +1, HEXA88 */
FR_DOUBLE xx[61];                      /* ist 3 x 20 +1, HEXA88 */
FR_DOUBLE d[37];                       /* ist 6 x 6  +1, HEXA88 */
FR_DOUBLE p[61];                       /* ist 3 x 20 +1, HEXA88 */

/* fuer Plattenberechnung */
FR_DOUBLE be[49];                         /* fuer 16-Knoten Platte  */
FR_DOUBLE hi[49];
FR_DOUBLE hj[49];
FR_DOUBLE hk[49];
FR_DOUBLE bbi[145];
FR_DOUBLE bsv[97];
FR_DOUBLE dbi[10];
FR_DOUBLE dsv[5];

/*--------------------------------------------------------------------------
* Variable
*-------------------------------------------------------------------------*/
FR_DOUBLE emode,rnuee,qparae,riyye,eyye,rizze,ezze,rite,wte,eps,rp;
FR_DOUBLE  rpomega,rpalpha;
FR_DOUBLE pree,tr1e,tr2e;
FR_INT4 intore,nel,ktyp,maxit,kfoun,jelem;
FR_INT4 LANG,IDYNMEM,ICFLAG;
FR_INT4 ndim,nkp,ne,nfg,neg,nfgp1,nkoi,kflag,ibflag,ipflag,npr,iqflag;

/*--------------------------------------------------------------------------
* vorbelegte Variable
*-------------------------------------------------------------------------*/
FR_INT4 MAXGS=0,MAXNFG=0,MAXK=0,MAXE=0,MAXKOI=0,MAXNEG=0;
FR_INT4 MAXESM=3600,MAXPR=0;

/*****************************************************************************
* hauptprogramm
*****************************************************************************/
int main(int argc,char *argv[])
{
int iret,i;
char cflag[10],cflagul[10];

/*---------------------------------------------------------------------------
*  Die Sprache feststellen
*--------------------------------------------------------------------------*/
LANG = 0;
iret= lan88i2();

if(iret != 0)
  {
  ale88i(iret);
  stop88i();
  }

/*---------------------------------------------------------------------------
*  Ueberschrift
*--------------------------------------------------------------------------*/
who88i2();

/*---------------------------------------------------------------------------
*  Die Flags abfragen
*--------------------------------------------------------------------------*/
ICFLAG= 0;

if(argc <= 1)
  {
  ale88i(AL_NOCFLAG); 
  stop88i();              
  }

strcpy(cflagul,argv[1]);  
for(i= 0; i < 6; i++)
  cflag[i]= toupper(cflagul[i]);
  
if     (strcmp("-C",cflag) == 0)
  ICFLAG= 1;
else if(strcmp("-S",cflag) == 0)
  ICFLAG= 2;
else if(strcmp("-BDOF",cflag) == 0)
  printf("\n140655\n\n");
else
  { 
  ale88i(AL_WROCFLAG); 
  stop88i();              
  }      

/*---------------------------------------------------------------------------
* dyn88i2 starten: anfordern dynamic memory
*--------------------------------------------------------------------------*/
iret= dyn88i2();
if(iret != 0)
  {
  ale88i(iret);
  stop88i();              
  }           

/*---------------------------------------------------------------------------
* Einlesen von Z88O1.BNY und Z88O4.BNY
*--------------------------------------------------------------------------*/
iret= r1y88i();
if(iret != 0)
  {
  ale88i(iret);
  stop88i();              
  }           

iret= r4y88i();
if(iret != 0)
  {
  ale88i(iret);
  stop88i();              
  }           

/*---------------------------------------------------------------------------
* Rechnen
*--------------------------------------------------------------------------*/
iret= z88bi();
if(iret != 0)
  {
  ale88i(iret);
  stop88i();              
  }           

iret= z88ci();
if(iret != 0)
  {
  ale88i(iret);
  stop88i();              
  }           

return(0);
}           

