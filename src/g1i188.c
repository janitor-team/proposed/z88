/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V12.0  February 14, 2005
*
* Z88 should compile and run under any UNIX OS and Motif 2.0.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/**********************************************************************
* Compilerunit g1i188 enthaelt:
*
* Function g1i188 prueft die erste zeile fuer Z88I1.TXT & Z88NI.TXT
* Function erif88: falscher Fileaufbau
*
* 15.9.2005 Rieg
**********************************************************************/

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88v.h>
#include <stdio.h>   /* FILE,printf */
#endif

/***********************************************************************
* Functions
***********************************************************************/
void erif88(FR_INT4 izeile);

/***********************************************************************
* Start G1I188
***********************************************************************/
int g1i188(void)
{
extern FILE *fdatei;

extern FR_INT4 MAXK,MAXE,MAXNFG,MAXNEG;
extern FR_INT4 ndim,nkp,ne,nfg,neg,kflag,ibflag,niflag,izeile,ifnii1;
extern FR_INT4 ipflag,iqflag;
extern FR_INT4 LANG;
     
int ier;

char *cresult;
char cline[256];
                
/**********************************************************************
* Checken der 1.Zeile
**********************************************************************/
izeile= 1; 

cresult= fgets(cline,256,fdatei);
if(!cresult)
  {
  erif88(izeile);
  return(2);
  }

if(ifnii1 == 0)              /* fuer Z88I1.TXT */
  {          
  ier= sscanf(cline,"%ld %ld %ld %ld %ld %ld %ld %ld %ld",
  &ndim,&nkp,&ne,&nfg,&neg,&kflag,&ibflag,&ipflag,&iqflag);
  if(ier != 9) 
    {
    printf("%s\n",cline);
    if(LANG == 1)
    printf("### Schreibfehler oder fehlende Daten in Zeile 1 entdeckt\n");
    if(LANG == 2)
    printf("### typing error or missing entries in line 1 detected\n");
    return(2);
    }
  }

if(ifnii1 == 1)              /* fuer Z88NI.TXT */
  {          
  ier= sscanf(cline,"%ld %ld %ld %ld %ld %ld %ld %ld %ld %ld",
  &ndim,&nkp,&ne,&nfg,&neg,&kflag,&ibflag,&ipflag,&iqflag,&niflag);
  if(ier != 10) 
    {
    printf("%s\n",cline);
    if(LANG == 1)
    printf("### Schreibfehler oder fehlende Daten in Zeile 1 entdeckt\n");
    if(LANG == 2)
    printf("### typing error or missing entries in line 1 detected\n");
    return(2);
    }
  }
        
/*---------------------------------------------------------------------
* ndim ?
*--------------------------------------------------------------------*/
if(!(ndim == 3 || ndim == 2))
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### Dimension der Struktur nicht 2 oder 3\n");
    printf("### 1.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### dimension of structure neither 2 nor 3\n");
    printf("### check 1st entry in line 1\n");
    }
  return(2);
  }

/*---------------------------------------------------------------------
* nkp ?
*--------------------------------------------------------------------*/
if(nkp <= 0)
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### Anzahl Knotenpunkte nicht groesser 0\n");
    printf("### 2.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### number of nodal points not greater than 0\n");
    printf("### check 2nd entry in line 1\n");
    }
  return(2);
  }

if(nkp > MAXK)
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### Anzahl Knotenpunkte groesser als %ld in Z88.DYN\n",MAXK);
    printf("### 2.Wert in Zeile 1 pruefen oder MAXK in Z88.DYN erhoehen\n");
    }
  if(LANG == 2)
    {
    printf("### number of nodal points greater than %ld in Z88.DYN\n",MAXK);
    printf("### check 2nd entry in line 1 or increase MAXK in Z88.DYN\n");
    }
  return(2);
  }

/*---------------------------------------------------------------------
* ne ?
*--------------------------------------------------------------------*/
if(ne <= 0)
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### Anzahl Elemente nicht groesser 0\n");
    printf("### 3.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### number of elements not greater than 0\n");
    printf("### check 3rd entry in line 1\n");
    }
  return(2);
  }

if(ne > MAXE)
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### Anzahl Elemente groesser als %ld in Z88.DYN\n",MAXE);
    printf("### 3.Wert in Zeile 1 pruefen oder MAXE in Z88.DYN erhoehen\n");
    }
  if(LANG == 2)
    {
    printf("### number of elements greater than %ld in Z88.DYN\n",MAXE);
    printf("### check 3rd entry in line 1 or increase MAXE in Z88.DYN\n");
    }
  return(2);
  }

/*---------------------------------------------------------------------
* nfg ?
*--------------------------------------------------------------------*/
if(ndim == 2)
  {
  if(nfg < 2*nkp && ipflag == 0)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Anzahl Freiheitsgrade nicht groesser als min. 2*NKP\n");
      printf("### 4.Wert in Zeile 1 ueberpruefen\n");
      }
    if(LANG == 2)
      {
      printf("### number of DOF not greater than 2*NKP at least\n");
      printf("### check 4th entry in line 1\n");
      }
    return(2);
    }
  if(nfg < 3*nkp && ipflag != 0)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Platten: Anzahl FG nicht groesser als min. 3*NKP\n");
      printf("### 4.Wert in Zeile 1 ueberpruefen\n");
      }
    if(LANG == 2)
      {
      printf("### plates: number of DOF not greater than 3*NKP at least\n");
      printf("### check 4th entry in line 1\n");
      }
    return(2);
    }

  }

if(ndim == 3)
  {
  if(nfg < 3*nkp)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Anzahl Freiheitsgrade nicht groesser als min. 3*NKP\n");
      printf("### 4.Wert in Zeile 1 ueberpruefen\n");
      }
    if(LANG == 2)
      {
      printf("### number of DOF not greater than 3*NKP at least\n");
      printf("### check 4th entry in line 1\n");
      }
    return(2);
    }
  }

if(nfg > MAXNFG)
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### Anzahl Freiheitsgrade groesser als %ld in Z88.DYN\n",MAXNFG);
    printf("### 4.Wert in Zeile 1 pruefen oder MAXNFG in Z88.DYN erhoehen\n");
    }
  if(LANG == 2)
    {
    printf("### number of DOF greater than %ld in Z88.DYN\n",MAXNFG);
    printf("### check 4th entry in line 1 or increase MAXNFG in Z88.DYN\n");
    }
  return(2);
  }

/*---------------------------------------------------------------------
* neg ?
*--------------------------------------------------------------------*/
if(neg < 1)
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### Anzahl E-Gesetze kleiner 1\n");
    printf("### 5.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### number of mat lines less than 1\n");
    printf("### check 5th entry in line 1\n");
    }
  return(2);
  }

if(neg > MAXNEG)
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### Anzahl E-Gesetze groesser als %ld in Z88.DYN\n",MAXNEG);
    printf("### 5.Wert in Zeile 1 pruefen oder MAXNEG in Z88.DYN erhoehen\n");
    }
  if(LANG == 2)
    {
    printf("### number of mat lines greater than %ld in Z88.DYN\n",MAXNEG);
    printf("### check 5th entry in line 1 or increase MAXNEG in Z88.DYN\n");
    }
  return(2);
  }

/*---------------------------------------------------------------------
* kflag ?
*--------------------------------------------------------------------*/
if(!(kflag == 0 || kflag == 1))
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### Koordinatenflag nicht 0 oder 1\n");
    printf("### 6.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### coordinate flag neither 0 nor 1\n");
    printf("### check 6th entry in line 1\n");
    }
  return(2);
  }

/*---------------------------------------------------------------------
* ibflag ?
*--------------------------------------------------------------------*/
if(!(ibflag == 0 || ibflag == 1))
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### Balkenflag nicht 0 oder 1\n");
    printf("### 7.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### beam flag neither 0 nor 1\n");
    printf("### check 7th entry in line 1\n");
    }
  return(2);
  }

/*---------------------------------------------------------------------
* ipflag ?
*--------------------------------------------------------------------*/
if(!(ipflag == 0  || ipflag == 1 || ipflag == 2))
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### Plattenflag nicht 0, 1 oder 2\n");
    printf("### 8.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### plate flag neither 0, 1 nor 2\n");
    printf("### check 8th entry in line 1\n");
    }
  return(2);
  }

if(ipflag != 0 && ibflag == 1)
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### Plattenflag und Balkenflag gleichzeitig gesetzt\n");
    printf("### in momentaner Z88- Version nicht zulaessig\n");
    printf("### 7. und 8.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### both plate flag and beam flag set\n");
    printf("### not allowed in this Z88 release\n");
    printf("### check 7th and 8th entry in line 1\n");
    }
  return(2);
  }

/*---------------------------------------------------------------------
* iqflag ?
*--------------------------------------------------------------------*/
if(!(iqflag == 0 || iqflag == 1))
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### Flaechenlastflag nicht 0 oder 1\n");
    printf("### 9.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### surface load flag neither 0 nor 1\n");
    printf("### check 9th entry in line 1\n");
    }
  return(2);
  }

if(ipflag == 1 && iqflag == 1)
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf(">>> Hinweis: IPFLAG = 1 und IQFLAG = 1\n");
    printf(">>> Ist fuer Platten 18,19 und 20 zulaessig\n");
    printf(">>> Dann Plattendruck in Datei Z88I5.TXT geben\n");
    }
  if(LANG == 2)
    {
    printf(">>> Attention: IPFLAG = 1 and IQFLAG = 1\n");
    printf(">>> However, is allowed for plate elements No.18,19 & 20\n");
    printf(">>> But then enter surface load into file Z88I5.TXT\n");
    }
  }

/*---------------------------------------------------------------------
* niflag ?
*--------------------------------------------------------------------*/
if(ifnii1 ==1)
  {
  if(!(niflag == 0 || niflag == 1))
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Fangradiusflag nicht 0 oder 1\n");
      printf("### 10.Wert in Zeile 1 ueberpruefen\n");
      }
    if(LANG == 2)
      {
      printf("### trap radius flag neither 0 nor 1\n");
      printf("### check 10th entry in line 1\n");
      }
    return(2);
    }
  }
      
/***********************************************************************
* Ende grp1: keine Fehler
***********************************************************************/
return(0);
}

/***********************************************************************
* Function erif88
***********************************************************************/
void erif88(FR_INT4 izeile)
{  
extern FR_INT4 LANG;

if(LANG == 1)
  {
  printf("### versuchte Zeile %ld zu lesen, aber Fileende wurde gefunden\n",
  izeile);
  printf("### Fileaufbau falsch, File zu kurz oder leer\n");
  printf("### evtl. fehlt ein CR/LF am Zeilenende\n");
  }

if(LANG == 2)
  {
  printf("### tried to read line %ld, but end of file was found\n",izeile);
  printf("### file format wrong, file too short or empty\n");
  printf("### perhaps is a CR/LF missing at end of file\n");
  }
  
return;
}         
