/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V10.0  December 12, 2001
*
* Z88 should compile and run under any UNIX OS and Motif 2.0.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/***********************************************************************
* wlog88n gibt Log-Datei-Meldungen aus (1 FR_INT4, 1 int)
* 24.7.2002 Rieg
***********************************************************************/ 

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88n.h>
#include <stdio.h>   /* FILE,fprintf,fflush */
#endif

/***********************************************************************
* Fuer Windows 95
***********************************************************************/
#ifdef FR_WIN95
#include <z88n.h>
#include <stdio.h>   /* FILE,fprintf,fflush */
#endif

/***********************************************************************
* hier beginnt Function wlog88n
***********************************************************************/
int wlog88n(FR_INT4 i,int iatx)
{
extern FILE *fwlo;
extern FR_INT4 LANG;

switch(iatx)
  {
  case LOG_BZ88N:
    if(LANG == 1) fprintf(fwlo,"Start Z88N Version 10");
    if(LANG == 2) fprintf(fwlo,"start Z88N version 10");
    fflush(fwlo);
  break;

  case LOG_OPENDYN:
    if(LANG == 1) fprintf(fwlo,"\nOeffnen der Datei Z88.DYN");
    if(LANG == 2) fprintf(fwlo,"\nopening file Z88.DYN");
    fflush(fwlo);
  break;

  case LOG_NODYN:
    if(LANG == 1)
    fprintf(fwlo,"\n### kann Z88.DYN nicht oeffnen ..Stop ###");
    if(LANG == 2)
    fprintf(fwlo,"\n### cannot open Z88.DYN ..stop ###");
    fflush(fwlo);
  break;

  case LOG_WRONGDYN:
    if(LANG == 1)
    fprintf(fwlo,"\n### File Z88.DYN ist nicht korrekt ..Stop ###");
    if(LANG == 2)
    fprintf(fwlo,"\n### file Z88.DYN is not correct ..stop ###");
    fflush(fwlo);
  break;

  case LOG_MAXSE:
    fprintf(fwlo,"\nMAXSE  = %ld",i);
    fflush(fwlo);
  break;

  case LOG_MAXESS:
    fprintf(fwlo,"\nMAXESS = %ld",i);
    fflush(fwlo);
  break;

  case LOG_MAXKSS:
    fprintf(fwlo,"\nMAXKSS = %ld",i);
    fflush(fwlo);
  break;

  case LOG_MAXAN:
    fprintf(fwlo,"\nMAXAN  = %ld",i);
    fflush(fwlo);
  break;

  case LOG_MAXK:
    fprintf(fwlo,"\nMAXK   = %ld",i);
    fflush(fwlo);
  break;

  case LOG_MAXE:
    fprintf(fwlo,"\nMAXE   = %ld",i);
    fflush(fwlo);
  break;

  case LOG_MAXNFG:
    fprintf(fwlo,"\nMAXNFG = %ld",i);
    fflush(fwlo);
  break;

  case LOG_MAXNEG:
    fprintf(fwlo,"\nMAXNEG = %ld",i);
    fflush(fwlo);
  break;

  case LOG_OKDYN:
    if(LANG == 1)
    fprintf(fwlo,"\nDatei Z88.DYN gelesen..scheint formal o.k. zu sein");
    if(LANG == 2)
    fprintf(fwlo,"\nfile Z88.DYN read .. seems to be o.k.");
    fflush(fwlo);
  break;

  case LOG_ALLOCMEMY:
    if(LANG == 1) fprintf(fwlo,"\nDynamisches Memory anlegen:");
    if(LANG == 2) fprintf(fwlo,"\nallocating dynamic memory:");
    fflush(fwlo);
  break;

  case LOG_ARRAYNOTOK:
    if(LANG == 1)
    fprintf(fwlo,"\n### Memory Kennung %ld nicht o.k. ..Stop ###",i);
    if(LANG == 2)
    fprintf(fwlo,"\n### memory id %ld is not o.k. ..stop ###",i);
    fflush(fwlo);
  break;

  case LOG_ARRAYOK:
    if(LANG == 1) fprintf(fwlo,"\nMemory Kennung %ld angelegt",i);
    if(LANG == 2) fprintf(fwlo,"\nmemory id %ld allocated",i);
    fflush(fwlo);
  break;

  case LOG_SUMMEMY:
    if(LANG == 1)
    fprintf(fwlo,"\nDynamisches Memory vollstaendig angefordert: %ld Bytes",i);
    if(LANG == 2)
    fprintf(fwlo,"\ndynamic memory totally allocated: %ld Bytes",i);
    fflush(fwlo);
  break;

  case LOG_EXITDYN88N:
    if(LANG == 1) fprintf(fwlo,"\nVerlassen Speichereinheit DYN88N");
    if(LANG == 2) fprintf(fwlo,"\nleaving storage function DYN88N");
    fflush(fwlo);
  break;

  case LOG_REANI:
    if(LANG == 1) fprintf(fwlo,"\nStart Leseeinheit RNI88");
    if(LANG == 2) fprintf(fwlo,"\nstart reading function RNI88");
    fflush(fwlo);
  break;

  case LOG_OPENNI:
    if(LANG == 1) fprintf(fwlo,"\nOeffnen der Datei Z88NI.TXT");
    if(LANG == 2) fprintf(fwlo,"\nopening file Z88NI.TXT");
    fflush(fwlo);
  break;

  case LOG_NONI:
    if(LANG == 1)
    fprintf(fwlo,"\n### kann Z88NI.TXT nicht oeffnen ..Stop ###");
    if(LANG == 2)
    fprintf(fwlo,"\n### cannot open Z88NI.TXT ..stop ###");
    fflush(fwlo);
  break;

  case LOG_OPENI1:
    if(LANG == 1) fprintf(fwlo,"\nOeffnen der Datei Z88I1.TXT");
    if(LANG == 2) fprintf(fwlo,"\nopening file Z88I1.TXT");
    fflush(fwlo);
  break;

  case LOG_NOI1:
    if(LANG == 1)
    fprintf(fwlo,"\n### kann Z88I1.TXT nicht oeffnen ..Stop ###");
    if(LANG == 2)
    fprintf(fwlo,"\n### cannot open Z88I1.TXT ..stop ###");
    fflush(fwlo);
  break;

  case LOG_EXSUPERK:
    if(LANG == 1)
      {
      fprintf(fwlo,"\n### nur %ld Superknoten zulaessig .. Stop ###\n",i);
      fprintf(fwlo,"\n### Abhilfe: MAXKSS in Z88.DYN erhoehen   ###\n");
      }  
    if(LANG == 2)
      {
      fprintf(fwlo,"\n### only %ld super-nodes allowed ..stop ###\n",i);
      fprintf(fwlo,"\n### recover: increase MAXKSS in Z88.DYN ###\n");
      }
    fflush(fwlo);
  break;

  case LOG_EXSUPERE:
    if(LANG == 1)
      {
      fprintf(fwlo,"\n### nur %ld Superelemente zulaessig .. Stop ###\n",i);
      fprintf(fwlo,"\n### Abhilfe: MAXESS in Z88.DYN erhoehen     ###\n");
      }  
    if(LANG == 2)
      {
      fprintf(fwlo,"\n### only %ld super-elements allowed ..stop ###\n",i);
      fprintf(fwlo,"\n### recover: increase MAXESS in Z88.DYN    ###\n");
      }
    fflush(fwlo);
  break;

  case LOG_EXITRNI88:
    if(LANG == 1)
    fprintf(fwlo,"\nVerlassen Leseeinheit RNI88: File Z88NI.TXT gelesen");
    if(LANG == 2)
    fprintf(fwlo,"\nleaving reading function RNI88: file Z88NI.TXT read");
    fflush(fwlo);
  break;

  case LOG_BERJOIN:
    if(LANG == 1) fprintf(fwlo,"\nBerechne Vektor Join");
    if(LANG == 2) fprintf(fwlo,"\ncomputing vector Join");
    fflush(fwlo);
  break;

  case LOG_EXITJOIN:
    if(LANG == 1) fprintf(fwlo,"\nEnde Berechnung Join");
    if(LANG == 2) fprintf(fwlo,"\nJoin computed");
    fflush(fwlo);
  break;

  case LOG_SMC188N:
    if(LANG == 1)
      {
      fprintf(fwlo,"\nStarten MC188N: Typ 10 --> Typ 10");
      fprintf(fwlo,"\nKoordinaten berechnen");
      }
    if(LANG == 2)
      {
      fprintf(fwlo,"\nstart MC188N: type 10 --> type 10");
      fprintf(fwlo,"\ncomputing coordinates");
      }
    fflush(fwlo);
  break;

  case LOG_SMC288N:
    if(LANG == 1)
      {
      fprintf(fwlo,"\nStarten MC288N: Typ 10 --> Typ 1");
      fprintf(fwlo,"\nKoordinaten berechnen");
      }
    if(LANG == 2)
      {
      fprintf(fwlo,"\nstart MC288N: type 10 --> type 1");
      fprintf(fwlo,"\ncomputing coordinates");
      }
    fflush(fwlo);
  break;

  case LOG_SMC388N:
    if(LANG == 1)
      {
      fprintf(fwlo,"\nStarten MC388N: Typ 7/8 --> Typ 7/8");
      fprintf(fwlo,"\nKoordinaten berechnen");
      }
    if(LANG == 2)
      {
      fprintf(fwlo,"\nstart MC388N: type 7/8 --> type 7/8");
      fprintf(fwlo,"\ncomputing coordinates");
      }
    fflush(fwlo);
  break;

  case LOG_SMC488N:
    if(LANG == 1)
      {
      fprintf(fwlo,"\nStarten MC488N: Typ 11/12 --> Typ 7/8");
      fprintf(fwlo,"\nKoordinaten berechnen");
      }
    if(LANG == 2)
      {
      fprintf(fwlo,"\nstart MC488N: type 11/12 --> type 7/8");
      fprintf(fwlo,"\ncomputing coordinates");
      }
    fflush(fwlo);
  break;

  case LOG_SMC588N:
    if(LANG == 1)
      {
      fprintf(fwlo,"\nStarten MC588N: Typ 20 --> Typ 19");
      fprintf(fwlo,"\nKoordinaten berechnen");
      }
    if(LANG == 2)
      {
      fprintf(fwlo,"\nstart MC588N: type 20 --> type 19");
      fprintf(fwlo,"\ncomputing coordinates");
      }
    fflush(fwlo);
  break;

  case LOG_TOBIG1:
    if(LANG == 1)
      {
      fprintf(fwlo,"\n### Dynamischer Speicher erschoepft.. Stop ###");
      fprintf(fwlo,"\n### Abhilfe: kleinere Struktur anfordern   ###");
      fprintf(fwlo,"\n### oder MAXSE in Z88.DYN erhoehen         ###\n");
      }
    if(LANG == 2)
      {
      fprintf(fwlo,"\n### dynamic memory exhausted.. stop ###");
      fprintf(fwlo,"\n### recover: generate smaller net   ###");
      fprintf(fwlo,"\n### or increase MAXSE in Z88.DYN    ###\n");
      }
    fflush(fwlo);
  break;

  case LOG_BERKOIN:
    if(LANG == 1) fprintf(fwlo,"\nKoinzidenzvektor berechnen");
    if(LANG == 2) fprintf(fwlo,"\ncomputing element informations");
    fflush(fwlo);
  break;

  case LOG_WRII1:
    if(LANG == 1) fprintf(fwlo,"\nZ88O1.TXT beschreiben, Ende Z88N");
    if(LANG == 2) fprintf(fwlo,"\nwriting Z88O1.TXT, Z88N done");
    fflush(fwlo);
  break;

  case LOG_FEEXID:
    if(LANG == 1)
      {
      fprintf(fwlo,"\n### Achtung ! Soeben generierte Struktur ist      ###");
      fprintf(fwlo,"\n###          fuer einen Rechenlauf zu gross       ###");
      fprintf(fwlo,"\n### Abhilfe: MAXK,MAXE,MAXNFG in Z88.DYN erhoehen ###\n");
      }
    if(LANG == 2)
      {
      fprintf(fwlo,"\n### Warning! just generated net is too   ###");
      fprintf(fwlo,"\n### heavy for a Z88F - run !  recover:   ###");
      fprintf(fwlo,"\n### increase MAXK,MAXE,MAXNFG in Z88.DYN ###\n");
      }
    fflush(fwlo);
  break;

  }
return(0);
}
