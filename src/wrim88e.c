/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V10.0  December 12, 2001
*
* Z88 should compile and run under any UNIX OS and Motif 2.0.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/***********************************************************************
* Diese Compilerunit umfasst:
* - wrim88e gibt Texte aus (1 FR_INT4)
* - wtyp88e gibt Elementtypen aus (1 FR_INT4, 1 FR_INT4)
* 18.1.2008 Rieg
***********************************************************************/ 

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88e.h>
#include <stdio.h>    /* printf */
#endif

/***********************************************************************
* Formate
***********************************************************************/
#ifdef FR_XINT
#define P5D "%5d"
#endif

#ifdef FR_XLONG
#define P5D "%5ld"
#endif

#ifdef FR_XLOLO
#define P5D "%5lld"
#endif

/***********************************************************************
*  hier beginnt Function wrim88e
***********************************************************************/
int wrim88e(FR_INT4 i,int iatx)
{
extern FR_INT4 LANG;

switch(iatx)
  {
  case TX_REAO1Y:
    if(LANG == 1) printf("Z88O1.BNY einlesen\n");
    if(LANG == 2) printf("reading Z88O1.BNY\n");
    break;
  case TX_REAO3Y:
    if(LANG == 1) printf("Z88O3.BNY einlesen\n");
    if(LANG == 2) printf("reading Z88O3.BNY\n");
    break;
  case TX_KNOTENK:
    if(LANG == 1) printf("***** Knotenkraefte *****\n");
    if(LANG == 2) printf("***** nodal forces *****\n");
    break;
  case TX_EXITZ88E:
    if(LANG == 1) printf("\nEnde Z88E\n");
    if(LANG == 2) printf("\nZ88E done\n");
    break;
  }
return(0);
}

/***********************************************************************
*  function wtyp88e gibt Elementtypen in Z88E aus
***********************************************************************/ 
int wtyp88e(FR_INT4 k,FR_INT4 i)
{
extern FR_INT4 LANG;

if(LANG == 1) printf("\rNr. " P5D " Typ " P5D,k,i);
if(LANG == 2) printf("\rno. " P5D " type " P5D,k,i);

return(0);
}
