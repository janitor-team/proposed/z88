/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V12.0  February 14, 2005
*
* Z88 should compile and run under any UNIX OS and Motif 2.0.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/**********************************************************************
* Function g4i188 prueft die 4.Eingabegruppe fuer Z88I1.TXT & Z88NI.TXT
* 4.10.2005 Rieg
**********************************************************************/

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88v.h>
#include <stdio.h>   /* FILE,printf */
#include <string.h>  /* strcpy */
#endif

/***********************************************************************
* Functions
***********************************************************************/
void erif88(FR_INT4 izeile);

/***********************************************************************
* Start G4I188
***********************************************************************/
int g4i188(void)
{
extern FILE *fdatei;
        
extern FR_INT4AY jtyp;

extern FR_INT4 ne,neg,ibflag,ipflag,iqflag,iwarn,izeile,LANG;

FR_DOUBLE emod,rnue,qpara,riyy,eyy,rizz,ezz,rit,wt;
       
FR_INT4 ivon,ibis,ibisalt,intord,i,j;

int ier;

char *cresult;
char cline[256],clina[256];
                
/**********************************************************************
* Checken der 4.Gruppe in Schleife
**********************************************************************/
ibisalt= 0;  /* nur wg. compiler warnings */

for(i = 1;i <= neg;i++)                          /* 40 */
  {
  izeile++;

  if(i > 1)
    ibisalt= ibis;
            
  cresult= fgets(cline,256,fdatei);
  if(!cresult)
    {
    erif88(izeile);
    return(2);
    }

/*----------------------------------------------------------------------
* Schreibfehler ?
*---------------------------------------------------------------------*/
  if(ibflag == 0)
    {
    ier= sscanf(cline,"%ld %ld %lg %lg %ld %lg",
    &ivon,&ibis,&emod,&rnue,&intord,&qpara);
    if(ier != 6) 
      {
      printf("%s\n",cline);
      if(LANG == 1)
        printf(
        "### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",
        izeile);
      if(LANG == 2)
        printf(
        "### typing error or missing entries in line %ld detected\n",
        izeile);
      return(2);
      }
    }

  if(ibflag == 1)
    {
    ier= sscanf(cline,"%ld %ld %lg %lg %ld %lg %lg %lg %lg %lg %lg %lg",
    &ivon,&ibis,&emod,&rnue,&intord,&qpara,&riyy,&eyy,&rizz,&ezz,&rit,&wt);
    if(ier != 12) 
      {
      printf("%s\n",cline);
      if(LANG == 1)
        printf(
        "### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",
        izeile);
      if(LANG == 2)
        printf(
        "### typing error or missing entries in line %ld detected\n",
        izeile);
      return(2);
      }
    }

  if(ipflag != 0 && iqflag == 0)
    {
    ier= sscanf(cline,"%ld %ld %lg %lg %ld %lg %lg",
    &ivon,&ibis,&emod,&rnue,&intord,&qpara,&riyy);
    if(ier != 7) 
      {
      printf("%s\n",cline);
      if(LANG == 1)
        printf(
        "### Schreibfehler oder fehlende Daten in Zeile %ld entdeckt\n",
        izeile);
      if(LANG == 2)
        printf(
        "### typing error or missing entries in line %ld detected\n",
        izeile);
      return(2);
      }
    }

/*---------------------------------------------------------------------
* ivon ?
*--------------------------------------------------------------------*/
/*=====================================================================
* Sonderfall 1. E-Gesetz:
*====================================================================*/
  if(i == 1)
    {
    if(ivon != 1)
      {
      printf("%s\n",cline);
      if(LANG == 1)
        {
        printf("### Startwert im 1. E-Gesetz nicht 1\n");
        printf("### 1.Wert in Zeile %ld ueberpruefen\n",izeile);
        }
      if(LANG == 2)
        {
        printf("### start value in 1st mat line not 1\n");
        printf("### check 1st entry in line %ld\n",izeile);
        }
      return(2);
      }
    }
          
/*=====================================================================
* ivon groesser als ne ?
*====================================================================*/
  if(ivon > ne)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf(
      "### Anfangswert im %ld. E-Gesetz groesser als %ld = Anzahl Elemente\n",
      i,ne);
      printf("### 3.Wert in Zeile 1 ueberpruefen\n");
      printf("### 1.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf(
      "### start value in %ld. mat line greater %ld=number of elements\n",
      i,ne);
      printf("### check 3rd entry in line 1\n");
      printf("### check 1st entry in line %ld\n",izeile);
      }
    return(2);
    }

/*=====================================================================
* ivon groesser als ibis ?
*====================================================================*/
  if(ivon > ibis)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Anfangswert im %ld. E-Gesetz groesser als Endwert\n",i);
      printf("### 1.Wert und 2.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### start value in %ld. mat line greater than end value\n",i);
      printf("### check 1st entry and 2nd entry in line %ld\n",izeile);
      }
    return(2);
    }

/*---------------------------------------------------------------------
* ibis ?
*--------------------------------------------------------------------*/
/*=====================================================================
* ibis groesser als ne ?
*====================================================================*/
  if(ibis > ne)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf(
      "### Endwert im %ld. E-Gesetz groesser als %ld = Anzahl Elemente\n",
      i,ne);
      printf("### 3.Wert in Zeile 1 ueberpruefen\n");
      printf("### 2.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf(
      "### end value in %ld. mat line greater %ld=number of elements\n",
      i,ne);
      printf("### check 3rd entry in line 1\n");
      printf("### check 2nd entry in line %ld\n",izeile);
      }
    return(2);
    }
          
/*=====================================================================
* i > 1: ivon nicht ibisalt +1 ?
*====================================================================*/
  if(i > 1)
    {
    if(ivon != ibisalt+1)
      {
      printf("%s\n",clina);
      printf("%s\n",cline);
      if(LANG == 1)
        {
        printf("### Endwert im %ld. E-Gesetz nicht\n",i-1);
        printf("### Anfangswert-1 im %ld. E-Gesetz\n",i);
        printf("### 2.Wert in Zeile %ld ueberpruefen\n",izeile-1);
        printf("### 1.Wert in Zeile %ld ueberpruefen\n",izeile);
        }
      if(LANG == 2)
        {
        printf("### end value in mat line %ld not equal\n",i-1);
        printf("### (start value-1) in mat line %ld\n",i);
        printf("### check 2nd entry in line %ld\n",izeile-1);
        printf("### check 1st entry in line %ld\n",izeile);
        }
      return(2);
      }
    }

/*=====================================================================
* Sonderfall letztes E-Gesetz:
*====================================================================*/
  if(i == neg)
    {
    if(ibis != ne)
      {
      printf("%s\n",cline);
      if(LANG == 1)
        {
        printf("### Endwert im letzten E-Gesetz nicht %ld\n",ne);
        printf("### 3.Wert in Zeile 1 ueberpruefen\n");
        printf("### 2.Wert in Zeile %ld ueberpruefen\n",izeile);
        }
      if(LANG == 2)
        {
        printf("### end value in last mat line not equal %ld\n",ne);
        printf("### check 3rd entry in line 1\n");
        printf("### check 2nd entry in line %ld\n",izeile);
        }
      return(2);
      }
    }

/*----------------------------------------------------------------------
* emod nicht zu klein ?
*---------------------------------------------------------------------*/
  if(FR_FABS(emod) < 1e-13)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### E-Modul fuer %ld. E-Gesetz nahe 0\n",i);
      printf("### 3.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### Young's Modulus for mat line %ld about 0\n",i);
      printf("### check 3rd entry in line %ld\n",izeile);
      }
    return(2);
    }
    
/*----------------------------------------------------------------------
* rnue nicht zu klein ?
*---------------------------------------------------------------------*/
  if(FR_FABS(rnue) < 1e-13)
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Querkontraktionszahl fuer %ld. E-Gesetz nahe 0\n",i);
      printf("### 4.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### Poisson's Ratio for mat line %ld about 0\n",i);
      printf("### check 4th entry in line %ld\n",izeile);
      }
    return(2);
    }

/*----------------------------------------------------------------------
* intord ?
*---------------------------------------------------------------------*/
if(!(intord == 0 || intord == 1 || intord == 2 || intord == 3 ||
     intord == 4 || intord == 5 || intord == 7 || intord == 13))
    {
    printf("%s\n",cline);
    if(LANG == 1)
      {
      printf("### Falsche Integrationsordnung im E-Gesetz %ld\n",i);
      printf("### Zulaessige Werte sind:\n");
      printf("### Element-Typen 2,3,4,5,6,9,13      : 0\n");
      printf("### Element-Typen 1,7,8,10,11,12,19,20: 1,2,3,4\n");
      printf("### Element-Typen 14,15,18            : 3,7,13\n");
      printf("### Element-Typen 16,17               : 1,4,5\n");
      printf("### 5.Wert in Zeile %ld ueberpruefen\n",izeile);
      }
    if(LANG == 2)
      {
      printf("### wrong integration value in mat line %ld\n",i);
      printf("### allowed values are the following:\n");
      printf("### element types 2,3,4,5,6,9,13      : 0\n");
      printf("### element types 1,7,8,10,11,12,19,20: 1,2,3,4\n");
      printf("### element types 14,15,18            : 3,7,13\n");
      printf("### element types 16,17               : 1,4,5\n");
      printf("### check 5th entry in line %ld\n",izeile);
      }
    return(2);
    }

/*---------------------------------------------------------------------
* qpara nicht zu klein ?
*--------------------------------------------------------------------*/ 
  for(j= ivon; j <= ibis; j++)
    {
    if(jtyp[j] == 3 || jtyp[j] == 4 || jtyp[j] == 5 ||
       jtyp[j] == 7 || jtyp[j] == 9 || jtyp[j] ==11 ||
       jtyp[j] ==14 || jtyp[j] ==18 || jtyp[j] ==19 ||
       jtyp[j] ==20)
      { 
      if(FR_FABS(qpara) < 1e-13)
        {
        printf("%s\n",cline);
        if(LANG == 1)
          {
          printf("? Querschnittsparameter sehr klein\n");
          printf("? Muss fuer Elementtypen 3,4,5,7,9,11,14,18,19,20\
 ungleich 0 sein\n");
          printf("? 6.Wert in Zeile %ld ueberpruefen\n",izeile);
          printf("? Vielleicht beabsichtigt .. kein Stop\n");
          }
        if(LANG == 2)
          {
          printf("? cross section value very small\n");
          printf("? must be greater than 0 for element types\
 3,4,5,7,9,11,14,18,19,20\n");
          printf("? check 6th entry in line %ld\n",izeile);
          printf("? possibly intended .. no halt\n");
          }
        iwarn++;
        }
      }
    }

/*---------------------------------------------------------------------
* Balkenparameter
*--------------------------------------------------------------------*/ 
/*====================================================================*
* rizz
*=====================================================================*/
  for(j= ivon; j <= ibis; j++)
    {
    if(jtyp[j] == 2 || jtyp[j] == 13)
      { 
      if(FR_FABS(rizz) < 1e-13)
        {
        printf("%s\n",cline);
        if(LANG == 1)
          {
          printf("? Biege- Traegheitsmoment ZZ sehr klein\n");
          printf("? Muss fuer Elementtypen 2 bzw. 13 ungleich 0 sein\n");
          printf("? 9.Wert in Zeile %ld ueberpruefen\n",izeile);
          printf("? Vielleicht beabsichtigt .. kein Stop\n");
          }
        if(LANG == 2)
          {
          printf("? second moment of area ZZ (bending) very small\n");
          printf("? must be greater than 0 for element types 2 and 13\n");
          printf("? check 9th entry in line %ld\n",izeile);
          printf("? possibly intended .. no halt\n");
          }
        iwarn++;
        }
      }
    }

/*====================================================================*
* ezz
*=====================================================================*/
  for(j= ivon; j <= ibis; j++)
    {
    if(jtyp[j] == 2 || jtyp[j] == 13)
      { 
      if(FR_FABS(ezz) < 1e-13)
        {
        printf("%s\n",cline);
        if(LANG == 1)
          {
        printf("? max.Randfaserabstand ZZ sehr klein\n");
        printf("? Muss fuer Elementtypen 2 bzw. 13 ungleich 0 sein\n");
        printf("? 10.Wert in Zeile %ld ueberpruefen\n",izeile);
        printf("? Vielleicht beabsichtigt .. kein Stop\n");
          }
        if(LANG == 2)
          {
          printf("? max. distance from neutral axis ZZ very small\n");
          printf("? must be greater than 0 for element types 2 and 13\n");
          printf("? check 10th entry in line %ld\n",izeile);
          printf("? possibly intended .. no halt\n");
          }
        iwarn++;
        }
      }
    }

/*====================================================================*
* riyy
*=====================================================================*/
  for(j= ivon; j <= ibis; j++)
    {
    if(jtyp[j] == 2)
      { 
      if(FR_FABS(riyy) < 1e-13)
        {
        printf("%s\n",cline);
        if(LANG == 1)
          {
          printf("? Biege- Traegheitsmoment YY sehr klein\n");
          printf("? Muss fuer Elementtypen 2 ungleich 0 sein\n");
          printf("? 7.Wert in Zeile %ld ueberpruefen\n",izeile);
          printf("? Vielleicht beabsichtigt .. kein Stop\n");
          }
        if(LANG == 2)
          {
          printf("? second moment of area YY (bending) very small\n");
          printf("? must be greater than 0 for element type 2\n");
          printf("? check 7th entry in line %ld\n",izeile);
          printf("? possibly intended .. no halt\n");
          }
        iwarn++;
        }
      }
    }

  for(j= ivon; j <= ibis; j++)
    {
    if(jtyp[j] == 18 || jtyp[j] == 19 || jtyp[j] == 20)
      { 
      if(FR_FABS(riyy) < 1e-13)
        {
        printf("%s\n",cline);
        if(LANG == 1)
          {
          printf("? Flaechenlast sehr klein\n");
          printf("? 7.Wert in Zeile %ld ueberpruefen\n",izeile);
          printf("? Vielleicht beabsichtigt .. kein Stop\n");
          }
        if(LANG == 2)
          {
          printf("? area load very small\n");
          printf("? check 7th entry in line %ld\n",izeile);
          printf("? possibly intended .. no halt\n");
          }
        iwarn++;
        }
      }
    }
/*====================================================================*
* eyy
*=====================================================================*/
  for(j= ivon; j <= ibis; j++)
    {
    if(jtyp[j] == 2)
      { 
      if(FR_FABS(eyy) < 1e-13)
        {
        printf("%s\n",cline);
        if(LANG == 1)
          {
        printf("? max.Randfaserabstand YY sehr klein\n");
        printf("? Muss fuer Elementtyp 2 ungleich 0 sein\n");
        printf("? 8.Wert in Zeile %ld ueberpruefen\n",izeile);
        printf("? Vielleicht beabsichtigt .. kein Stop\n");
          }
        if(LANG == 2)
          {
          printf("? max. distance from neutral axis YY very small\n");
          printf("? must be greater than 0 for element type 2\n");
          printf("? check 8th entry in line %ld\n",izeile);
          printf("? possibly intended .. no halt\n");
          }
        iwarn++;
        }
      }
    }

/*====================================================================*
* rit
*=====================================================================*/
  for(j= ivon; j <= ibis; j++)
    {
    if(jtyp[j] == 2)
      { 
      if(FR_FABS(rit) < 1e-13)
        {
        printf("%s\n",cline);
        if(LANG == 1)
          {
        printf("? Torsion-Traegheitsmoment sehr klein\n");
        printf("? Muss fuer Elementtyp 2 ungleich 0 sein\n");
        printf("? 11.Wert in Zeile %ld ueberpruefen\n",izeile);
        printf("? Vielleicht beabsichtigt .. kein Stop\n");
          }
        if(LANG == 2)
          {
          printf("? second moment of area (torsion) very small\n");
          printf("? must be greater than 0 for element type 2\n");
          printf("? check 11th entry in line %ld\n",izeile);
          printf("? possibly intended .. no halt\n");
          }
        iwarn++;
        }
      }
    }

/*====================================================================*
* wt
*=====================================================================*/
  for(j= ivon; j <= ibis; j++)
    {
    if(jtyp[j] == 2)
      { 
      if(FR_FABS(wt) < 1e-13)
        {
        printf("%s\n",cline);
        if(LANG == 1)
          {
          printf("? Torsion-Widerstandsmoment sehr klein\n");
          printf("? Muss fuer Elementtyp 2 ungleich 0 sein\n");
          printf("? 12.Wert in Zeile %ld ueberpruefen\n",izeile);
          printf("? Vielleicht beabsichtigt .. kein Stop\n");
          }
        if(LANG == 2)
          {
          printf("? section modulus (torsion) very small\n");
          printf("? must be greater than 0 for element type 2\n");
          printf("? check 12th entry in line %ld\n",izeile);
          printf("? possibly intended .. no halt\n");
          }
        iwarn++;
        }
      }
    }

  strcpy(clina,cline);
  }                                              /* e40 */


/***********************************************************************
* Ende grp4: keine Fehler
***********************************************************************/
return(0);
}
