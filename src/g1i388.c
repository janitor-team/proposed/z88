/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V13.0  December 12, 2008
*
* Z88 should compile and run under any UNIX OS and Motif 2.0.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/**********************************************************************
* Function g1i388 prueft Z88I3.TXT
* 29.11.2008 Rieg
**********************************************************************/

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88v.h>
#include <stdio.h>   /* FILE,printf */
#endif

/***********************************************************************
* Functions
***********************************************************************/
void erif88(FR_INT4 izeile);

/***********************************************************************
* Start G1I388
***********************************************************************/
int g1i388(void)
{
extern FILE *fdatei;

extern FR_INT4 ispann,izeile,LANG;

FR_INT4 ninto,kflag,isflag;

int ier;

char *cresult;
char cline[256];

/**********************************************************************
* File ueberhaupt notwendig ?
**********************************************************************/
if(ispann == 0)
  {
  if(LANG == 1)
    {
    printf("? File Z88I3.TXT (Parameterfile Spannungen) ist\n");
    printf("? inhaltlich fuer das vorgesehene Eingabefile\n");
    printf("? Z88I1.TXT nicht noetig\n");
    printf("? Inhalt von Z88I3.TXT kann daher beliebig sein\n");          
    }
  if(LANG == 2)
    {
    printf("? file Z88I3.TXT (stress flag file) is of no \n");
    printf("? importance for this general input file Z88I1.TXT\n");
    printf("? contents of Z88I3.TXT can be arbitrary\n");          
    }
  return(0);
  }

/**********************************************************************
* Checken der 1.Zeile
**********************************************************************/
izeile= 1; 

cresult= fgets(cline,256,fdatei);
if(!cresult)
  {
  erif88(izeile);
  return(2);
  }

ier= sscanf(cline,"%ld %ld %ld",&ninto,&kflag,&isflag);
if(ier != 3) 
  {
  printf("%s\n",cline);
  if(LANG == 1)
    printf("### Schreibfehler oder fehlende Daten in Zeile 1 entdeckt\n");
  if(LANG == 2)
    printf("### typing error or missing entries in line 1 detected\n");
  return(2);
  }
        
/*----------------------------------------------------------------------
* logische Pruefungen
*---------------------------------------------------------------------*/
if(!(ninto == 0 || ninto == 1 || ninto == 2 || ninto == 3 ||
     ninto == 4 || ninto == 5 || ninto == 7 || ninto == 13))
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### Falsche Integrationsordnung:\n");
    printf("### Zulaessig fuer Element-Typen 2,3,4,5,6,9,13: 0\n");
    printf("### Zulaessig fuer Element-Typen 1,7,8,10,11,12: 1,2,3,4\n");
    printf("### Zulaessig fuer Element-Typen 14,15         : 3,7,13\n");
    printf("### Zulaessig fuer Element-Typen 16,17         : 1,4,5\n");
    printf("### 1.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### wrong integration value\n");
    printf("### allowed values for element types 2,3,4,5,6,9,13: 0\n");
    printf("### allowed values for element types 1,7,8,10,11,12: 1,2,3,4\n");
    printf("### allowed values for element types 14,15         : 3,7,13\n");
    printf("### allowed values for element types 16,17         : 1,4,5\n");
    printf("### check 1st entry in line 1\n");
    }
  return(2);
  }

if(kflag < 0 || kflag > 1)
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### KFLAG unzulaessig\n");
    printf("### Zulaessig: 0 = Standard-Spannungsrechung\n");
    printf("###            1 = zusaetzlich Radial- und Tangentialsp.\n");
    printf("### 2.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### KFLAG invalid\n");
    printf("### allowed: 0 = computing standard stresses\n");
    printf("###          1 = radial- and tangential stresses added\n");
    printf("### check 2nd entry in line 1\n");
    }
  return(2);
  }

if(isflag < 0 || isflag > 3)
  {
  printf("%s\n",cline);
  if(LANG == 1)
    {
    printf("### Steuerflag fuer Vergleichsspannungen unzulaessig\n");
    printf("### Zulaessig: 0 = keine Vergleichsspannungsrechnung\n");
    printf("###            1 = Gestaltsaenderungsenergie-Hypothese\n");
    printf("###            2 = Normalspannungs-Hypothese\n");
    printf("###            3 = Schubspannungs-Hypothese\n");
    printf("### 3.Wert in Zeile 1 ueberpruefen\n");
    }
  if(LANG == 2)
    {
    printf("### flag for reduced stress invalid\n");
    printf("### allowed: 0 = no computing of reduced stresses\n");
    printf("###          1 = compute von Mises stresses\n");
    printf("###          2 = compute principal stresses\n");
    printf("###          3 = compute Tresca stresses\n");
    printf("### check 3rd entry in line 1\n");
    }
  return(2);
  }

return(0);
}
