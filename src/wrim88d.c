/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V13.0  February 12, 2008
*
* Z88 should compile and run under any UNIX OS and Motif 2.0.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/***********************************************************************
* Diese Compilerunit umfasst:
* - wrim88d gibt Texte aus (1 FR_INT4)
* - wtyp88d gibt Elementtypen aus (1 FR_INT4, 1 FR_INT4)
* 22.3.2008 Rieg
***********************************************************************/ 

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88d.h>
#include <stdio.h>    /* printf */
#endif

/***********************************************************************
*  hier beginnt Function wrim88d
***********************************************************************/
int wrim88d(FR_INT4 i,int iatx)
{
extern FR_INT4 LANG,isflag;

switch(iatx)
  {
  case TX_REAO1Y:
    if(LANG == 1) printf("Z88O1.BNY einlesen\n");
    if(LANG == 2) printf("reading Z88O1.BNY\n");
    break;
  case TX_REAO3Y:
    if(LANG == 1) printf("Z88O3.BNY einlesen\n");
    if(LANG == 2) printf("reading Z88O3.BNY\n");
    break;
  case TX_SPANNU:
    if(LANG == 1 && isflag == 1) printf("***** Spannungen (GEH) *****\n");
    if(LANG == 2 && isflag == 1) printf("***** stresses (v.Mises) *****\n");
    if(LANG == 1 && isflag == 2) printf("***** Spannungen (NH) *****\n");
    if(LANG == 2 && isflag == 2) printf("***** stresses (principal) *****\n");
    if(LANG == 1 && isflag == 3) printf("***** Spannungen (SH) *****\n");
    if(LANG == 2 && isflag == 3) printf("***** stresses (Tresca) *****\n");
    break;

  case TX_EXITZ88D:
    if(LANG == 1) printf("\nEnde Z88D\n");
    if(LANG == 2) printf("\nZ88D done\n");
    break;
  }
return(0);
}

/***********************************************************************
*  function wtyp88d gibt Elementtypen in Z88D aus
***********************************************************************/ 
int wtyp88d(FR_INT4 k,FR_INT4 i)
{
extern FR_INT4 LANG;

#ifdef FR_XINT
if(LANG == 1) printf("\rNr. %5d Typ %5d",k,i);
if(LANG == 2) printf("\rno. %5d type %5d",k,i);
#endif

#ifdef FR_XLONG
if(LANG == 1) printf("\rNr. %5ld Typ %5ld",k,i);
if(LANG == 2) printf("\rno. %5ld type %5ld",k,i);
#endif

#ifdef FR_XLOLO
if(LANG == 1) printf("\rNr. %5lld Typ %5lld",k,i);
if(LANG == 2) printf("\rno. %5lld type %5lld",k,i);
#endif

return(0);
}
