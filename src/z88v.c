/***********************************************************************
* 
*               *****   ***    ***
*                  *   *   *  *   *
*                 *     ***    ***
*                *     *   *  *   *
*               *****   ***    ***
*
* A FREE Finite Elements Analysis Program in ANSI C for the Windows & UNIX OS.
*
* Composed and edited and copyright by 
* Professor Dr.-Ing. Frank Rieg, University of Bayreuth, Germany
*
* eMail: 
* frank.rieg@uni-bayreuth.de
* dr.frank.rieg@t-online.de
* 
* V13.0  February 14, 2008
*
* Z88 should compile and run under any Windows and UNIX OS.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; see the file COPYING.  If not, write to
* the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
***********************************************************************/ 
/***********************************************************************
* Z88V
* 15.5.2008 Rieg
***********************************************************************/

/***********************************************************************
* Fuer UNIX
***********************************************************************/
#ifdef FR_UNIX
#include <z88v.h>
#include <stdio.h>   /* FILE,fclose,printf */
#include <string.h>  /* strcpy */
#endif

/***********************************************************************
* Functions
***********************************************************************/
int who88v(void);
int dyn88v(void);
int ale88v(int);
int FR_GETC(void);
int g1i188(void);
int g2i188(void);
int g3i188(void);
int g4i188(void);
int g5i188(void);
int g1i288(void);
int g2i288(void);
int g1i388(void);
int g1i588(void);
int g1i488(void);
int g2i588(void);
int vend88(int);
void stop88v(void);
int lan88v(void);
void clr88(void);
 
/***********************************************************************
* Globale Variable
***********************************************************************/
FILE *fdatei,*fdyn,*fwlo;

/*  
**   fdatei= z88ni.txt,z88i1.txt,z88i2.txt,z88i3.txt
**   fdyn  = z88.dyn
**   fwlo  = z88e.log
*/ 

char cdyn[8] = "z88.dyn";
char clgd[9] = "z88v.log";

FR_INT4AY jtyp;
FR_INT4AY jzeile;
FR_INT4AY ifrei;

FR_INT4 IDYNMEM,LANG=2;
FR_INT4 MAXSE,MAXESS,MAXKSS,MAXAN;
FR_INT4 MAXGS,MAXKOI,MAXK,MAXE,MAXNFG,MAXNEG,MAXPR,MAXIEZ,MAXRBD;
FR_INT4 MAXGP,NEEDKOI;
FR_INT4 MAXESM=3600;

FR_INT4 ndim,nkp,ne,nfg,neg,kflag,niflag,ibflag,ipflag,iqflag,nrb,npr;
FR_INT4 iwarn,izeile,iruni1,ispann,ifnii1;

FR_DOUBLE eps,rpomega,rpalpha;
FR_INT4   maxit,icore;

int       *pp;

char      cdatei[10];

/***********************************************************************
* Start Z88V
***********************************************************************/
int main(void)
{
extern FR_INT4 MAXSE,MAXESS,MAXKSS,MAXAN;
extern FR_INT4 MAXGS,MAXKOI,MAXK,MAXE,MAXNFG,MAXNEG,MAXPR,MAXIEZ,MAXRBD;
extern FR_INT4 MAXGP,MAXESM;

extern FR_INT4 LANG;

FR_DOUBLE      RDYNMEM;

FR_INT4        isum= 0;

int            jfile,ier,iret;

                 
iruni1= 0;                   /* Check Z88I1.TXT gelaufen ? ja = 1     */
ispann= 0;                   /* Spannungsfile noetig ? ja = 1         */
ifnii1= 0;                   /* Vorwahl fuer Z88I1.TXT, Z88NI.TXT = 1 */

/*---------------------------------------------------------------------------
*  Die Sprache feststellen
*--------------------------------------------------------------------------*/
LANG = 0;
iret= lan88v();

if(iret != 0)
  {
  ale88v(iret);
  stop88v();
  }

/*---------------------------------------------------------------------------
* dyn88e starten: anfordern dynamic memory
*--------------------------------------------------------------------------*/
iret= dyn88v();
if(iret != 0)
  {
  ale88v(iret);
  stop88v();              
  }           

LnextFile:;

/*---------------------------------------------------------------------------
* who88v starten: Ueberschrift
*--------------------------------------------------------------------------*/
who88v();

iwarn= 0;

if(LANG == 1)
  {
  printf(" 1  -->  Datei der Allgemeine Eingaben Z88I1.TXT\n");
  printf(" 2  -->  Datei der Randbedingungen Z88I2.TXT\n");
  printf(" 3  -->  Parameterfile fuer den Spannungsprozessor Z88I3.TXT\n");
  printf(" 4  -->  Parameterfile fuer die Sparsematrix-Solver Z88I4.TXT\n");
  printf(" 5  -->  Netzgenerator- Eingabefile Z88NI.TXT\n");
  printf(" 6  -->  Speicher-Steuerdatei Z88.DYN\n");
  printf(" 7  -->  Datei der Strecken- und Flaechenlasten Z88I5.TXT\n");
  printf(" 9  -->  Beende Z88V\n\n");
  printf("         Ihre Wahl: ");
  }

if(LANG == 2)
  {
  printf(" 1  -->  general inputfile Z88I1.TXT\n");
  printf(" 2  -->  constraints file Z88I2.TXT\n");
  printf(" 3  -->  stress flag file Z88I3.TXT\n");
  printf(" 4  -->  control file for the Sparse Matrix Solvers Z88I4.TXT\n");
  printf(" 5  -->  mesh generator inputfile Z88NI.TXT\n");
  printf(" 6  -->  memory definition file Z88.DYN\n");
  printf(" 7  -->  edge and surface load file Z88I5.TXT\n");
  printf(" 9  -->  exit Z88V\n\n");
  printf("         your choice: ");
  }
  
/**********************************************************************
* Oeffnen des gewuenschten Files 
**********************************************************************/
do
  {
  jfile= FR_GETC();

  if(jfile == 5)
    {
    ifnii1= 1;
    strcpy(cdatei,"z88ni.txt");
    fdatei= fopen(cdatei,"r");
    if(fdatei == NULL) vend88(3);
    }
  else if (jfile == 1)
    {
    ifnii1= 0;
    strcpy(cdatei,"z88i1.txt");
    fdatei= fopen(cdatei,"r");
    if(fdatei == NULL) vend88(3);
    }
  else if (jfile == 2)
    {
    strcpy(cdatei,"z88i2.txt");
    fdatei= fopen(cdatei,"r");
    if(fdatei == NULL) vend88(3);
    }
  else if (jfile == 3)
    {
    strcpy(cdatei,"z88i3.txt");
    fdatei= fopen(cdatei,"r");
    if(fdatei == NULL) vend88(3);
    }
  else if (jfile == 4)
    {
    strcpy(cdatei,"z88i4.txt");
    fdatei= fopen(cdatei,"r");
    if(fdatei == NULL) vend88(3);
    }
  else if (jfile == 6)
    {
    strcpy(cdatei,"z88.dyn");
    }
  else if (jfile == 7)
    {
    strcpy(cdatei,"z88i5.txt");
    fdatei= fopen(cdatei,"r");
    if(fdatei == NULL) vend88(3);
    }
  else if (jfile == 9)
    {
    stop88v();
    }
  else
    {
    if(LANG == 1) printf("Falsche Auswahl --> neue Taste: ");
    if(LANG == 2) printf("wrong choice --> once again: ");
    }
  }
while(!(jfile == 5 || jfile == 1 || jfile == 2 || jfile == 4 ||
        jfile == 3 || jfile == 6 || jfile == 7 || jfile == 9)); 
    
             
if(fdatei != NULL) rewind(fdatei);

if(LANG == 1) printf("File %s geoeffnet ..\n",cdatei);
if(LANG == 2) printf("file %s opened ..\n",cdatei);
             
/**********************************************************************
* je nach Wahl des Files Aktionen einleiten:
**********************************************************************/
/*---------------------------------------------------------------------
* Z88NI.TXT und Z88I1.TXT
*--------------------------------------------------------------------*/
if(jfile == 5 || jfile == 1)
  {
/*=====================================================================
* 1. Eingabegruppe
*====================================================================*/
  if(LANG == 1) printf("Checken der 1. Eingabegruppe ..\n");
  if(LANG == 2) printf("checking 1st input group ..\n");
  ier= g1i188();
  if (ier != 0) vend88(ier);

/*=====================================================================
* 2. Eingabegruppe
*====================================================================*/
  if(LANG == 1) printf("Checken der 2. Eingabegruppe ..\n");
  if(LANG == 2) printf("checking 2nd input group ..\n");
  ier= g2i188();
  if (ier != 0) vend88(ier);
  
/*=====================================================================
* 3. Eingabegruppe
*====================================================================*/
  if(LANG == 1) printf("Checken der 3. Eingabegruppe ..\n");
  if(LANG == 2) printf("checking 3rd input group ..\n");
  ier= g3i188();
  if (ier != 0) vend88(ier);

/*=====================================================================
* 4. Eingabegruppe
*====================================================================*/
  if(LANG == 1) printf("Checken der 4. Eingabegruppe ..\n");
  if(LANG == 2) printf("checking 4th input group ..\n");
  ier= g4i188();
  if (ier != 0) vend88(ier);
  if(ifnii1 == 0) iruni1= 1;
        
/*=====================================================================
* 5. Eingabegruppe, falls Z88NI.TXT
*====================================================================*/
  if(ifnii1 == 1)
    {
    if(LANG == 1) printf("Checken der 5. Eingabegruppe ..\n");
    if(LANG == 2) printf("checking 5th input group ..\n");
    ier= g5i188();
    if (ier != 0) vend88(ier);
    }

  if(LANG == 1) printf("MAXKOI mindestens %ld\n",NEEDKOI);
  if(LANG == 2) printf("MAXKOI at least %ld\n",NEEDKOI);
  }
          
/*---------------------------------------------------------------------
* Z88I2.TXT
*--------------------------------------------------------------------*/
else if (jfile == 2)
  {
/*=====================================================================
* 1. Eingabegruppe
*====================================================================*/
  if(iruni1 == 0)
    {
    if(LANG == 1)
      {
      printf("Bitte erst File Z88I1.TXT checken...\n");
      printf("Weiter mit 1: ");
      }  
    if(LANG == 2)
      {
      printf("please check file Z88I1.TXT first...\n");
      printf("continue with 1: ");
      }  
    FR_GETC();
    goto LnextFile;
    }
            
  if(LANG == 1) printf("Checken der 1. Eingabegruppe ..\n");
  if(LANG == 2) printf("checking 1st input group ..\n");
  ier= g1i288();
  if (ier != 0) vend88(ier);

/*=====================================================================
* 2. Eingabegruppe
*====================================================================*/
  if(LANG == 1) printf("Checken der 2. Eingabegruppe ..\n");
  if(LANG == 2) printf("checking 2nd input group ..\n");
  ier= g2i288();
  if (ier != 0) vend88(ier);

  if(LANG == 1) printf("MAXRBD mindestens %ld\n",nrb);
  if(LANG == 2) printf("MAXRBD at least %ld\n",nrb);
  }
  
/*---------------------------------------------------------------------
* Z88I3.TXT
*--------------------------------------------------------------------*/
else if (jfile == 3)
  {
  if(iruni1 == 0)
    {
    if(LANG == 1)
      {
      printf("Bitte erst File Z88I1.TXT checken...\n");
      printf("Weiter mit 1: ");
      }  
    if(LANG == 2)
      {
      printf("please check file Z88I1.TXT first...\n");
      printf("continue with 1: ");
      }  
    FR_GETC();
    goto LnextFile;
    }
            
  if(LANG == 1) printf("Checken der 1. Eingabegruppe ..\n");
  if(LANG == 2) printf("checking 1st input group ..\n");
  ier= g1i388();
  if (ier != 0) vend88(ier);
  }

/*---------------------------------------------------------------------
* Z88I4.TXT
*--------------------------------------------------------------------*/
else if (jfile == 4)
  {
  if(LANG == 1) printf("Checken der 1. Eingabegruppe ..\n");
  if(LANG == 2) printf("checking 1st input group ..\n");
  ier= g1i488();
  if (ier != 0) vend88(ier);
  if(LANG == 1) printf("Z88I2: Max. %ld Iterationen\n\
Z88I2: Epsilon %g\n\
Z88I2: SIC-Vorkonditionierer: Alpha= %g\n\
Z88I2: SOR-Vorkonditionierer: Omega= %g\n\
Sparsematrix-Solver Z88I2 und Z88PAR werden %ld CPUs verwenden\n",
maxit,eps,rpalpha,rpomega,icore);
  if(LANG == 2) printf("Z88I2: Max. %ld iterations\n\
Z88I2: Epsilon %g\n\
Z88I2: SIC preconditioner: Alpha= %g\n\
Z88I2: SOR preconditioner: Omega= %g\n\
Sparse Matrix Solvers Z88I2 and Z88PAR will use %ld CPUs\n",
maxit,eps,rpomega,rpalpha,icore);
  }

/*---------------------------------------------------------------------
* Z88I5.TXT
*--------------------------------------------------------------------*/
else if (jfile == 7)
  {
/*=====================================================================
* 1. Eingabegruppe
*====================================================================*/
  if(iruni1 == 0)
    {
    if(LANG == 1)
      {
      printf("Bitte erst File Z88I1.TXT checken...\n");
      printf("Weiter mit 1: ");
      }  
    if(LANG == 2)
      {
      printf("please check file Z88I1.TXT first...\n");
      printf("continue with 1: ");
      }  
    FR_GETC();
    goto LnextFile;
    }

  if(iqflag == 0)
    {
    if(LANG == 1)
      {
      printf("IQFLAG in Z88I1.TXT nicht gesetzt...\n");
      printf("Flaechenlastdatei Z88I5.TXT wird nicht angezogen...\n");
      printf("Weiter mit 1: ");
      }  
    if(LANG == 2)
      {
      printf("IQFLAG not set in file Z88I1.TXT...\n");
      printf("surface load file Z88I5.TXT not used...\n");
      printf("continue with 1: ");
      }  
    FR_GETC();
    goto LnextFile;
    }
            
  if(LANG == 1) printf("Checken der 1. Eingabegruppe ..\n");
  if(LANG == 2) printf("checking 1st input group ..\n");
  ier= g1i588();
  if (ier != 0) vend88(ier);

/*=====================================================================
* 2. Eingabegruppe
*====================================================================*/
  if(LANG == 1) printf("Checken der 2. Eingabegruppe ..\n");
  if(LANG == 2) printf("checking 2nd input group ..\n");
  ier= g2i588();
  if (ier != 0) vend88(ier);

  if(LANG == 1) printf("MAXPR mindestens %ld\n",npr);
  if(LANG == 2) printf("MAXPR at least %ld\n",npr);
  }
  
/*---------------------------------------------------------------------
* Z88.DYN
*--------------------------------------------------------------------*/
else if (jfile == 6)
  {
  clr88();

  if(LANG == 1)
    {
     printf("\n\n\nDynamischen Memorybedarf fuer Z88-Module\n");
     printf(      "----------------------------------------\n\n");
    }

  if(LANG == 2)
    {
    printf("\n\n\nDynamic Memory Needs for Z88 Programs\n");
    printf(      "-------------------------------------\n\n");
    }

/*----------------------------------------------------------------------
*  Bildschirm loeschen + Ueberschrift
*---------------------------------------------------------------------*/
  if(MAXSE <= 0 || MAXESS <= 0 || MAXKSS <= 0 || MAXAN <= 0)
    {
    if(LANG == 1) printf("### Irgendein Wert in Gruppe NET <= 0..Stop\n");
    if(LANG == 2) printf("### some value in group NET <= 0..stop\n");
    vend88(2);
    }  

  if(MAXGS <= 0 || MAXKOI <= 0 || MAXK <= 0 || MAXE <= 0 || MAXNFG <= 0 ||
     MAXNEG <= 0 || MAXPR <= 0  || MAXIEZ <= 0 || MAXRBD <= 0 || MAXGP <= 0)
    {
    if(LANG == 1) printf("### Irgendein Wert in Gruppe COMMON <= 0..Stop\n");
    if(LANG == 2) printf("### some value in group COMMON <= 0..stop\n");
    vend88(2);
    }  

/*----------------------------------------------------------------------
*  auswerten..
*---------------------------------------------------------------------*/
  if(LANG == 1)
  printf("Z88.DYN definiert momentan folgende Speicherbedarfe:\n\n");

  if(LANG == 2)
  printf("Z88.DYN currently defines the following memory needs:\n\n");

/*=====================================================================*
*  Z88F
*=====================================================================*/
  RDYNMEM =   ((double)MAXGS+1.) *sizeof(FR_DOUBLE); /* gs */
  RDYNMEM+=   ((double)MAXESM+1.)*sizeof(FR_DOUBLE); /* se */
  RDYNMEM+= 2*((double)MAXNFG+1.)*sizeof(FR_DOUBLE); /* rs, fak */
  RDYNMEM+= 3*((double)MAXK+1.)  *sizeof(FR_DOUBLE); /* x,y,z */
  RDYNMEM+= 3*((double)MAXNEG+1.)*sizeof(FR_DOUBLE); /* emod, rnue, qpara */
  RDYNMEM+=   ((double)MAXNFG+1.)*sizeof(FR_INT4);   /* ip */
  RDYNMEM+=   ((double)MAXKOI+1.)*sizeof(FR_INT4);   /* koi */
  RDYNMEM+= 2*((double)MAXK+1.)  *sizeof(FR_INT4);   /* ifrei, ioffs */
  RDYNMEM+= 2*((double)MAXE+1.)  *sizeof(FR_INT4);   /* koffs, ityp */
  RDYNMEM+= 3*((double)MAXNEG+1.)*sizeof(FR_INT4);   /* ivon,ibis,intord */
  RDYNMEM+= 6*((double)MAXNEG+1.)*sizeof(FR_DOUBLE); /* riyy,eyy,rizz,ezz,rit,wt */
  RDYNMEM+=10*((double)MAXPR+1.) *sizeof(FR_INT4);   /* nep,noi,noffs */
  RDYNMEM+= 3*((double)MAXPR+1.) *sizeof(FR_DOUBLE); /* pres,tr1,tr2 */

  isum= (FR_INT4) (RDYNMEM/1048576.);  /* in MegaByte */

  printf("   Z88F    : %12ld MegaBytes\n",isum);

/*=====================================================================*
*  Z88I1
*=====================================================================*/
  RDYNMEM = 3*((double)MAXK+1.)  *sizeof(FR_DOUBLE);       /* x,y,z */
  RDYNMEM+= 3*((double)MAXNEG+1.)*sizeof(FR_DOUBLE);       /* emod, rnue, qpara */
  RDYNMEM+=   ((double)MAXNFG+1.)*sizeof(FR_INT4);         /* ip */
  RDYNMEM+=   ((double)MAXIEZ+1.)*sizeof(FR_INT4);         /* iez */
  RDYNMEM+=   ((double)MAXKOI+1.)*sizeof(FR_INT4);         /* koi */
  RDYNMEM+= 2*((double)MAXK+1.)  *sizeof(FR_INT4);         /* ifrei, ioffs */
  RDYNMEM+= 2*((double)MAXE+1.)  *sizeof(FR_INT4);         /* koffs, ityp */
  RDYNMEM+= 3*((double)MAXNEG+1.)*sizeof(FR_INT4);         /* ivon,ibis,intord */
  RDYNMEM+= 6*((double)MAXNEG+1.)*sizeof(FR_DOUBLE); /* riyy,eyy,rizz,ezz,rit,wt */

  isum= (FR_INT4) (RDYNMEM/1048576.);

  printf("   Z88I1   : %12ld MegaBytes\n",isum);

/*=====================================================================*
*  Z88I2
*=====================================================================*/
/*=====================================================================*
*  Z88I2
*=====================================================================*/
  RDYNMEM =   ((double)MAXGS+1.) *sizeof(FR_DOUBLE);     /* GS */

  RDYNMEM+=   ((double)MAXESM+1.)*sizeof(FR_DOUBLE);     /* se */
  RDYNMEM+= 7*((double)MAXNFG+1.)*sizeof(FR_DOUBLE);     /* rs,fak,xi,xa,v,pk,zz*/
  RDYNMEM+= 3*((double)MAXK+1.)  *sizeof(FR_DOUBLE);     /* x,y,z */
  RDYNMEM+= 3*((double)MAXNEG+1.)*sizeof(FR_DOUBLE);     /* emod, rnue, qpara */
  RDYNMEM+=   ((double)MAXNFG+1.)*sizeof(FR_INT4);       /* ip */
  RDYNMEM+=   ((double)MAXGS+1.) *sizeof(FR_INT4);       /* iez */
  RDYNMEM+=   ((double)MAXKOI+1.)*sizeof(FR_INT4);       /* koi */
  RDYNMEM+= 2*((double)MAXK+1.)  *sizeof(FR_INT4);       /* ifrei, ioffs */
  RDYNMEM+= 2*((double)MAXE+1.)  *sizeof(FR_INT4);       /* koffs, ityp */
  RDYNMEM+= 3*((double)MAXNEG+1.)*sizeof(FR_INT4);       /* ivon,ibis,intord */
  RDYNMEM+= 6*((double)MAXNEG+1.)*sizeof(FR_DOUBLE);     /* riyy,eyy,rizz,ezz,rit,wt */
  RDYNMEM+=10*((double)MAXPR+1.) *sizeof(FR_INT4);       /* nep,noi,noffs */
  RDYNMEM+= 3*((double)MAXPR+1.) *sizeof(FR_DOUBLE);     /* pres,tr1,tr2 */

  isum= (FR_INT4)(RDYNMEM/1048576.);   /* in MegaByte */

  printf("   Z88I2 -S: %12ld MegaBytes\n",isum);

  isum+= ((double)MAXGS+1.) *sizeof(FR_DOUBLE)/1048576.;     /* CI */

  printf("   Z88I2 -C: %12ld MegaBytes\n",isum);

/*=====================================================================*
*  Z88PAR
*=====================================================================*/
  RDYNMEM =   ((double)MAXGS+1.) *sizeof(FR_DOUBLE);     /* GS */
  RDYNMEM+=   ((double)MAXESM+1.)*sizeof(FR_DOUBLE);     /* se */
  RDYNMEM+= 2*((double)MAXNFG+1.)*sizeof(FR_DOUBLE);     /* rs,fak */
  RDYNMEM+= 3*((double)MAXK+1.)  *sizeof(FR_DOUBLE);     /* x,y,z */
  RDYNMEM+= 3*((double)MAXNEG+1.)*sizeof(FR_DOUBLE);     /* emod, rnue, qpara */
  RDYNMEM+=   ((double)MAXNFG+1.)*sizeof(FR_INT4);       /* ip */
  RDYNMEM+=   ((double)MAXGS+1.) *sizeof(FR_INT4);       /* iez */
  RDYNMEM+=   ((double)MAXKOI+1.)*sizeof(FR_INT4);       /* koi */
  RDYNMEM+= 2*((double)MAXK+1.)  *sizeof(FR_INT4);       /* ifrei, ioffs */
  RDYNMEM+= 2*((double)MAXE+1.)  *sizeof(FR_INT4);       /* koffs, ityp */
  RDYNMEM+= 3*((double)MAXNEG+1.)*sizeof(FR_INT4);       /* ivon,ibis,intord */
  RDYNMEM+= 6*((double)MAXNEG+1.)*sizeof(FR_DOUBLE);     /* riyy,eyy,rizz,ezz,rit,wt */
  RDYNMEM+=10*((double)MAXPR+1.) *sizeof(FR_INT4);       /* nep,noi,noffs */
  RDYNMEM+= 3*((double)MAXPR+1.) *sizeof(FR_DOUBLE);     /* pres,tr1,tr2 */

  isum= (FR_INT4) (RDYNMEM/1048576.);

  printf("   Z88PAR  : %12ld MegaBytes\n\n",isum);

/*=====================================================================*
*  Tja dann
*=====================================================================*/
  if(LANG == 1)
    {
    printf("Stellen Sie sicher, dass der virtuelle Speicher Ihres Computers\n");
    printf("den hoechsten Wert abdeckt. Sonst Werte in Z88.DYN erniedrigen\n");
    }

  if(LANG == 2)
    {
    printf("Make sure that virtual memory of your computer covers the\n");
    printf("highest value. Otherwise decrease values in file Z88.DYN\n\n");
    }

  } /* Ende Z88.DYN */
  
/**********************************************************************
* Ende Z88V ohne Fehler
**********************************************************************/
vend88(0);

goto LnextFile;

return (0);
}
